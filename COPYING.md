# COPYING

## Source Code
All source code in this repository is available under the GNU Affero General Public License, version 3 only.

A copy of the license is provided in this repository as the file "LICENSE".

## SDL2 CMake Modules
Note that the cmake modules are not a part of the source code itself, and are not required to build the program. Instead, they may be used as a convenience.

`cmake/FindSDL2.cmake`\
`cmake/FindSDL2_image.cmake`\
`cmake/FindSDL2_ttf.cmake`\
Original: https://github.com/Twinklebear/TwinklebearDev-Lessons; January 19, 2015 - 11:09\
Changes: None\
Author: Will Usher / Twinklebear; https://www.willusher.io \
License: MIT license; https://github.com/Twinklebear/TwinklebearDev-Lessons/blob/9de745276809c22681b21c9144fb613113b74e84/LICENSE.md

## Binary Assets
The various binary assets (images etc) are available under difference licenses, and are listed here.

`resources/ground_grass_gen_08_256.png`\
Original: https://opengameart.org/node/15604; Monday, April 1, 2013 - 23:08\
Changes: Reduced size from 1024 x 1024 to 256 x 256\
Author: para; https://opengameart.org/users/para \
License: CC0 1.0; https://creativecommons.org/publicdomain/zero/1.0/

`resources/player_team_a.png`\
Original: https://sourceforge.net/p/ysoccer/code/ci/master/tree/java/android/assets/images/player/BIG_V.png; Sunday, October 30, 2016 - 07:26\
Original: https://sourceforge.net/p/ysoccer/code/ci/master/tree/java/android/assets/images/player/hairstyles/CURLY_A.png; Saturday, 31 December, 2016 - 16:21\
Changes: Doubled size, removed sprite images to simple running, pasted hair onto player, manually adjusted colours\
Author: Massimo Modica; https://sourceforge.net/u/massimo32/profile \
License: GPL 2 or later; https://sourceforge.net/p/ysoccer/code/ci/master/tree/java/android/assets/docs/readme.htm

`resources/player_team_b.png`\
Original: https://sourceforge.net/p/ysoccer/code/ci/master/tree/java/android/assets/images/player/DOUBLE_STRIPE.png; Sunday, October 30, 2016 - 07:26\
Original: https://sourceforge.net/p/ysoccer/code/ci/master/tree/java/android/assets/images/player/hairstyles/SMOOTH_I.png; Sunday, November 20, 2016 - 15:16\
Changes: Doubled size, removed sprite images to simple running, pasted hair onto player, manually adjusted colours\
Author: Massimo Modica; https://sourceforge.net/u/massimo32/profile \
License: GPL 2 or later; https://sourceforge.net/p/ysoccer/code/ci/master/tree/java/android/assets/docs/readme.htm

`resources/ball.png`\
Original: https://sourceforge.net/p/ysoccer/code/ci/master/tree/java/android/assets/images/ball.png; Thursday, August 29, 2019 - 11:54\
Changes: Doubled size\
Author: Massimo Modica; https://sourceforge.net/u/massimo32/profile \
License: GPL 2 or later; https://sourceforge.net/p/ysoccer/code/ci/master/tree/java/android/assets/docs/readme.htm

`resources/PressStart2P.ttf` \
Original: https://fontlibrary.org/en/font/press-start-2p, press-start-2p.zip; Thursday, November 1, 2012\
Changes: None\
Author: Cody CodeMan38 Boisclair; https://fontlibrary.org/en/member/codeman38 \
License: OFL (SIL Open Font License) Version 1.1; https://scripts.sil.org/OFL

`resources/line_h.png` \
`resources/line_v.png` \
`resources/numbers.png` \
Original source, created in GIMP\
Author: stookie <4303586-stookie@users.noreply.gitlab.com>; copyright (C) 2019\
License: CC BY-SA 4.0; https://creativecommons.org/licenses/by-sa/4.0/

`resources/markers.png` \
`resources/source/markers.svg` \
Original source, created in Inkscape/GIMP\
Author: stookie <4303586-stookie@users.noreply.gitlab.com>; copyright (C) 2019\
License: CC BY-SA 4.0; https://creativecommons.org/licenses/by-sa/4.0/

`resources/goal_bar.png` \
`resources/goal_dot.png` \
`resources/goal_post.png` \
`resources/source/goals.svg` \
Original source, created in Inkscape/GIMP\
Author: stookie <4303586-stookie@users.noreply.gitlab.com>; copyright (C) 2020\
License: CC BY-SA 4.0; https://creativecommons.org/licenses/by-sa/4.0/

`resources/kick_direction.png` \
`resources/kick_power_frame.png` \
`resources/kick_power_level.png` \
`resources/kick_snap_frame.png` \
`resources/kick_snap_level.png` \
`resources/kick_control_level.png` \
`resources/source/kick_control.svg` \
Original source, created in Inkscape/GIMP\
Author: stookie <4303586-stookie@users.noreply.gitlab.com>; copyright (C) 2020\
License: CC BY-SA 4.0; https://creativecommons.org/licenses/by-sa/4.0/

