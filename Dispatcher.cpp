/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "Dispatcher.hpp"

#include "GlobalData.hpp"
#include "ISdlSystem.hpp"
#include "ISystem.hpp"

#include "ball/Data.hpp"
#include "ball/System.hpp"
#include "camera/Data.hpp"
#include "camera/System.hpp"
#include "collision/Data.hpp"
#include "collision/System.hpp"
#include "input/Data.hpp"
#include "input/System.hpp"
#include "players/Data.hpp"
#include "players/System.hpp"
#include "rng/Data.hpp"
#include "rng/System.hpp"
#include "rules/Data.hpp"
#include "rules/System.hpp"
#include "video/Data.hpp"
#include "video/System.hpp"

#include <cstring>
#include <iostream>
#include <map>
#include <type_traits>
#include <utility>


Dispatcher::Dispatcher(double iIterationRate_Hz) :
    _sdlCtx{},
    _systems{},
    _inputSdlSystems{},
    _outputSdlSystems{},
    _readGlobalStateWriteAccess{},
    _readGlobalState{_readGlobalStateWriteAccess},
    _writeGlobalState{}
{
    if (iIterationRate_Hz <= 1.0)
        throw DispatcherException{"Invalid iteration rate"};

    _writeGlobalState._main._iterationRate_Hz = iIterationRate_Hz;
    _writeGlobalState._main._iterationDelay_s = 1.0 / iIterationRate_Hz;
    _writeGlobalState._main._iterationDelay_ms = 1000.0 / iIterationRate_Hz;
    _writeGlobalState._main._timeSinceStart_ms = 0.0;
    _writeGlobalState._main._gameTimeSinceStart_ms = 0.0;

    std::cout << "size of SystemData is " << sizeof(State::Global) << std::endl;
}

Dispatcher::~Dispatcher()
{
}

void Dispatcher::run()
{
    populateSystems();
    initialiseAll();

    Uint32 aIterations = 0;
    Uint32 aStartTime = _sdlCtx.getTimeSinceStartMs();

    while (!_readGlobalState._input._quit)
    {
        Uint32 aDelayTime1 = _sdlCtx.getTimeSinceStartMs();
        _writeGlobalState._main._timeSinceStart_ms = aDelayTime1;
        _writeGlobalState._main._gameTimeSinceStart_ms += _writeGlobalState._main._iterationDelay_ms;

        iterateAll();

        Uint32 aDelayTime2 = _sdlCtx.getTimeSinceStartMs();

        const double& aIterationDelayMs{_writeGlobalState._main._iterationDelay_ms};
        Uint32 aDelay = static_cast<Uint32>(aIterationDelayMs - (aDelayTime2 - aDelayTime1));
        aDelay = std::min(static_cast<Uint32>(aIterationDelayMs + 1.0), std::max(Uint32{1}, aDelay));

        aIterations++;
        _sdlCtx.delayMs(aDelay);
    }

    Uint32 aStopTime = _sdlCtx.getTimeSinceStartMs();

    finaliseAll();

    std::cout << "iterations=" << aIterations << ", time=" << (aStopTime - aStartTime) <<
                 ", frame rate=" << (1000.0 * aIterations / (aStopTime - aStartTime)) << std::endl;
}

void Dispatcher::populateSystems()
{
    // Input systems, using SDL:
    _inputSdlSystems.push_back(SdlSystemDataPair{std::unique_ptr<ISdlSystem>{new Input::System{_sdlCtx}},
                                                 _writeGlobalState._input});

    // Normal systems:
    _systems.push_back(SystemDataPair{std::unique_ptr<ISystem>{new Ball::System{}}, _writeGlobalState._ball});
    _systems.push_back(SystemDataPair{std::unique_ptr<ISystem>{new Camera::System{}}, _writeGlobalState._camera});
    _systems.push_back(SystemDataPair{std::unique_ptr<ISystem>{new Collision::System{}}, _writeGlobalState._collision});
    _systems.push_back(SystemDataPair{std::unique_ptr<ISystem>{new Players::System{}}, _writeGlobalState._players});
    _systems.push_back(SystemDataPair{std::unique_ptr<ISystem>{new RNG::System{}}, _writeGlobalState._rng});
    _systems.push_back(SystemDataPair{std::unique_ptr<ISystem>{new Rules::System{}}, _writeGlobalState._rules});

    // Output systems, using SDL:
    _outputSdlSystems.push_back(SdlSystemDataPair{std::unique_ptr<ISdlSystem>{new Video::System{_sdlCtx}},
                                                  _writeGlobalState._video});
}


void Dispatcher::initialiseAll()
{
    // Copy the current read-write global data buffer, to use as the initial
    // state of the subsequent read-only global data buffer.
    updateGlobalReadState();

    for (auto& aSdlSystem : _inputSdlSystems)
    {
        aSdlSystem.first->initialise(_readGlobalState._main, aSdlSystem.second);
    }

    for (auto& aSystem : _systems)
    {
        aSystem.first->initialise(_readGlobalState._main, aSystem.second);
    }

    for (auto& aSdlSystem : _outputSdlSystems)
    {
        aSdlSystem.first->initialise(_readGlobalState._main, aSdlSystem.second);
    }
}

void Dispatcher::iterateAll()
{
    // Copy the current read-write global data buffer, to use as the initial
    // state of the subsequent read-only global data buffer.
    updateGlobalReadState();

    for (auto& aSdlSystem : _inputSdlSystems)
    {
        aSdlSystem.first->iterate(_readGlobalState, aSdlSystem.second);
    }

    for (auto& aSystem : _systems)
    {
        aSystem.first->iterate(_readGlobalState, aSystem.second);
    }

    for (auto& aSdlSystem : _outputSdlSystems)
    {
        aSdlSystem.first->iterate(_readGlobalState, aSdlSystem.second);
    }
}

void Dispatcher::finaliseAll()
{
    // Copy the current read-write global data buffer, to use as the initial
    // state of the subsequent read-only global data buffer.
    updateGlobalReadState();

    for (auto& aSdlSystem : _inputSdlSystems)
    {
        aSdlSystem.first->finalise(_readGlobalState, aSdlSystem.second);
    }

    for (auto& aSystem : _systems)
    {
        aSystem.first->finalise(_readGlobalState, aSystem.second);
    }

    for (auto& aSdlSystem : _outputSdlSystems)
    {
        aSdlSystem.first->finalise(_readGlobalState, aSdlSystem.second);
    }
}

void Dispatcher::updateGlobalReadState()
{
    _readGlobalStateWriteAccess = _writeGlobalState;
}
