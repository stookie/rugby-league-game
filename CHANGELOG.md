# Change Log

## Next
- Relicense the code: AGPL 3 only
- Display the game state in the panel
- Add options to the panel, like kick type

## DevMilestone06: Forcings Back, Home Brand Edition; 10 Aug 2020
- Players have direction when stationary
- Bot competes very rudimentarily in forcings back
- Drones have very rudimentary positioning
- Kick control (direction/power/snap), for place/drop/long kicks
- Start of rules implementation, first being kick-off
- Bot kicks field goals accurately, and kicks off randomly

## DevMilestone05: Kick Practice With More Mates; 16 Apr 2020
- Multiple players per team
- Draw players and ball sorted by y-position
- Introduce switching control of players, Human and Bot
- Goal posts, bar graphics added
- New collision system, player/ball and goals/ball handled
- Two kick types, normal and drop kicks
- Started keeping score, currently field goals on drop kicks

## DevMilestone04: Kick With A Mate; 11 Feb 2020
- Add bot as AI opposition
- Structure of teams, players and squads, and team names
- Add trivial rules, taking turns to kick, now in 3D
- Formalised (pseudo) Random Number Generation
- Add bot markers, similar to player

## DevMilestone03: Kick In The Park; 17 Dec 2019
- Add the ball, can be possessed by player
- Add player marker, with and without ball possession
- Player can kick the ball, with variable strength
- Camera linked to ball
- Text plumbing, an old-school font, printing in the bar

## DevMilestone02: Jog In The Park; 25 Sep 2019
- Add player, controlled by keyboard
- Animated graphic of player
- Camera linked to player, with deadzone and slew

## DevMilestone01: Panning Camera; 08 Aug 2019
- Basic code structure, including dispatcher and data
- Simple field drawn
- User controls camera directly

