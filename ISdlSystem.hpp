/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_MAIN_ISDL_SYSTEM
#define RL_MAIN_ISDL_SYSTEM


struct ISystemData;
class SdlCtx;

namespace State
{

struct Main;
struct Global;

}


class ISdlSystem
{
public:
    ISdlSystem(SdlCtx& ioSdlCtx) :
        _sdlCtx{ioSdlCtx}
    {
    }

    virtual ~ISdlSystem() = default;

    ISdlSystem(const ISdlSystem&) = delete;
    ISdlSystem& operator=(const ISdlSystem&) = delete;
    ISdlSystem(ISdlSystem&&) = delete;
    ISdlSystem& operator=(ISdlSystem&&) = delete;

    virtual void initialise(const State::Main& iReadOnlyMainData, ISystemData& oSystemData) = 0;
    virtual void iterate(const State::Global& iReadOnlyData, ISystemData& oSystemData) = 0;
    virtual void finalise(const State::Global& iReadOnlyData, ISystemData& oSystemData) = 0;

protected:
    SdlCtx& _sdlCtx;
};

#endif   // RL_MAIN_ISDL_SYSTEM
