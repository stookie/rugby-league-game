/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_PLAYERS_HUMAN
#define RL_PLAYERS_HUMAN


#include "players/Controlled.hpp"

#include "input/Data.hpp"


namespace State
{

struct Main;
struct Global;

}

namespace Players
{

struct Data;
struct GenericData;
struct ControlledData;

}


namespace Players
{

// The Human player class is under direct control from the keyboard.

class Human : public Controlled
{
public:
    Human(Constants::Teams::TeamsType iTeam);

    virtual ~Human() = default;

    Human(const Human&) = delete;
    Human& operator=(const Human&) = delete;
    Human(Human&&) = delete;
    Human& operator=(Human&&) = delete;

    virtual void initialise(const State::Main& iReadOnlyMainData, Data& ioData) override;
    virtual void iterate(const State::Global& iReadOnlyData, Data& ioData) override;
    virtual void finalise(const State::Global& iReadOnlyData, Data& ioData) override;

protected:
    // Control of the movement of the player
    virtual void controlMovement(const State::Global& iReadOnlyData, const ControlledData& ioControlledData) override;

    // Handle the movement of the player
    virtual void handleMovement(const State::Global& iReadOnlyData, GenericData& ioGenericData) override;

    // Control of the players kicks
    virtual void controlKick(const State::Global& iReadOnlyData, const ControlledData& ioControlledData) override;

    // Handle the players kicks
    virtual void handleKick(const State::Global& iReadOnlyData, ControlledData& ioControlledData) override;

    // Control of the swapping between players
    virtual void controlSwap(const State::Global& iReadOnlyData, const Data& ioData) override;

    // Handle the player swap
    virtual bool handleSwap(const State::Global& iReadOnlyData, Data& ioData) override;
};

}

#endif   // RL_PLAYERS_HUMAN
