/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "players/Controlled.hpp"

#include "players/Data.hpp"

#include "GlobalConstants.hpp"
#include "GlobalData.hpp"
#include "Log.hpp"


namespace Players
{

Controlled::Controlled(Constants::Teams::TeamsType iTeam) :
    Generic{iTeam},
    _kickType{Constants::Players::kNoKick},
    _kickAction{},
    _kickActionChanged{},
    _playerSwap{},
    _prevKickType{Constants::Players::kNoKick},
    _quickKickStartTime_ms{},
    _controlledKickState{kNotStarted},
    _controlledKickIncreaseAmount_pIt{0.0},
    _controlledKickDecreaseAmount_pIt{0.0}
{
}

void Controlled::initialise(const State::Main& iReadOnlyMainData, Data& ioData)
{
    Generic::initialise(iReadOnlyMainData, ioData);

    _controlledKickIncreaseAmount_pIt =
        iReadOnlyMainData._iterationDelay_s / Constants::Players::kControlledKickIncreaseTime_s;

    _controlledKickDecreaseAmount_pIt =
        iReadOnlyMainData._iterationDelay_s / Constants::Players::kControlledKickDecreaseTime_s;

    _controlledKickDirectionChangeRate_degpIt =
        Constants::Players::kControlledKickDirectionChangeRate_degps * iReadOnlyMainData._iterationDelay_s;

    resetMovementForKickoff(Constants::Rules::kStartTeamRunningDirection[_team],
                            (Constants::Rules::kStartTeamKickingOff == _team),
                            ioData);
}

void Controlled::iterate(const State::Global& iReadOnlyData, Data& ioData)
{
    if (iReadOnlyData._rules._gameState == Constants::Rules::GameState::kKickOff &&
        iReadOnlyData._rules._gameStateChanged)
    {
        resetMovementForKickoff(iReadOnlyData._rules._directionOfTeam[_team],
                                (iReadOnlyData._rules._possessionTeam == _team),
                                ioData);
    }

    Generic::iterate(iReadOnlyData, ioData);
}

void Controlled::finalise(const State::Global& iReadOnlyData, Data& ioData)
{
    Generic::finalise(iReadOnlyData, ioData);
}

void Controlled::resetMovementForKickoff(Constants::Field::EndType iRunningDirection, bool iInPossession, Data& ioData)
{
    ioData._controlledPlayer[_team] = 0;

    if (iInPossession)
    {
        ioData._controlled[_team]._posX_m = 0.0;
        ioData._controlled[_team]._posY_m = 0.0;

        ioData._controlled[_team]._velX_mps = 0.0;
        ioData._controlled[_team]._velY_mps = 0.0;
    }
    else
    {
        //XYZ HARD CODED
        ioData._controlled[_team]._posX_m = 0.0;
        ioData._controlled[_team]._posY_m = 15.0;
        ioData._controlled[_team]._posY_m *= Constants::Field::kFromEndCoeffY[iRunningDirection];

        ioData._controlled[_team]._velX_mps = 0.0;
        ioData._controlled[_team]._velY_mps = 10.0;
        ioData._controlled[_team]._velY_mps *= Constants::Field::kTowardsEndCoeffY[iRunningDirection];
    }

    ioData._controlled[_team]._directionEW = Constants::Field::DirectionEW::kNone;
    ioData._controlled[_team]._directionNS = (iRunningDirection == Constants::Field::kNorthEnd ?
                                              Constants::Field::DirectionNS::kNorth :
                                              Constants::Field::DirectionNS::kSouth);
}

void Controlled::handleMovement(const State::Global& iReadOnlyData, GenericData& ioGenericData)
{
    Generic::handleMovement(iReadOnlyData, ioGenericData);
}

void Controlled::handleKick(const State::Global& iReadOnlyData, ControlledData& ioControlledData)
{
    if (ioControlledData._kickBall)
    {
        // Kick flag is a one-shot flag
        ioControlledData._kickBall = false;
        ioControlledData._kickType = Constants::Players::kNoKick;
        ioControlledData._kickControlShow = false;
        _frozen = false;
    }
    else if (iReadOnlyData._rules._currentlyPossessed && iReadOnlyData._rules._possessionTeam == _team)
    {
        // Disable the freeze for now
        //_frozen = true;

        if (iReadOnlyData._rules._gameState == Constants::Rules::GameState::kInPlay && (_kickType != _prevKickType))
        {
            // If the kick type changes, reset all intermediate / internal kick variables
            _prevKickType = _kickType;
            handleKickTypeChange();
        }
        else if (iReadOnlyData._rules._gameState == Constants::Rules::GameState::kKickOff)
        {
            if (iReadOnlyData._rules._gameStateChanged)
            {
                handleKickTypeChange();
            }

            _kickType = Constants::Players::kPlaceKick;
            _prevKickType = Constants::Players::kPlaceKick;
            _frozen = true;
        }

        if (_kickType == Constants::Players::kQuickKick)
        {
            handleQuickKick(iReadOnlyData, ioControlledData);
        }
        else if (_kickType == Constants::Players::kDropKick ||
                 _kickType == Constants::Players::kPlaceKick ||
                 _kickType == Constants::Players::kLongKick)
        {
            handleControlledKick(iReadOnlyData, ioControlledData);
        }
    }
    else if (!(iReadOnlyData._rules._currentlyPossessed && iReadOnlyData._rules._possessionTeam == _team) &&
             ioControlledData._releasingBall &&
             !iReadOnlyData._collision._ballPlayerCollision[_team])
    {
        // Releasing flag is reset when the player and ball are no longer colliding
        ioControlledData._releasingBall = false;
    }
}

void Controlled::handleKickTypeChange()
{
    _quickKickStartTime_ms = 0.0;
    _controlledKickState = kNotStarted;
}

void Controlled::handleQuickKick(const State::Global& iReadOnlyData, ControlledData& ioControlledData)
{
    if (_kickActionChanged && _kickAction)
    {
        _quickKickStartTime_ms = iReadOnlyData._main._gameTimeSinceStart_ms;
    }
    else if (_kickActionChanged && !_kickAction)
    {
        using Constants::Players::kQuickKickActionMaxTime_ms;
        using Constants::Players::kQuickKickWeakVel_mps;
        using Constants::Players::kQuickKickStrongDeltaVel_mps;
        using Constants::Players::kQuickKickVerticalFactor;

        // In case the start of the kick action was missed (ie, the action was before the ball was held),
        // then treat the kick as if an instant tap
        if (_quickKickStartTime_ms == 0.0)
            _quickKickStartTime_ms = iReadOnlyData._main._gameTimeSinceStart_ms;

        const double aKickTime_ms = iReadOnlyData._main._gameTimeSinceStart_ms - _quickKickStartTime_ms;
        _quickKickStartTime_ms = 0.0;

        double aKickStrengthFactor = std::min(aKickTime_ms, kQuickKickActionMaxTime_ms) / kQuickKickActionMaxTime_ms;
        aKickStrengthFactor = std::min(std::max(aKickStrengthFactor, 0.0), 1.0);
        double aKickStrength = kQuickKickWeakVel_mps + aKickStrengthFactor * kQuickKickStrongDeltaVel_mps;

        ioControlledData._releasingBall = true;
        ioControlledData._kickBall = true;
        ioControlledData._kickType = _kickType;

        ioControlledData._ballReleaseVelZ_mps = aKickStrength * kQuickKickVerticalFactor;

        // If diagonal, then apportion the power to both x and y directions.
        if (ioControlledData._directionEW != Constants::Field::DirectionEW::kNone &&
            ioControlledData._directionNS != Constants::Field::DirectionNS::kNone)
        {
            aKickStrength *= Constants::Maths::kInvSqrt2;
        }

        if (ioControlledData._directionEW == Constants::Field::DirectionEW::kWest)
            ioControlledData._ballReleaseVelX_mps = -aKickStrength;
        else if (ioControlledData._directionEW == Constants::Field::DirectionEW::kEast)
            ioControlledData._ballReleaseVelX_mps = aKickStrength;
        else
            ioControlledData._ballReleaseVelX_mps = 0.0;

        if (ioControlledData._directionNS == Constants::Field::DirectionNS::kNorth)
            ioControlledData._ballReleaseVelY_mps = -aKickStrength;
        else if (ioControlledData._directionNS == Constants::Field::DirectionNS::kSouth)
            ioControlledData._ballReleaseVelY_mps = aKickStrength;
        else
            ioControlledData._ballReleaseVelY_mps = 0.0;
    }
}

void Controlled::handleControlledKick(const State::Global& iReadOnlyData, ControlledData& ioControlledData)
{
    handleControlledKickAngle(iReadOnlyData, ioControlledData);

    // Returns True when the kick is complete
    if (handleControlledKickStateMachine(iReadOnlyData, ioControlledData))
    {
        handleControlledKickVelocity(iReadOnlyData, ioControlledData);
    }
}

void Controlled::handleControlledKickAngle(const State::Global& iReadOnlyData, ControlledData& ioControlledData)
{
    bool aInitAngles{};

    // For kick-off, initialise the angles only once, when the state changes
    // Otherwise, continue to initialise the angles until the kick action starts
    if (iReadOnlyData._rules._gameState == Constants::Rules::GameState::kKickOff)
        aInitAngles = iReadOnlyData._rules._gameStateChanged;
    else
        aInitAngles = (_controlledKickState == kNotStarted);

    if (aInitAngles)
    {
        double aDirection_deg{0.0};
        int aAxesCount{0};

        // N: -90 + 0 /1 = -90
        // NE: -90 + 0 /2 = -45
        // E: 0 + 0 /1 = 0
        // SE: 90 + 0 /2 = 45
        // S: 90 + 0 /1 = 90
        // SW: 90 + 180 /2 = 135
        // W: 0 + 180 /1 = 180
        // NW: -90 - 180 /2 = -135

        if (ioControlledData._directionNS == Constants::Field::DirectionNS::kNorth)
        {
            aDirection_deg = -90.0;
            aAxesCount = 1;
        }
        else if (ioControlledData._directionNS == Constants::Field::DirectionNS::kSouth)
        {
            aDirection_deg = 90.0;
            aAxesCount = 1;
        }

        if (ioControlledData._directionEW == Constants::Field::DirectionEW::kEast)
        {
            aDirection_deg += 0.0;
            aAxesCount++;
        }
        else if (ioControlledData._directionEW == Constants::Field::DirectionEW::kWest)
        {
            aDirection_deg += (aDirection_deg < 0.0 ? -180.0 : 180.0);
            aAxesCount++;
        }

        ioControlledData._kickControlAngle_deg = aDirection_deg / std::max(1, aAxesCount);
    }
    else
    {
        double aTarget_deg{0.0};
        int aAxesCount{0};

        if (_controlledKickTargetDirectionNS == Constants::Field::DirectionNS::kNorth)
        {
            aTarget_deg = -90.0;
            aAxesCount = 1;
        }
        else if (_controlledKickTargetDirectionNS == Constants::Field::DirectionNS::kSouth)
        {
            aTarget_deg = 90.0;
            aAxesCount = 1;
        }

        if (_controlledKickTargetDirectionEW == Constants::Field::DirectionEW::kEast)
        {
            aTarget_deg += 0.0;
            aAxesCount++;
        }
        else if (_controlledKickTargetDirectionEW == Constants::Field::DirectionEW::kWest)
        {
            aTarget_deg += (aTarget_deg < 0.0 ? -180.0 : 180.0);
            aAxesCount++;
        }

        if (aAxesCount > 0)
        {
            aTarget_deg /= aAxesCount;

            double aDelta_deg = aTarget_deg - ioControlledData._kickControlAngle_deg;

            if (aDelta_deg > 180.0)
                aDelta_deg -= 360.0;
            else if (aDelta_deg < -180.0)
                aDelta_deg += 360.0;

            aDelta_deg = std::min(aDelta_deg, _controlledKickDirectionChangeRate_degpIt);
            aDelta_deg = std::max(aDelta_deg, -_controlledKickDirectionChangeRate_degpIt);
            ioControlledData._kickControlAngle_deg += aDelta_deg;
        }
    }
}

bool Controlled::handleControlledKickStateMachine(const State::Global& iReadOnlyData, ControlledData& ioControlledData)
{
    // States (refer to hpp for enum descriptions):
    // 0:NotStarted, 1:IncreasingPower, 2:IncreasingSwingOnly, DecreasingSwingOnly,
    // 4:DecreasingPower, 5:DecreasingForSnap, 6:EndSnapSet, 7:EndSnapMissed

    // Transitions:
    // 0-1, 1-2, 2-3, 3-5, 5-6 : normal power and snap
    // 0-1, 1-2, 2-3, 3-5, 5-7 : normal power but miss snap and airswing
    // 0-1, 1-4, 4-7 : set power for too long completely and airswing
    // 0-1, 1-4, 4-3, 3-5, 5-6 : set power for too long but let go on way down and snap
    // 0-1, 1-4, 4-3, 3-5, 5-7 : set power for too long but let go on way down but miss snap and airswing

    // Local aliases
    const double& kInc{_controlledKickIncreaseAmount_pIt};
    const double& kDec{_controlledKickDecreaseAmount_pIt};

    // Set to True if the kick is complete, and the release velocity is to be set
    bool aKickComplete{false};

    if (_controlledKickState == kNotStarted)
    {
        ioControlledData._kickControlPower = 0.0;
        ioControlledData._kickControlSnap = 0.0;
        ioControlledData._kickControlIndicator = 0.0;
    }

    if (_controlledKickState == kEndSnapSet)
    {
        TRC(TrcType::kCntKck, "KC Raw Values: ", ioControlledData._kickControlPower, " / ",
            ioControlledData._kickControlSnap, " / ", ioControlledData._kickControlAngle_deg);
        _controlledKickState = kNotStarted;
        aKickComplete = true;
    }
    else if (_kickActionChanged && _kickAction)
    {
        if (_controlledKickState == kNotStarted)
        {
            _controlledKickState = kIncreasingPower;
            ioControlledData._kickControlPower = 0.0;
            ioControlledData._kickControlSnap = 0.0;
            ioControlledData._kickControlIndicator = 0.0;
        }
        else if (_controlledKickState == kDecreasingForSnap)
        {
            _controlledKickState = kEndSnapSet;
            ioControlledData._kickControlSnap = std::abs(ioControlledData._kickControlIndicator);
        }
        else if (_controlledKickState == kIncreasingSwingOnly)
        {
            // Buttons have no impact at this state, but control indicator must continue to update
            ioControlledData._kickControlIndicator = std::min(1.0, ioControlledData._kickControlIndicator + kInc);

            if (ioControlledData._kickControlIndicator == 1.0)
            {
                _controlledKickState = kDecreasingSwingOnly;
            }
        }
        else if (_controlledKickState == kDecreasingSwingOnly)
        {
            // Buttons have no impact at this state, but control indicator must continue to update
            ioControlledData._kickControlIndicator = std::max(0.0, ioControlledData._kickControlIndicator - kDec);

            if (ioControlledData._kickControlIndicator == 0.0)
            {
                _controlledKickState = kDecreasingForSnap;
            }
        }
    }
    else if (!_kickActionChanged && _kickAction)
    {
        if (_controlledKickState == kIncreasingPower)
        {
            ioControlledData._kickControlIndicator = std::min(1.0, ioControlledData._kickControlIndicator + kInc);
            ioControlledData._kickControlPower = ioControlledData._kickControlIndicator;

            if (ioControlledData._kickControlIndicator == 1.0)
            {
                _controlledKickState = kDecreasingPower;
            }
        }
        else if (_controlledKickState == kDecreasingPower)
        {
            ioControlledData._kickControlIndicator = std::max(0.0, ioControlledData._kickControlIndicator - kDec);
            ioControlledData._kickControlPower = ioControlledData._kickControlIndicator;

            if (ioControlledData._kickControlIndicator == 0.0)
            {
                _controlledKickState = kEndSnapSet;
                ioControlledData._kickControlSnap = std::abs(ioControlledData._kickControlIndicator);
            }
        }
        else if (_controlledKickState == kIncreasingSwingOnly)
        {
            // Buttons have no impact at this state, but control indicator must continue to update
            ioControlledData._kickControlIndicator = std::min(1.0, ioControlledData._kickControlIndicator + kInc);

            if (ioControlledData._kickControlIndicator == 1.0)
            {
                _controlledKickState = kDecreasingSwingOnly;
            }
        }
        else if (_controlledKickState == kDecreasingSwingOnly)
        {
            // Buttons have no impact at this state, but control indicator must continue to update
            ioControlledData._kickControlIndicator = std::max(0.0, ioControlledData._kickControlIndicator - kDec);

            if (ioControlledData._kickControlIndicator == 0.0)
            {
                _controlledKickState = kDecreasingForSnap;
            }
        }
        else if (_controlledKickState == kDecreasingForSnap)
        {
            _controlledKickState = kEndSnapSet;
            ioControlledData._kickControlSnap = std::abs(ioControlledData._kickControlIndicator);
        }
    }
    else if (_kickActionChanged && !_kickAction)
    {
        if (_controlledKickState == kIncreasingPower)
        {
            _controlledKickState = kIncreasingSwingOnly;
            ioControlledData._kickControlIndicator = std::min(1.0, ioControlledData._kickControlIndicator + kInc);

            if (ioControlledData._kickControlIndicator == 1.0)
            {
                _controlledKickState = kDecreasingSwingOnly;
            }
        }
        else if (_controlledKickState == kDecreasingPower)
        {
            _controlledKickState = kDecreasingSwingOnly;
            ioControlledData._kickControlIndicator = std::max(0.0, ioControlledData._kickControlIndicator - kDec);

            if (ioControlledData._kickControlIndicator == 0.0)
            {
                _controlledKickState = kEndSnapSet;
                ioControlledData._kickControlSnap = std::abs(ioControlledData._kickControlIndicator);
            }
        }
    }
    else if (!_kickActionChanged && !_kickAction)
    {
        if (_controlledKickState == kIncreasingSwingOnly)
        {
            ioControlledData._kickControlIndicator = std::min(1.0, ioControlledData._kickControlIndicator + kInc);

            if (ioControlledData._kickControlIndicator == 1.0)
            {
                _controlledKickState = kDecreasingSwingOnly;
            }
        }
        else if (_controlledKickState == kDecreasingSwingOnly)
        {
            ioControlledData._kickControlIndicator = std::max(0.0, ioControlledData._kickControlIndicator - kDec);

            if (ioControlledData._kickControlIndicator == 0.0)
            {
                _controlledKickState = kDecreasingForSnap;
            }
        }
        else if (_controlledKickState == kDecreasingForSnap)
        {
            ioControlledData._kickControlIndicator = std::max(-1.0, ioControlledData._kickControlIndicator - kDec);
            ioControlledData._kickControlSnap = std::abs(ioControlledData._kickControlIndicator);

            if (ioControlledData._kickControlIndicator == -1.0)
            {
                _controlledKickState = kEndSnapSet;
                ioControlledData._kickControlSnap = std::abs(ioControlledData._kickControlIndicator);
            }
        }
    }

    // Freeze the controlled players velocity while in the controlled kick process:
    //  At kick-off, from the beginning until the kick is complete
    //  Normally, from the start of the kick control until the kick is complete
    _controlFrozen = !aKickComplete &&
                     ((iReadOnlyData._rules._gameState == Constants::Rules::GameState::kKickOff) ||
                      (_controlledKickState != kNotStarted));

    // The display of the kick control follows the same logic as freezing the player, above
    ioControlledData._kickControlShow = _controlFrozen;

    return aKickComplete;
}

void Controlled::handleControlledKickVelocity(const State::Global& iReadOnlyData, ControlledData& ioControlledData)
{
    double aKickWeakVel_mps{};
    double aKickStrongDeltaVel_mps{};
    double aKickVerticalFactor{};

    switch (_kickType)
    {
        case Constants::Players::kDropKick:
            TRC(TrcType::kCntKck, "KC Type: Drop Kick");
            aKickWeakVel_mps = Constants::Players::kDropKickWeakVel_mps;
            aKickStrongDeltaVel_mps = Constants::Players::kDropKickStrongDeltaVel_mps;
            aKickVerticalFactor = Constants::Players::kDropKickVerticalFactor;
            break;
        case Constants::Players::kPlaceKick:
            TRC(TrcType::kCntKck, "KC Type: Place Kick");
            aKickWeakVel_mps = Constants::Players::kPlaceKickWeakVel_mps;
            aKickStrongDeltaVel_mps = Constants::Players::kPlaceKickStrongDeltaVel_mps;
            aKickVerticalFactor = Constants::Players::kPlaceKickVerticalFactor;
            break;
        case Constants::Players::kLongKick:
            TRC(TrcType::kCntKck, "KC Type: Long Kick");
            aKickWeakVel_mps = Constants::Players::kLongKickWeakVel_mps;
            aKickStrongDeltaVel_mps = Constants::Players::kLongKickStrongDeltaVel_mps;
            aKickVerticalFactor = Constants::Players::kLongKickVerticalFactor;
            break;
        default:
            break;
    }

    ioControlledData._releasingBall = true;
    ioControlledData._kickBall = true;
    ioControlledData._kickType = _kickType;

    double aKickStrengthFactor = std::min(std::max(ioControlledData._kickControlPower, 0.0), 1.0);

    double aKickSnap = (ioControlledData._kickControlSnap * 2.0 - 1.0);   // [0.0, 1.0] -> [-1.0, 1.0]

    // Allow for a small deadzone to make a perfect kick easier
    if (aKickSnap < Constants::Players::kControlledKickSnapDeadzone &&
        aKickSnap > -Constants::Players::kControlledKickSnapDeadzone)
    {
        aKickSnap = 0.0;
    }

    // Use a square function for the snap, near perfect has a much smaller effect than a bad snap
    aKickSnap = std::min(std::max(std::abs(aKickSnap) * aKickSnap, -1.0), 1.0);

    double aKickStrength{};

    if (aKickSnap >= 1.0 || aKickSnap <= -1.0)
    {
        // Completely missed the snap, the kick is an air swing
        aKickStrength = Constants::Players::kControlledKickShankVel_mps;
        ioControlledData._ballReleaseVelZ_mps = aKickStrength * Constants::Players::kControlledKickShankVerticalFactor;
    }
    else
    {
        aKickStrength = aKickWeakVel_mps + aKickStrengthFactor * aKickStrongDeltaVel_mps;
        // The snap impact on the power:
        aKickStrength *= (1.0 - std::abs(aKickSnap));
        ioControlledData._ballReleaseVelZ_mps = aKickStrength * aKickVerticalFactor;
    }

    // The snap impact on the angle:
    double aAngle_deg = ioControlledData._kickControlAngle_deg +
                        Constants::Players::kControlledKickShankDirectionOffset_deg * aKickSnap;

    if (aAngle_deg > 180.0)
        aAngle_deg -= 360.0;
    else if (aAngle_deg < -180.0)
        aAngle_deg += 360.0;

    double aAngle_rad = aAngle_deg * Constants::Maths::kPi / 180.0;
    ioControlledData._ballReleaseVelX_mps = aKickStrength * std::cos(aAngle_rad);
    ioControlledData._ballReleaseVelY_mps = aKickStrength * std::sin(aAngle_rad);
    TRC(TrcType::kCntKck, "KC Mod Values: ", aKickStrengthFactor, "(", aKickStrength, ") / ", aKickSnap, " / ",
        aAngle_deg);
}

bool Controlled::handleSwap(const State::Global& iReadOnlyData, Data& ioData)
{
    if (!_playerSwap || (iReadOnlyData._rules._currentlyPossessed && iReadOnlyData._rules._possessionTeam == _team))
    {
        return false;
    }

    // First, find the nearest player to the ball, not including the currently controlled one
    bool aFirst{true};
    std::size_t aNearestPlayer{0};
    double aNearestDistance2_m2{};   // no point taking sqrt, just using a comparison

    for (std::size_t aPlayer = 0; aPlayer < Constants::Rules::kPlayers; aPlayer++)
    {
        if (aPlayer == ioData._controlledPlayer[_team])
        {
            continue;
        }

        const double aDeltaX_m{iReadOnlyData._ball._posX_m - ioData._players[_team][aPlayer]._posX_m};
        const double aDeltaY_m{iReadOnlyData._ball._posY_m - ioData._players[_team][aPlayer]._posY_m};

        const double aDistance2_m2{aDeltaX_m * aDeltaX_m + aDeltaY_m + aDeltaY_m};

        if (aDistance2_m2 < aNearestDistance2_m2 || aFirst)
        {
            aFirst = false;
            aNearestPlayer = aPlayer;
            aNearestDistance2_m2 = aDistance2_m2;
        }
    }

    // Then, do the actual swap
    ioData._controlledPlayer[_team] = aNearestPlayer;
    const DroneData& aSwappedPlayer{ioData._players[_team][aNearestPlayer]};

    static_assert(GenericData::kGenericDataVersion == 1, "Generic data structure has changed");

    ioData._controlled[_team]._posX_m = aSwappedPlayer._posX_m;
    ioData._controlled[_team]._posY_m = aSwappedPlayer._posY_m;
    ioData._controlled[_team]._velX_mps = aSwappedPlayer._velX_mps;
    ioData._controlled[_team]._velY_mps = aSwappedPlayer._velY_mps;
    ioData._controlled[_team]._directionEW = aSwappedPlayer._directionEW;
    ioData._controlled[_team]._directionNS = aSwappedPlayer._directionNS;

    return true;
}

}   // namespace Players
