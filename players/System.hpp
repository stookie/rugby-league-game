/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_PLAYERS_SYSTEM
#define RL_PLAYERS_SYSTEM


#include "ISystem.hpp"

#include "players/Controlled.hpp"
#include "players/Drone.hpp"

#include "GlobalConstants.hpp"

#include <array>
#include <memory>


struct ISystemData;

namespace State
{

struct Global;

}


namespace Players
{

class System : public ISystem
{
public:
    System();

    virtual ~System() = default;

    System(const System&) = delete;
    System& operator=(const System&) = delete;
    System(System&&) = delete;
    System& operator=(System&&) = delete;

    virtual void initialise(const State::Main& iReadOnlyMainData, ISystemData& oSystemData) override;
    virtual void iterate(const State::Global& iReadOnlyData, ISystemData& oSystemData) override;
    virtual void finalise(const State::Global& iReadOnlyData, ISystemData& oSystemData) override;

private:
    // A Controlled player can be either a Human or a Bot player, and is configured by the global constants.
    // There is one Controlled player per team.
    // Use a pointer to allow polymorphism on type Controlled.
    std::array<std::unique_ptr<Controlled>, Constants::Teams::kNumTeams> _controlled;

    // The Drone players are the non playing players.
    // The Controlled players are also reflected in their corresponding Drone player.
    std::array<std::array<Drone, Constants::Rules::kPlayers>, Constants::Teams::kNumTeams> _drones;
};

}

#endif   // RL_PLAYERS_SYSTEM
