/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "players/System.hpp"

#include "players/Data.hpp"
#include "players/Human.hpp"
#include "players/Bot.hpp"

#include "GlobalConstants.hpp"
#include "GlobalData.hpp"

#include <cassert>
#include <utility>


namespace Players
{

System::System() :
    _controlled{nullptr, nullptr},
    _drones{
        // TeamA (Human) Team:
        static_cast<const Drone&>(Drone{Constants::Teams::kTeamA, 0}),
        static_cast<const Drone&>(Drone{Constants::Teams::kTeamA, 1}),
        // TeamB (Bot) Team:
        static_cast<const Drone&>(Drone{Constants::Teams::kTeamB, 0}),
        static_cast<const Drone&>(Drone{Constants::Teams::kTeamB, 1}) }
{
    static_assert(Constants::Teams::kNumTeams == 2, "Number of teams must be 2");

    static_assert((Constants::Teams::kTeamController[Constants::Teams::kTeamA] != Constants::Teams::kHuman ||
                   Constants::Teams::kTeamController[Constants::Teams::kTeamB] != Constants::Teams::kHuman),
                  "At most one team can be human controlled");

    // Controlled players: Humans and/or Bots (configured by Global Constants)
    for (Constants::Teams::TeamsTypeUT aTeam = Constants::Teams::kTeam0; aTeam < Constants::Teams::kNumTeams; aTeam++)
    {
        switch (Constants::Teams::kTeamController[aTeam])
        {
            case Constants::Teams::kHuman:
                _controlled[aTeam].reset(new Human{static_cast<Constants::Teams::TeamsType>(aTeam)});
                break;
            case Constants::Teams::kBot:
                _controlled[aTeam].reset(new Bot{static_cast<Constants::Teams::TeamsType>(aTeam)});
                break;
        }
    }

    // Drones
    for (std::size_t aTeam = Constants::Teams::kTeam0; aTeam < Constants::Teams::kNumTeams; aTeam++)
    {
        for (std::size_t aPlayer = 0; aPlayer < Constants::Rules::kPlayers; aPlayer++)
        {
            assert(_drones[aTeam][aPlayer]._team == aTeam);
            assert(_drones[aTeam][aPlayer]._number == aPlayer);
        }
    }
}

void System::initialise(const State::Main& iReadOnlyMainData, ISystemData& oSystemData)
{
    Data& aData = static_cast<Data&>(oSystemData);

    aData._controlledPlayer[Constants::Teams::kTeamA] = 0;
    aData._controlledPlayer[Constants::Teams::kTeamB] = 0;

    _controlled[Constants::Teams::kTeamA]->initialise(iReadOnlyMainData, aData);
    _controlled[Constants::Teams::kTeamB]->initialise(iReadOnlyMainData, aData);

    for (auto& aTeam : _drones)
    {
        for (Drone& aPlayer : aTeam)
        {
            aPlayer.initialise(iReadOnlyMainData, aData);
        }
    }
}

void System::iterate(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
    Data& aData = static_cast<Data&>(oSystemData);

    _controlled[Constants::Teams::kTeamA]->iterate(iReadOnlyData, aData);
    _controlled[Constants::Teams::kTeamB]->iterate(iReadOnlyData, aData);

    for (auto& aTeam : _drones)
    {
        for (Drone& aPlayer : aTeam)
        {
            aPlayer.iterate(iReadOnlyData, aData);
        }
    }
}

void System::finalise(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
    Data& aData = static_cast<Data&>(oSystemData);

    _controlled[Constants::Teams::kTeamA]->finalise(iReadOnlyData, aData);
    _controlled[Constants::Teams::kTeamB]->finalise(iReadOnlyData, aData);

    for (auto& aTeam : _drones)
    {
        for (Drone& aPlayer : aTeam)
        {
            aPlayer.finalise(iReadOnlyData, aData);
        }
    }
}

}   // namespace Players
