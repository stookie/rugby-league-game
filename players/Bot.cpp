/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "players/Bot.hpp"

#include "players/Data.hpp"

#include "GlobalConstants.hpp"
#include "GlobalData.hpp"
#include "Log.hpp"

#include <cmath>


// Anonymous namespace
namespace
{

constexpr double kNan_deg{999.9};

}   // Anonymous namespace


namespace Players
{

Bot::Bot(Constants::Teams::TeamsType iTeam) :
    Controlled{iTeam},
    _angle_deg{kNan_deg},
    _oldAngle_deg{kNan_deg},
    _targetX_m{},
    _targetY_m{},
    _targetFixedSet{},
    _prevKickAction{},
    _kickCountDownStarted{},
    _kickCountDown_ms{},
    _runningKickState{kNotStarted}
{
}

void Bot::initialise(const State::Main& iReadOnlyMainData, Data& ioData)
{
    Controlled::initialise(iReadOnlyMainData, ioData);
}

void Bot::iterate(const State::Global& iReadOnlyData, Data& ioData)
{
    Controlled::iterate(iReadOnlyData, ioData);

    controlSwap(iReadOnlyData, ioData);

    handleSwap(iReadOnlyData, ioData);

    controlMovement(iReadOnlyData, ioData._controlled[_team]);

    handleMovement(iReadOnlyData, ioData._controlled[_team]);

    controlKick(iReadOnlyData, ioData._controlled[_team]);

    handleKick(iReadOnlyData, ioData._controlled[_team]);
}

void Bot::finalise(const State::Global& iReadOnlyData, Data& ioData)
{
    Controlled::finalise(iReadOnlyData, ioData);
}

void Bot::controlMovement(const State::Global& iReadOnlyData, const ControlledData& ioControlledData)
{
    if ((iReadOnlyData._rules._currentlyPossessed && iReadOnlyData._rules._possessionTeam != _team) ||
        (!iReadOnlyData._rules._currentlyPossessed && iReadOnlyData._rules._possessionTeam == _team))
    {
        // Bot doesnt move if Human is in possession, or Bot has released the ball
        _moveEW = Constants::Field::DirectionEW::kNone;
        _moveNS = Constants::Field::DirectionNS::kNone;
        _angle_deg = kNan_deg;
        _oldAngle_deg = kNan_deg;
        _targetX_m = 0.0;
        _targetY_m = 0.0;
        _targetFixedSet = false;
    }
    else
    {
        // Bot chases the ball if Human has released the ball,
        //  or faces towards the goals if in possession.
        const bool aInPossession{iReadOnlyData._rules._currentlyPossessed &&
                                 iReadOnlyData._rules._possessionTeam == _team};

        if (aInPossession)
        {
            if (!_targetFixedSet)
            {
                if (iReadOnlyData._rules._gameState == Constants::Rules::GameState::kKickOff)
                {
                    double aRnd = iReadOnlyData._rng._uniReal[Constants::RNG::kUR1];
                    aRnd *= 100.0;
                    _targetX_m = (std::floor(aRnd) / 100.0 - 0.5) * Constants::Field::kWidth_m;

                    //XYZ HARD CODED 10
                    constexpr double kRangeY = Constants::Field::kLength_m / 2.0 + Constants::Field::kIngoal_m - 10.0;

                    aRnd -= std::floor(aRnd);
                    aRnd *= 100.0;
                    _targetY_m = (std::floor(aRnd) / 100.0 * kRangeY + 10.0) * //XYZ HARD CODED 10
                                 Constants::Field::kTowardsEndCoeffY[iReadOnlyData._rules._directionOfTeam[_team]];

                    //XYZ Need to support the power, currently hard coded to 0.8
                    TRC(TrcType::kCntKck, "BKC: Kickoff Target set with random ",
                        iReadOnlyData._rng._uniReal[Constants::RNG::kUR1], "; target X = ", _targetX_m,
                        ", target Y = ", _targetY_m);
                }
                else
                {
                    _targetX_m = 0.0;
                    _targetY_m = Constants::Field::kLength_m / 2.0 *
                                 Constants::Field::kTowardsEndCoeffY[iReadOnlyData._rules._directionOfTeam[_team]];
                }

                _targetFixedSet = true;
            }
        }
        else
        {
            _targetX_m = iReadOnlyData._ball._posX_m;
            _targetY_m = iReadOnlyData._ball._posY_m;
            _targetFixedSet = false;
        }

        double aDeltaX_m = _targetX_m - ioControlledData._posX_m;
        double aDeltaY_m = _targetY_m - ioControlledData._posY_m;

        double aDistance_m = std::sqrt(aDeltaX_m * aDeltaX_m + aDeltaY_m * aDeltaY_m);
        double aAngle_rad = std::atan2(aDeltaY_m, aDeltaX_m);   // -Pi .. Pi
        _angle_deg = (aAngle_rad * 180.0) / Constants::Maths::kPi;   // -180 .. 180

        if (aDistance_m < Constants::Players::kBotChaseTargetRange_m)
        {
            _moveEW = Constants::Field::DirectionEW::kNone;
            _moveNS = Constants::Field::DirectionNS::kNone;
            _angle_deg = kNan_deg;
            _oldAngle_deg = kNan_deg;
        }
        else if (std::fabs(_angle_deg - _oldAngle_deg) > Constants::Players::kBotChaseAngleDelta_deg)
        {
            if (_angle_deg <= -112.5 || _angle_deg >= 112.5)
                _moveEW = Constants::Field::DirectionEW::kWest;
            else if (_angle_deg <= 67.5 && _angle_deg >= -67.5)
                _moveEW = Constants::Field::DirectionEW::kEast;
            else
                _moveEW = Constants::Field::DirectionEW::kNone;

            if (_angle_deg >= 22.5 && _angle_deg <= 157.5)
                _moveNS = Constants::Field::DirectionNS::kSouth;
            else if (_angle_deg <= -22.5 && _angle_deg >= -157.5)
                _moveNS = Constants::Field::DirectionNS::kNorth;
            else
                _moveNS = Constants::Field::DirectionNS::kNone;

            _oldAngle_deg = _angle_deg;
        }
    }
}

void Bot::handleMovement(const State::Global& iReadOnlyData, GenericData& ioGenericData)
{
    Controlled::handleMovement(iReadOnlyData, ioGenericData);
}

void Bot::controlKick(const State::Global& iReadOnlyData, const ControlledData& ioControlledData)
{
    TRC(TrcType::kCntKck, "BKC: controlKick(): ", _kickAction, " / ", _prevKickAction, "; ", _kickType);
    _prevKickAction = _kickAction;

    if (iReadOnlyData._rules._currentlyPossessed && iReadOnlyData._rules._possessionTeam == _team)
    {
        const double aDeltaX_m = _targetX_m - ioControlledData._posX_m;
        const double aDeltaY_m = _targetY_m - ioControlledData._posY_m;
        const double aDistance2_m2 = aDeltaX_m * aDeltaX_m + aDeltaY_m * aDeltaY_m;

        if (iReadOnlyData._rules._gameState == Constants::Rules::GameState::kInPlay &&
            _kickType == Constants::Players::kNoKick)
        {
            if (aDistance2_m2 > 2025.0)   //XYZ HARD CODE : 45^2
                _kickType = Constants::Players::kQuickKick;
            else
                _kickType = Constants::Players::kDropKick;
        }

        //XYZ TEMPORARILY USE DROP KICK LOGIC FOR PLACE KICK:
        if (_kickType == Constants::Players::kDropKick || _kickType == Constants::Players::kPlaceKick)
            controlFieldGoalKick(iReadOnlyData, ioControlledData);
        else if (_kickType == Constants::Players::kQuickKick)
            controlQuickKick(iReadOnlyData, ioControlledData);
    }
    else
    {
        _kickCountDownStarted = false;
        _kickCountDown_ms = 0.0;
        _kickAction = false;
        _kickType = Constants::Players::kNoKick;
        _runningKickState = kNotStarted;
    }

    _kickActionChanged = _prevKickAction != _kickAction;
}

void Bot::handleKick(const State::Global& iReadOnlyData, ControlledData& ioControlledData)
{
    Controlled::handleKick(iReadOnlyData, ioControlledData);
}

void Bot::controlSwap(const State::Global& iReadOnlyData, const Data& ioData)
{
    _playerSwap = false;

    if (!iReadOnlyData._rules._currentlyPossessed && iReadOnlyData._rules._possessionTeam != _team)
    {
        const double aBotDeltaX_m{iReadOnlyData._ball._posX_m - ioData._controlled[_team]._posX_m};
        const double aBotDeltaY_m{iReadOnlyData._ball._posY_m - ioData._controlled[_team]._posY_m};
        const double aBotDistance_m{std::sqrt(aBotDeltaX_m * aBotDeltaX_m + aBotDeltaY_m * aBotDeltaY_m)};

        for (std::size_t aPlayer = 0; aPlayer < Constants::Rules::kPlayers; aPlayer++)
        {
            if (aPlayer == ioData._controlledPlayer[_team])
            {
                continue;
            }

            const double aPlrDeltaX_m{iReadOnlyData._ball._posX_m - ioData._players[_team][aPlayer]._posX_m};
            const double aPlrDeltaY_m{iReadOnlyData._ball._posY_m - ioData._players[_team][aPlayer]._posY_m};
            const double aPlrDistance_m{std::sqrt(aPlrDeltaX_m * aPlrDeltaX_m + aPlrDeltaY_m * aPlrDeltaY_m)};

            if (aPlrDistance_m + Constants::Players::kBotSwapRange_m < aBotDistance_m)
            {
                _playerSwap = true;
                break;
            }
        }
    }
}

bool Bot::handleSwap(const State::Global& iReadOnlyData, Data& ioData)
{
    return Controlled::handleSwap(iReadOnlyData, ioData);
}

void Bot::controlQuickKick(const State::Global& iReadOnlyData, const ControlledData& ioControlledData)
{
    TRC(TrcType::kCntKck, "BKC: controlQuickKick()");

    if (!_kickCountDownStarted)
    {
        _kickCountDownStarted = true;
        _kickCountDown_ms = (iReadOnlyData._rng._uniReal[Constants::RNG::kUR1] * 2.0 + 1.0) * 1000.0; //XYZ CONSTS
        _kickAction = true;
    }
    else
    {
        _kickCountDown_ms -= iReadOnlyData._main._iterationDelay_ms;
        _kickAction = true;
    }

    if (_kickCountDown_ms < 0.0)
    {
        _kickCountDown_ms = 0.0;
        _kickCountDownStarted = false;
        _kickAction = false;
    }
}

void Bot::controlFieldGoalKick(const State::Global& iReadOnlyData, const ControlledData& ioControlledData)
{
    if (_runningKickState == kNotStarted)
    {
        TRC(TrcType::kCntKck, "BKC: Not Started");
        _runningKickState = kWaiting;
        _kickAction = false;
        _kickCountDownStarted = true;

        if (iReadOnlyData._rules._gameState == Constants::Rules::GameState::kKickOff)
            _kickCountDown_ms = Constants::Players::kBotRestartDelay_ms;
        else
            _kickCountDown_ms = 0.0;
    }
    else if (_runningKickState == kWaiting)
    {
        TRC(TrcType::kCntKck, "BKC: Waiting");
        _kickAction = false;
        _kickCountDown_ms -= iReadOnlyData._main._iterationDelay_ms;

        if (_kickCountDown_ms < 0.0)
        {
            TRC(TrcType::kCntKck, "BKC: Waiting, Times Up");
            _runningKickState = kSetPower;
            _kickAction = true;
            _kickCountDown_ms = 0.0;
            _kickCountDownStarted = false;
        }
    }
    else if (_runningKickState == kSetPower)
    {
        TRC(TrcType::kCntKck, "BKC: Set Power");
        _kickAction = true;

        if (ioControlledData._kickControlPower > Constants::Players::kBotFieldGoalPowerLevel)
        {
            TRC(TrcType::kCntKck, "BKC: Set Power > ", Constants::Players::kBotFieldGoalPowerLevel);
            _kickAction = false;
            _runningKickState = kSetSnap;
        }
    }
    else if (_runningKickState == kSetSnap)
    {
        TRC(TrcType::kCntKck, "BKC: Snap");
        _kickAction = false;

        if (ioControlledData._kickControlSnap > Constants::Players::kBotFieldGoalSnapLevel)
        {
            TRC(TrcType::kCntKck, "BKC: Snap > ", Constants::Players::kBotFieldGoalSnapLevel);
            _kickAction = true;
            _runningKickState = kKicked;
        }
    }
    else if (_runningKickState == kKicked)
    {
        TRC(TrcType::kCntKck, "BKC: Kicked");
        _kickAction = false;
    }

    if (ioControlledData._kickControlShow && (_runningKickState == kSetPower || _runningKickState == kSetSnap))
    {
        double aDelta_deg = _angle_deg - ioControlledData._kickControlAngle_deg;

        if (aDelta_deg > 180.0)
            aDelta_deg -= 360.0;
        else if (aDelta_deg < -180.0)
            aDelta_deg += 360.0;

        const bool aClockwise{aDelta_deg > 0.0};

        if (std::fabs(aDelta_deg) < 1.5 || _angle_deg == kNan_deg)//XYZ HARD CODE
        {
            _controlledKickTargetDirectionEW = Constants::Field::DirectionEW::kNone;
            _controlledKickTargetDirectionNS = Constants::Field::DirectionNS::kNone;
        }
        else if ((_angle_deg >= -180.0 && _angle_deg < -135.0 && aClockwise) ||
                 (_angle_deg >= 135.0 && _angle_deg <= 180.0 && aClockwise) ||
                 (_angle_deg >= -45.0 && _angle_deg < 45.0 && !aClockwise))
        {
            _controlledKickTargetDirectionEW = Constants::Field::DirectionEW::kNone;
            _controlledKickTargetDirectionNS = Constants::Field::DirectionNS::kNorth;
        }
        else if ((_angle_deg >= -45.0 && _angle_deg < 45.0 && aClockwise) ||
                 (_angle_deg >= -180.0 && _angle_deg < -135.0 && !aClockwise) ||
                 (_angle_deg >= 135.0 && _angle_deg <= 180.0 && !aClockwise))
        {
            _controlledKickTargetDirectionEW = Constants::Field::DirectionEW::kNone;
            _controlledKickTargetDirectionNS = Constants::Field::DirectionNS::kSouth;
        }
        else if ((_angle_deg >= -135.0 && _angle_deg < -45.0 && aClockwise) ||
                 (_angle_deg >= 45.0 && _angle_deg < 135.0 && !aClockwise))
        {
            _controlledKickTargetDirectionEW = Constants::Field::DirectionEW::kEast;
            _controlledKickTargetDirectionNS = Constants::Field::DirectionNS::kNone;
        }
        else if ((_angle_deg >= 45.0 && _angle_deg < 135.0 && aClockwise) ||
                 (_angle_deg >= -135.0 && _angle_deg < -45.0 && !aClockwise))
        {
            _controlledKickTargetDirectionEW = Constants::Field::DirectionEW::kWest;
            _controlledKickTargetDirectionNS = Constants::Field::DirectionNS::kNone;
        }
    }
    else
    {
        _controlledKickTargetDirectionEW = Constants::Field::DirectionEW::kNone;
        _controlledKickTargetDirectionNS = Constants::Field::DirectionNS::kNone;
    }
}

}   // namespace Players
