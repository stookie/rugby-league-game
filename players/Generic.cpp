/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "players/Generic.hpp"

#include "players/Data.hpp"

#include "GlobalConstants.hpp"
#include "GlobalData.hpp"


// Anonymous namespace
namespace
{

constexpr double kCoeffEW[3] = {0.0, 1.0, -1.0};   // None, East, West; checked in constructor
constexpr double kCoeffNS[3] = {0.0, 1.0, -1.0};   // None, South, North; checked in constructor

}   // Anonymous namespace


namespace Players
{

Generic::Generic(Constants::Teams::TeamsType iTeam) :
    _team{iTeam},
    _moveEW{Constants::Field::DirectionEW::kNone},
    _moveNS{Constants::Field::DirectionNS::kNone},
    _frozen{},
    _controlFrozen{},
    _deltaAccel_mpspIt{},
    _deltaDecel_mpspIt{}
{
    static_assert(kCoeffEW[static_cast<unsigned char>(Constants::Field::DirectionEW::kNone)] == 0.0 &&
                  kCoeffEW[static_cast<unsigned char>(Constants::Field::DirectionEW::kEast)] == 1.0 &&
                  kCoeffEW[static_cast<unsigned char>(Constants::Field::DirectionEW::kWest)] == -1.0,
                  "Invalid EW movement coefficient");

    static_assert(kCoeffNS[static_cast<unsigned char>(Constants::Field::DirectionNS::kNone)] == 0.0 &&
                  kCoeffNS[static_cast<unsigned char>(Constants::Field::DirectionNS::kSouth)] == 1.0 &&
                  kCoeffNS[static_cast<unsigned char>(Constants::Field::DirectionNS::kNorth)] == -1.0,
                  "Invalid NS movement coefficient");
}

void Generic::initialise(const State::Main& iReadOnlyMainData, Data& ioData)
{
    _deltaAccel_mpspIt = Constants::Players::kAccel_mps2 * iReadOnlyMainData._iterationDelay_s;
    _deltaDecel_mpspIt = Constants::Players::kDecel_mps2 * iReadOnlyMainData._iterationDelay_s;
}

void Generic::iterate(const State::Global& iReadOnlyData, Data& ioData)
{
}

void Generic::finalise(const State::Global& iReadOnlyData, Data& ioData)
{
}

void Generic::handleMovement(const State::Global& iReadOnlyData, GenericData& ioGenericData)
{
    // Speed maintained on direction changes
    // 8 directions only (ie no ENE)
    // Speed capped
    // Decelerate to zero on no keys

    double aSpeed_mps{std::sqrt(ioGenericData._velX_mps * ioGenericData._velX_mps +
                                ioGenericData._velY_mps * ioGenericData._velY_mps)};

    bool aNoDirection{(_moveEW == Constants::Field::DirectionEW::kNone &&
                       _moveNS == Constants::Field::DirectionNS::kNone)};

    if (_frozen || _controlFrozen)
    {
        if (_frozen)
        {
            // Location is fixed, so velocity is zero
            aSpeed_mps = 0.0;
        }

        if (_controlFrozen)
        {
            // No change in speed or direction so use previous direction
            _moveEW = ioGenericData._directionEW;
            _moveNS = ioGenericData._directionNS;
            aNoDirection = false;
        }
    }
    else if (aNoDirection)
    {
        aSpeed_mps -= _deltaDecel_mpspIt;

        // Necessary for deceleration in the direction of velocity
        if (ioGenericData._velX_mps > 0.0)
            _moveEW = Constants::Field::DirectionEW::kEast;
        else if (ioGenericData._velX_mps < 0.0)
            _moveEW = Constants::Field::DirectionEW::kWest;

        if (ioGenericData._velY_mps > 0.0)
            _moveNS = Constants::Field::DirectionNS::kSouth;
        else if (ioGenericData._velY_mps < 0.0)
            _moveNS = Constants::Field::DirectionNS::kNorth;
    }
    else
    {
        aSpeed_mps += _deltaAccel_mpspIt;
    }

    if (_moveEW == Constants::Field::DirectionEW::kEast)
    {
        ioGenericData._directionEW = Constants::Field::DirectionEW::kEast;
    }
    else if (_moveEW == Constants::Field::DirectionEW::kWest)
    {
        ioGenericData._directionEW = Constants::Field::DirectionEW::kWest;
    }
    else if (aSpeed_mps > 0.0 || !aNoDirection)
    {
        // Only reset the direction if the speed is not zero or a direction has been set.
        // If speed is zero with no direction set, the previous value should be retained.
        ioGenericData._directionEW = Constants::Field::DirectionEW::kNone;
    }

    if (_moveNS == Constants::Field::DirectionNS::kSouth)
    {
        ioGenericData._directionNS = Constants::Field::DirectionNS::kSouth;
    }
    else if (_moveNS == Constants::Field::DirectionNS::kNorth)
    {
        ioGenericData._directionNS = Constants::Field::DirectionNS::kNorth;
    }
    else if (aSpeed_mps > 0.0 || !aNoDirection)
    {
        // Only reset the direction if the speed is not zero or a direction has been set.
        // If speed is zero with no direction set, the previous value should be retained.
        ioGenericData._directionNS = Constants::Field::DirectionNS::kNone;
    }

    const bool aDiagonal{!(_moveEW == Constants::Field::DirectionEW::kNone ||
                           _moveNS == Constants::Field::DirectionNS::kNone)};

    aSpeed_mps = std::max(std::min(aSpeed_mps, Constants::Players::kMaxSpeed_mps), 0.0);

    ioGenericData._velX_mps = aSpeed_mps *
                              kCoeffEW[static_cast<unsigned char>(_moveEW)] *
                              (aDiagonal ? Constants::Maths::kInvSqrt2 : 1.0);

    ioGenericData._velY_mps = aSpeed_mps *
                              kCoeffNS[static_cast<unsigned char>(_moveNS)] *
                              (aDiagonal ? Constants::Maths::kInvSqrt2 : 1.0);

    const double& aLimX{Constants::Players::kFieldLimitWidth_m};
    ioGenericData._posX_m += ioGenericData._velX_mps * iReadOnlyData._main._iterationDelay_s;
    ioGenericData._posX_m = std::max(std::min(ioGenericData._posX_m, aLimX), -aLimX);

    const double& aLimY{Constants::Players::kFieldLimitLength_m};
    ioGenericData._posY_m += ioGenericData._velY_mps * iReadOnlyData._main._iterationDelay_s;
    ioGenericData._posY_m = std::max(std::min(ioGenericData._posY_m, aLimY), -aLimY);
}

}   // namespace Players
