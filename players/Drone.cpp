/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "players/Drone.hpp"

#include "players/Data.hpp"

#include "GlobalData.hpp"


// Anonymous namespace
namespace
{

constexpr double kNan_deg{999.9};

}   // Anonymous namespace


namespace Players
{

Drone::Drone(Constants::Teams::TeamsType iTeam, std::size_t iNumber) :
    Generic{iTeam},
    _number{iNumber},
    _oldAngle_deg{kNan_deg},
    _targetX_m{},
    _targetY_m{}
{
}

void Drone::initialise(const State::Main& iReadOnlyMainData, Data& ioData)
{
    Generic::initialise(iReadOnlyMainData, ioData);

    resetMovementForKickoff(Constants::Rules::kStartTeamRunningDirection[_team],
                            (Constants::Rules::kStartTeamKickingOff == _team),
                            ioData);
}

void Drone::iterate(const State::Global& iReadOnlyData, Data& ioData)
{
    if (iReadOnlyData._rules._gameState == Constants::Rules::GameState::kKickOff &&
        iReadOnlyData._rules._gameStateChanged)
    {
        resetMovementForKickoff(iReadOnlyData._rules._directionOfTeam[_team],
                                (iReadOnlyData._rules._possessionTeam == _team),
                                ioData);
    }

    Generic::iterate(iReadOnlyData, ioData);

    if (_number == ioData._controlledPlayer[_team])
    {
        static_assert(GenericData::kGenericDataVersion == 1, "Generic data structure has changed");

        ioData._players[_team][_number]._posX_m = ioData._controlled[_team]._posX_m;
        ioData._players[_team][_number]._posY_m = ioData._controlled[_team]._posY_m;
        ioData._players[_team][_number]._velX_mps = ioData._controlled[_team]._velX_mps;
        ioData._players[_team][_number]._velY_mps = ioData._controlled[_team]._velY_mps;
        ioData._players[_team][_number]._directionEW = ioData._controlled[_team]._directionEW;
        ioData._players[_team][_number]._directionNS = ioData._controlled[_team]._directionNS;
    }
    else
    {
        controlMovement(iReadOnlyData, static_cast<DroneData&>(ioData._players[_team][_number]));

        handleMovement(iReadOnlyData, static_cast<GenericData&>(ioData._players[_team][_number]));
    }
}

void Drone::finalise(const State::Global& iReadOnlyData, Data& ioData)
{
    Generic::finalise(iReadOnlyData, ioData);
}

void Drone::resetMovementForKickoff(Constants::Field::EndType iRunningDirection, bool iInPossession, Data& ioData)
{
    //XYZ HARD CODED

    ioData._players[_team][_number]._posX_m = 5.0 + 5.0 * _number;
    ioData._players[_team][_number]._posY_m = 5.0 + 5.0 * _number;
    ioData._players[_team][_number]._velX_mps = 0.0;
    ioData._players[_team][_number]._velY_mps = 0.0;

    ioData._players[_team][_number]._posY_m *= Constants::Field::kFromEndCoeffY[iRunningDirection];
    ioData._players[_team][_number]._velY_mps *= Constants::Field::kTowardsEndCoeffY[iRunningDirection];

    ioData._players[_team][_number]._directionEW = Constants::Field::DirectionEW::kNone;

    if (iRunningDirection == Constants::Field::kNorthEnd)
        ioData._players[_team][_number]._directionNS = Constants::Field::DirectionNS::kNorth;
    else
        ioData._players[_team][_number]._directionNS = Constants::Field::DirectionNS::kSouth;
}

void Drone::controlMovement(const State::Global& iReadOnlyData, DroneData& ioDroneData)
{
    if (iReadOnlyData._rules._currentlyPossessed && iReadOnlyData._rules._possessionTeam != _team)
    {
        // If other team is in possession, stand in front (temporary proof of concept)

        constexpr double kDroneOffsetY_m{15.0};   //XYZ HARD CODED

        _targetX_m = iReadOnlyData._ball._posX_m;
        _targetY_m = iReadOnlyData._ball._posY_m;

        _targetY_m += kDroneOffsetY_m * Constants::Field::kFromEndCoeffY[iReadOnlyData._rules._directionOfTeam[_team]];

        if (std::abs(_targetY_m) > Constants::Field::kLength_m / 2.0)
        {
            // Stand on the goal line
            _targetY_m = Constants::Field::kLength_m / 2.0 *
                         Constants::Field::kFromEndCoeffY[iReadOnlyData._rules._directionOfTeam[_team]];
        }

        double aDeltaX_m = _targetX_m - ioDroneData._posX_m;
        double aDeltaY_m = _targetY_m - ioDroneData._posY_m;

        double aDistance_m = std::sqrt(aDeltaX_m * aDeltaX_m + aDeltaY_m * aDeltaY_m);
        double aAngle_rad = std::atan2(aDeltaY_m, aDeltaX_m);   // -Pi .. Pi
        double aAngle_deg = (aAngle_rad * 180.0) / Constants::Maths::kPi;   // -180 .. 180

        if (aDistance_m < Constants::Players::kBotChaseTargetRange_m)//XYZ
        {
            _moveEW = Constants::Field::DirectionEW::kNone;
            _moveNS = Constants::Field::DirectionNS::kNone;
            _oldAngle_deg = kNan_deg;
        }
        else if (std::fabs(aAngle_deg - _oldAngle_deg) > Constants::Players::kBotChaseAngleDelta_deg)
        {
            if (aAngle_deg <= -112.5 || aAngle_deg >= 112.5)
                _moveEW = Constants::Field::DirectionEW::kWest;
            else if (aAngle_deg <= 67.5 && aAngle_deg >= -67.5)
                _moveEW = Constants::Field::DirectionEW::kEast;
            else
                _moveEW = Constants::Field::DirectionEW::kNone;

            if (aAngle_deg >= 22.5 && aAngle_deg <= 157.5)
                _moveNS = Constants::Field::DirectionNS::kSouth;
            else if (aAngle_deg <= -22.5 && aAngle_deg >= -157.5)
                _moveNS = Constants::Field::DirectionNS::kNorth;
            else
                _moveNS = Constants::Field::DirectionNS::kNone;

            _oldAngle_deg = aAngle_deg;
        }
    }
    else
    {
        // Do nothing at the moment
        _moveEW = Constants::Field::DirectionEW::kNone;
        _moveNS = Constants::Field::DirectionNS::kNone;
    }
}

void Drone::handleMovement(const State::Global& iReadOnlyData, GenericData& ioGenericData)
{
    Generic::handleMovement(iReadOnlyData, ioGenericData);
}

}   // namespace Players
