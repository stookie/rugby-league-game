/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_PLAYERS_GENERIC
#define RL_PLAYERS_GENERIC


#include "GlobalConstants.hpp"


namespace State
{

struct Main;
struct Global;

}

namespace Players
{

struct Data;
struct GenericData;
class System;

}


namespace Players
{

// The Generic player class is the partially abstract base of all player classes.

class Generic
{
public:
    Generic(Constants::Teams::TeamsType iTeam);

    virtual ~Generic() = default;

    Generic(const Generic&) = default;   // Used for constructing in arrays
    Generic& operator=(const Generic&) = delete;
    Generic(Generic&&) = delete;
    Generic& operator=(Generic&&) = delete;

    virtual void initialise(const State::Main& iReadOnlyMainData, Data& ioData);
    virtual void iterate(const State::Global& iReadOnlyData, Data& ioData);
    virtual void finalise(const State::Global& iReadOnlyData, Data& ioData);

    friend class System;

protected:
    // Handle the movement of the player
    virtual void handleMovement(const State::Global& iReadOnlyData, GenericData& ioGenericData);

    const Constants::Teams::TeamsType _team;   // which team the player is assigned to

    Constants::Field::DirectionEW _moveEW;
    Constants::Field::DirectionNS _moveNS;

    bool _frozen;   // if set the player is frozen in movement
    bool _controlFrozen;   // if set the player control is frozen, movement is constant

private:
    double _deltaAccel_mpspIt;   // m per sec per It
    double _deltaDecel_mpspIt;   // m per sec per It
};

}

#endif   // RL_PLAYERS_GENERIC
