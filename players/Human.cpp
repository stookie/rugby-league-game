/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "players/Human.hpp"

#include "players/Data.hpp"

#include "GlobalConstants.hpp"
#include "GlobalData.hpp"


namespace Players
{

Human::Human(Constants::Teams::TeamsType iTeam) :
    Controlled{iTeam}
{
}

void Human::initialise(const State::Main& iReadOnlyMainData, Data& ioData)
{
    Controlled::initialise(iReadOnlyMainData, ioData);
}

void Human::iterate(const State::Global& iReadOnlyData, Data& ioData)
{
    Controlled::iterate(iReadOnlyData, ioData);

    controlSwap(iReadOnlyData, ioData);

    handleSwap(iReadOnlyData, ioData);

    controlMovement(iReadOnlyData, ioData._controlled[_team]);

    handleMovement(iReadOnlyData, ioData._controlled[_team]);

    controlKick(iReadOnlyData, ioData._controlled[_team]);

    handleKick(iReadOnlyData, ioData._controlled[_team]);
}

void Human::finalise(const State::Global& iReadOnlyData, Data& ioData)
{
    Controlled::finalise(iReadOnlyData, ioData);
}

void Human::controlMovement(const State::Global& iReadOnlyData, const ControlledData& ioControlledData)
{
    if (iReadOnlyData._input._keyIsPressed[Input::Data::kUp] ==
        iReadOnlyData._input._keyIsPressed[Input::Data::kDown])
    {
        // Both buttons are held, or neither is held
        _moveNS = Constants::Field::DirectionNS::kNone;
    }
    else if (iReadOnlyData._input._keyIsPressed[Input::Data::kUp])
    {
        _moveNS = Constants::Field::DirectionNS::kNorth;
    }
    else if (iReadOnlyData._input._keyIsPressed[Input::Data::kDown])
    {
        _moveNS = Constants::Field::DirectionNS::kSouth;
    }

    if (iReadOnlyData._input._keyIsPressed[Input::Data::kLeft] ==
        iReadOnlyData._input._keyIsPressed[Input::Data::kRight])
    {
        // Both buttons are held, or neither is held
        _moveEW = Constants::Field::DirectionEW::kNone;
    }
    else if (iReadOnlyData._input._keyIsPressed[Input::Data::kLeft])
    {
        _moveEW = Constants::Field::DirectionEW::kWest;
    }
    else if (iReadOnlyData._input._keyIsPressed[Input::Data::kRight])
    {
        _moveEW = Constants::Field::DirectionEW::kEast;
    }
}

void Human::handleMovement(const State::Global& iReadOnlyData, GenericData& ioGenericData)
{
    Controlled::handleMovement(iReadOnlyData, ioGenericData);
}

void Human::controlKick(const State::Global& iReadOnlyData, const ControlledData& ioControlledData)
{
    //XYZ FOR NOW, JUST HAVE 1 SET OF VARS PER KEY, CONSIDER IMPROVING LATER (NOTE USE OF REFERENCES)

    const bool& aKickActionB = iReadOnlyData._input._keyIsPressed[Input::Data::kButtonB];
    const bool& aKickActionC = iReadOnlyData._input._keyIsPressed[Input::Data::kButtonC];
    const bool& aKickActionChangedB = iReadOnlyData._input._keyPressedChanged[Input::Data::kButtonB];
    const bool& aKickActionChangedC = iReadOnlyData._input._keyPressedChanged[Input::Data::kButtonC];

    if (iReadOnlyData._rules._currentlyPossessed && iReadOnlyData._rules._possessionTeam == _team)
    {
        if (_kickType == Constants::Players::kNoKick)
        {
            if (aKickActionChangedB && aKickActionB)
            {
                _kickType = Constants::Players::kQuickKick;
            }
            else if (aKickActionChangedC && aKickActionC)
            {
                _kickType = Constants::Players::kDropKick;
            }
        }

        switch (_kickType)
        {
            case Constants::Players::kQuickKick:
                _kickAction = aKickActionB;
                _kickActionChanged = aKickActionChangedB;
                break;
            case Constants::Players::kPlaceKick:
            case Constants::Players::kDropKick:
            case Constants::Players::kLongKick:
                _kickAction = aKickActionC;
                _kickActionChanged = aKickActionChangedC;
                break;
            default:
                _kickAction = false;
                _kickActionChanged = false;
                break;
        }

        // For controlled kicks, once started the player velocity is frozen and the direction
        //  keys control the target direction of the kick
        if (_kickType == Constants::Players::kDropKick ||
            _kickType == Constants::Players::kLongKick ||
            _kickType == Constants::Players::kPlaceKick)
        {
            if (iReadOnlyData._input._keyIsPressed[Input::Data::kLeft] &&
                !iReadOnlyData._input._keyIsPressed[Input::Data::kRight])
            {
                _controlledKickTargetDirectionEW = Constants::Field::DirectionEW::kWest;
            }
            else if (iReadOnlyData._input._keyIsPressed[Input::Data::kRight] &&
                     !iReadOnlyData._input._keyIsPressed[Input::Data::kLeft])
            {
                _controlledKickTargetDirectionEW = Constants::Field::DirectionEW::kEast;
            }
            else
            {
                _controlledKickTargetDirectionEW = Constants::Field::DirectionEW::kNone;
            }

            if (iReadOnlyData._input._keyIsPressed[Input::Data::kUp] &&
                !iReadOnlyData._input._keyIsPressed[Input::Data::kDown])
            {
                _controlledKickTargetDirectionNS = Constants::Field::DirectionNS::kNorth;
            }
            else if (iReadOnlyData._input._keyIsPressed[Input::Data::kDown] &&
                     !iReadOnlyData._input._keyIsPressed[Input::Data::kUp])
            {
                _controlledKickTargetDirectionNS = Constants::Field::DirectionNS::kSouth;
            }
            else
            {
                _controlledKickTargetDirectionNS = Constants::Field::DirectionNS::kNone;
            }
        }
    }
    else
    {
        _kickType = Constants::Players::kNoKick;
        _kickAction = false;
        _kickActionChanged = false;
    }
}

void Human::handleKick(const State::Global& iReadOnlyData, ControlledData& ioControlledData)
{
    Controlled::handleKick(iReadOnlyData, ioControlledData);
}

void Human::controlSwap(const State::Global& iReadOnlyData, const Data& ioData)
{
    const bool& aSwapAction = iReadOnlyData._input._keyIsPressed[Input::Data::kButtonA];
    const bool& aSwapActionChanged = iReadOnlyData._input._keyPressedChanged[Input::Data::kButtonA];

    // The player swap only applies at release of the button used for the swap control, but then
    //  only if the ball is not possessed by the Human's team.
    _playerSwap = aSwapActionChanged && !aSwapAction &&
                  !(iReadOnlyData._rules._currentlyPossessed && iReadOnlyData._rules._possessionTeam == _team);
}

bool Human::handleSwap(const State::Global& iReadOnlyData, Data& ioData)
{
    return Controlled::handleSwap(iReadOnlyData, ioData);
}

}   // namespace Players
