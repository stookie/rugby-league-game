/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_PLAYERS_BOT
#define RL_PLAYERS_BOT


#include "players/Controlled.hpp"


namespace State
{

struct Main;
struct Global;

}

namespace Players
{

struct Data;
struct GenericData;
struct ControlledData;

}


namespace Players
{

// The Bot player class is under direct control from the A.I..

class Bot : public Controlled
{
public:
    Bot(Constants::Teams::TeamsType iTeam);

    virtual ~Bot() = default;

    Bot(const Bot&) = delete;
    Bot& operator=(const Bot&) = delete;
    Bot(Bot&&) = delete;
    Bot& operator=(Bot&&) = delete;

    virtual void initialise(const State::Main& iReadOnlyMainData, Data& ioData) override;
    virtual void iterate(const State::Global& iReadOnlyData, Data& ioData) override;
    virtual void finalise(const State::Global& iReadOnlyData, Data& ioData) override;

protected:
    // Control of the movement of the player
    virtual void controlMovement(const State::Global& iReadOnlyData, const ControlledData& ioControlledData) override;

    // Handle the movement of the player
    virtual void handleMovement(const State::Global& iReadOnlyData, GenericData& ioGenericData) override;

    // Control of the players kicks
    virtual void controlKick(const State::Global& iReadOnlyData, const ControlledData& ioControlledData) override;

    // Handle the players kicks
    virtual void handleKick(const State::Global& iReadOnlyData, ControlledData& ioControlledData) override;

    // Control of the swapping between players
    virtual void controlSwap(const State::Global& iReadOnlyData, const Data& ioData) override;

    // Handle the player swap
    virtual bool handleSwap(const State::Global& iReadOnlyData, Data& ioData) override;

private:
    // The enum defining the state of a running kick
    enum RunningKickState : unsigned char
    {
        kNotStarted,   // The kick hasnt started yet
        kWaiting,   // The player is waiting and not yet kicking
        kSetPower,   // The power is being set for the kick
        kSetSnap,   // The snap is being set for the kick
        kKicked,   // The kick is complete
    };

    // Control of a quick kick
    void controlQuickKick(const State::Global& iReadOnlyData, const ControlledData& ioControlledData);

    // Control of field goal kick
    void controlFieldGoalKick(const State::Global& iReadOnlyData, const ControlledData& ioControlledData);

    double _angle_deg;
    double _oldAngle_deg;

    double _targetX_m;
    double _targetY_m;
    bool _targetFixedSet;

    bool _prevKickAction;
    bool _kickCountDownStarted;
    double _kickCountDown_ms;
    RunningKickState _runningKickState;
};

}

#endif   // RL_PLAYERS_BOT
