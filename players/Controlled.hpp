/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_PLAYERS_CONTROLLED
#define RL_PLAYERS_CONTROLLED


#include "players/Generic.hpp"

#include "GlobalConstants.hpp"


namespace State
{

struct Main;
struct Global;

}

namespace Players
{

struct Data;
struct GenericData;
struct ControlledData;

}


namespace Players
{

// The Controlled player class is the partially abstract base of both the human
// and bot player classes. These are the players under control.
// Data for these classes is replicated to the equivalent Generic class.

class Controlled : public Generic
{
public:
    Controlled(Constants::Teams::TeamsType iTeam);

    virtual ~Controlled() = default;

    Controlled(const Controlled&) = delete;
    Controlled& operator=(const Controlled&) = delete;
    Controlled(Controlled&&) = delete;
    Controlled& operator=(Controlled&&) = delete;

    virtual void initialise(const State::Main& iReadOnlyMainData, Data& ioData) override;
    virtual void iterate(const State::Global& iReadOnlyData, Data& ioData) override;
    virtual void finalise(const State::Global& iReadOnlyData, Data& ioData) override;

protected:
    // Force the human player and the bot to use the same interface, so there is no advantage to
    //  either; do this by enforcing that they use attributes from this class, by making the
    //  read-write Data structure constant in the control() methods.

    // Control of the movement of the player
    virtual void controlMovement(const State::Global& iReadOnlyData, const ControlledData& ioControlledData) = 0;

    // Handle movement of the player
    virtual void handleMovement(const State::Global& iReadOnlyData, GenericData& ioGenericData) override;

    // Control of the players kicks
    virtual void controlKick(const State::Global& iReadOnlyData, const ControlledData& ioControlledData) = 0;

    // Handle the players kicks
    virtual void handleKick(const State::Global& iReadOnlyData, ControlledData& ioControlledData);

    // Control of the swapping between players
    virtual void controlSwap(const State::Global& iReadOnlyData, const Data& ioData) = 0;

    // Handle the player swap
    // Returns True when a swap has been performed
    virtual bool handleSwap(const State::Global& iReadOnlyData, Data& ioData);

    // The interface with the implementing (Human, Bot) classes:

    Constants::Players::KickType _kickType;   // the type of the upcoming kick
    bool _kickAction;   // whether the kick action is set; the meaning depends on the kick type
    bool _kickActionChanged;   // whether the kick action has changed

    Constants::Field::DirectionEW _controlledKickTargetDirectionEW;   // controlled kick direction target
    Constants::Field::DirectionNS _controlledKickTargetDirectionNS;   // controlled kick direction target

    bool _playerSwap;   // set to True in controlSwap() to change control to the nearest drone

private:
    // Reset the controlled player state for kick-off
    void resetMovementForKickoff(Constants::Field::EndType iRunningDirection, bool iInPossession, Data& ioData);

    // The enum defining the state of the controlled kick interface
    enum ControlledKickState : unsigned char
    {
        kNotStarted,   // The kick control swing hasnt started yet
        kIncreasingPower,   // Kick control increasing, setting the kick power
        kIncreasingSwingOnly,   // Kick control increasing, although kick power has already been set
        kDecreasingSwingOnly,   // Kick control decreasing, although kick power has already been set
        kDecreasingPower,   // Kick control decreasing, but kick power not yet set so also decreasing
        kDecreasingForSnap,   // Kick control decreasing and now setting the snap
        kEndSnapSet,   // Kick control has ended, with power and snap set
    };

    // Handle the case of a change to the requested kick type; by resetting internal state
    void handleKickTypeChange();

    // Handle the control of quick kicks
    void handleQuickKick(const State::Global& iReadOnlyData, ControlledData& ioControlledData);

    // Handle the controlled kicks (drop, place, or controlled kicks in play)
    void handleControlledKick(const State::Global& iReadOnlyData, ControlledData& ioControlledData);

    // Handle the controlled kicks, sub-divided into separate functions:
    // Determine the angle of the kick, must be called first
    void handleControlledKickAngle(const State::Global& iReadOnlyData, ControlledData& ioControlledData);
    // Determine the state of the controlled kick mechanism; returns true when the kick is complete
    bool handleControlledKickStateMachine(const State::Global& iReadOnlyData, ControlledData& ioControlledData);
    // Determine the velocity of the eventual kick
    void handleControlledKickVelocity(const State::Global& iReadOnlyData, ControlledData& ioControlledData);


    // General kick data
    Constants::Players::KickType _prevKickType;   // the previous value of _kickType

    // Kick data specific to quick kicks
    double _quickKickStartTime_ms;   // game time when kick action started

    // Kick data specific to controlled kicks
    ControlledKickState _controlledKickState;   // the current state of the controlled kick interface
    double _controlledKickIncreaseAmount_pIt;   // amount the kick control increases per iteration
    double _controlledKickDecreaseAmount_pIt;   // amount the kick control decreases per iteration
    double _controlledKickDirectionChangeRate_degpIt;   // the rate of change of the aimed direction
};

}

#endif   // RL_PLAYERS_CONTROLLED
