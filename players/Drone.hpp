/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_PLAYERS_DRONE
#define RL_PLAYERS_DRONE


#include "players/Generic.hpp"

#include "GlobalConstants.hpp"


namespace State
{

struct Main;
struct Global;

}

namespace Players
{

struct Data;
struct GenericData;
struct DroneData;
class System;

}


namespace Players
{

// The Drone player class is used for the NPC players; the players that run
// around appropriately but dont really play the game.
// They are there to be taken over by the human / bot players.

class Drone : public Generic
{
public:
    Drone(Constants::Teams::TeamsType iTeam, std::size_t iNumber);

    virtual ~Drone() = default;

    Drone(const Drone&) = default;   // Used for constructing in arrays
    Drone& operator=(const Drone&) = delete;
    Drone(Drone&&) = delete;
    Drone& operator=(Drone&&) = delete;

    virtual void initialise(const State::Main& iReadOnlyMainData, Data& ioData) override;
    virtual void iterate(const State::Global& iReadOnlyData, Data& ioData) override;
    virtual void finalise(const State::Global& iReadOnlyData, Data& ioData) override;

    friend class System;

protected:
    // Control of the movement of the drone player, only if non-controlled
    void controlMovement(const State::Global& iReadOnlyData, DroneData& ioDroneData);

    // Handle the movement of the player
    virtual void handleMovement(const State::Global& iReadOnlyData, GenericData& ioGenericData) override;

    const std::size_t _number;   // the number of the player on the team

private:
    // Reset the controlled player state for kick-off
    void resetMovementForKickoff(Constants::Field::EndType iRunningDirection, bool iInPossession, Data& ioData);

    double _oldAngle_deg;

    double _targetX_m;
    double _targetY_m;
};

}

#endif   // RL_PLAYERS_DRONE
