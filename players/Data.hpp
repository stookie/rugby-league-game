/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_PLAYERS_DATA
#define RL_PLAYERS_DATA

#include "ISystemData.hpp"

#include "GlobalConstants.hpp"


namespace Players
{

// Common to all players
struct GenericData
{
public:
    // Manual versioning to try to avoid missing fields when copying
    // UPDATE THIS WHEN CHANGING STRUCTURE !!
    static constexpr unsigned char kGenericDataVersion{1};

    double _posX_m;
    double _posY_m;

    double _velX_mps;
    double _velY_mps;

    Constants::Field::DirectionEW _directionEW;
    Constants::Field::DirectionNS _directionNS;
};

// A player not under control, a NPC
struct DroneData : GenericData
{
};

// A player under control, ie a Human or a Bot
struct ControlledData : GenericData
{
public:
    bool _releasingBall;   // flag set from release request to end of ball/player collision

    bool _kickBall;   // one-shot flag, set to request kick
    Constants::Players::KickType _kickType;   // one-shot indication of the kick type

    double _ballReleaseVelX_mps;   // x velocity of ball at release
    double _ballReleaseVelY_mps;   // y velocity of ball at release
    double _ballReleaseVelZ_mps;   // z velocity of ball at release

    bool _kickControlShow;   // whether to show the kick control for this controlled player
    double _kickControlAngle_deg;   // the desired angle of the controlled kick [-180, 180]
    double _kickControlPower;   // the derired power of the controlled kick [0, 1.0]
    double _kickControlSnap;   // the snap/accuracy of the controlled kick [0, 1.0]
    double _kickControlIndicator;   // the position of the power / snap control [-1.0, 1.0]
};

struct Data : ISystemData
{
public:
    Data() = default;
    ~Data() = default;

    Data(const Data&) = delete;
    Data& operator=(const Data&) = default;   // default
    Data(Data&&) = delete;
    Data& operator=(Data&&) = delete;

    // The Controlled data represents the controlling player (ie Human or Bot), and
    //  the Generic data is also copied to the relevant slot in the Drone data.
    ControlledData _controlled[Constants::Teams::kNumTeams];
    DroneData _players[Constants::Teams::kNumTeams][Constants::Rules::kPlayers];
    std::size_t _controlledPlayer[Constants::Teams::kNumTeams];
};

}

#endif   // RL_PLAYERS_DATA
