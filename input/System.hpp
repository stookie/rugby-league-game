/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_INPUT_SYSTEM
#define RL_INPUT_SYSTEM

#include "ISdlSystem.hpp"

#include "input/Data.hpp"

#include <SDL2/SDL_keycode.h>

#include <map>


struct ISystemData;


namespace Input
{

class System : public ISdlSystem
{
public:
    System(SdlCtx& ioSdlCtx);

    virtual ~System() = default;

    System(const System&) = delete;
    System& operator=(const System&) = delete;
    System(System&&) = delete;
    System& operator=(System&&) = delete;

    virtual void initialise(const State::Main& iReadOnlyMainData, ISystemData& oSystemData) override;
    virtual void iterate(const State::Global& iReadOnlyData, ISystemData& oSystemData) override;
    virtual void finalise(const State::Global& iReadOnlyData, ISystemData& oSystemData) override;

private:
    // Update the pressed state of a key in reponse to a key press
    void setKeyState(Data::KeyType iKeyType,
                     bool iIsPressed,
                     const State::Global& iReadOnlyData,
                     Data& oSystemData);

    // Update the time a key was pressed, iteration to iteration
    void setKeyTime(const State::Global& iReadOnlyData, Data& oSystemData);

    const std::map<SDL_Keycode, Data::KeyType> kMapSdlToInput;
};

}

#endif   // RL_INPUT_SYSTEM
