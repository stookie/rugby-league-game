/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "input/System.hpp"

#include "input/Data.hpp"

#include "GlobalData.hpp"
#include "GlobalConstants.hpp"
#include "SdlCtx.hpp"

#include <SDL2/SDL.h>

#include <iostream>


namespace Input
{

System::System(SdlCtx& ioSdlCtx) :
    ISdlSystem{ioSdlCtx},
    kMapSdlToInput{ {Constants::Input::kKeyUp,      Data::kUp},
                    {Constants::Input::kKeyDown,    Data::kDown},
                    {Constants::Input::kKeyLeft,    Data::kLeft},
                    {Constants::Input::kKeyRight,   Data::kRight},
                    {Constants::Input::kKeyButtonA, Data::kButtonA},
                    {Constants::Input::kKeyButtonB, Data::kButtonB},
                    {Constants::Input::kKeyButtonC, Data::kButtonC},
                    {Constants::Input::kKeyShift,   Data::kShift}, }
{
}

void System::initialise(const State::Main& iReadOnlyMainData, ISystemData& oSystemData)
{
}

void System::iterate(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
    Data& aData = static_cast<Data&>(oSystemData);

    SDL_Event aEvent;

    while (_sdlCtx.pollEvents(aEvent))
    {
        if (aEvent.type == SDL_QUIT ||
            (aEvent.type == SDL_KEYDOWN && aEvent.key.keysym.sym == SDLK_ESCAPE))
        {
            aData._quit = true;
        }
        else if (aEvent.type == SDL_KEYDOWN || aEvent.type == SDL_KEYUP)
        {
            const auto& aKeyPair = kMapSdlToInput.find(aEvent.key.keysym.sym);

            if (aKeyPair != kMapSdlToInput.end())
            {
                bool aIsPressed{aEvent.type == SDL_KEYDOWN};
                setKeyState(aKeyPair->second, aIsPressed, iReadOnlyData, aData);
            }
        }
    }

    setKeyTime(iReadOnlyData, aData);
}

void System::finalise(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
}

void System::setKeyState(Data::KeyType iKeyType,
                         bool iIsPressed,
                         const State::Global& iReadOnlyData,
                         Data& oSystemData)
{
    if (iKeyType < Data::kNumKeyTypes)
    {
        oSystemData._keyIsPressed[iKeyType] = iIsPressed;
        // std::cout << "Pressed: " << iKeyType << std::endl;
    }
}

void System::setKeyTime(const State::Global& iReadOnlyData, Data& oSystemData)
{
    Data::KeyType aKeyType{};

    while (aKeyType < Data::kNumKeyTypes)
    {
        const bool& aWasPressed{iReadOnlyData._input._keyIsPressed[aKeyType]};
        const bool& aPressed{oSystemData._keyIsPressed[aKeyType]};

        bool& aChanged{oSystemData._keyPressedChanged[aKeyType]};
        double& aTimeMs{oSystemData._keyPressedTime_ms[aKeyType]};

        aChanged = (aWasPressed != aPressed);

        // if (aChanged)
        //     std::cout << "Changed: " << aKeyType << std::endl;

        if (aPressed && !aChanged)
            aTimeMs += iReadOnlyData._main._iterationDelay_ms;
        else if (aPressed && aChanged)
            aTimeMs = iReadOnlyData._main._iterationDelay_ms;
        else if (!aPressed && !aChanged)
            aTimeMs = 0.0;

        // (!aPressed && aChanged) is noop, retain time key was pressed for

        aKeyType = static_cast<Data::KeyType>(aKeyType + 1);   // assumes enum is sequential from 0 to kNumKeyTypes
    }
}

}   // namespace Video
