/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_INPUT_DATA
#define RL_INPUT_DATA

#include "ISystemData.hpp"

#include "GlobalConstants.hpp"

#include <array>


namespace Input
{

struct Data : ISystemData
{
public:
    // The keys used to control the game action (must be sequential from 0 on)
    enum KeyType : unsigned char { kUp, kDown, kLeft, kRight, kButtonA, kButtonB, kButtonC, kShift, kNumKeyTypes };

    Data() = default;
    ~Data() = default;

    Data(const Data&) = delete;
    Data& operator=(const Data&) = default;   // default
    Data(Data&&) = delete;
    Data& operator=(Data&&) = delete;

    std::array<bool, kNumKeyTypes> _keyIsPressed;
    std::array<bool, kNumKeyTypes> _keyPressedChanged;
    std::array<double, kNumKeyTypes> _keyPressedTime_ms;

    bool _quit;
};

}

#endif   // RL_INPUT_DATA
