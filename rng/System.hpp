/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_RNG_SYSTEM
#define RL_RNG_SYSTEM

#include "ISystem.hpp"

#include <random>


struct ISystemData;


namespace RNG
{

class System : public ISystem
{
public:
    System();

    virtual ~System() = default;

    System(const System&) = delete;
    System& operator=(const System&) = delete;
    System(System&&) = delete;
    System& operator=(System&&) = delete;

    virtual void initialise(const State::Main& iReadOnlyMainData, ISystemData& oSystemData) override;
    virtual void iterate(const State::Global& iReadOnlyData, ISystemData& oSystemData) override;
    virtual void finalise(const State::Global& iReadOnlyData, ISystemData& oSystemData) override;

private:
    std::default_random_engine _engine;
    std::uniform_real_distribution<double> _distUniReal;
};

}

#endif   // RL_RNG_SYSTEM
