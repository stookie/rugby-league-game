/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "rng/System.hpp"

#include "rng/Data.hpp"

#include "GlobalConstants.hpp"
#include "GlobalData.hpp"
#include "Log.hpp"


namespace RNG
{

System::System() :
    _engine{decltype(_engine)::result_type{Constants::RNG::kSeed}},
    _distUniReal{0.0, 1.0}
{
}

void System::initialise(const State::Main& iReadOnlyMainData, ISystemData& oSystemData)
{
}

void System::iterate(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
    Data& aData = static_cast<Data&>(oSystemData);

    for (std::size_t aCount = 0; aCount < Constants::RNG::kNumUniReal; aCount++)
    {
        aData._uniReal[aCount] = _distUniReal(_engine);
        TRC(TrcType::kRng, "Random number ", aCount, " is ", aData._uniReal[aCount]);
    }
}

void System::finalise(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
}

}   // namespace RNG
