# Rugby League game

A free/libre open source game of Rugby League (under sloooow... development)

## Summary

This is an attempt to create a free, open source game of Rugby League. It probably won't be finished, will take years if it is, and be a bit crap then anyway.

It is inspired by the many top-down sports games of the 1980's and early 1990's; particularly E.T.'s Rugby League.

It is currently at a VERY early stage. It will be renamed to something better at a later point.

It is written in C++, and uses the SDL2 libraries. It is developed in Linux (Debian, specifically), but should be fully portable to all platforms supporting SDL2.

The source code is released under the GNU Affero General Public License, version 3 only. For the licenses of other included files, see "COPYING.md".

![GNU AGPL 3](https://www.gnu.org/graphics/agplv3-155x51.png "GNU AGPL 3")

## Build Instructions

Unix makefile build:

```
> mkdir build && cd build
> cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ../
> make
```

Eclipse project:

```
> cmake -G "Eclipse CDT4 - Unix Makefiles" -D CMAKE_BUILD_TYPE=Debug ./
```
