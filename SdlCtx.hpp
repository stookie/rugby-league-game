/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_MAIN_SDL_CTX
#define RL_MAIN_SDL_CTX

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include <cstddef>
#include <memory>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>


class SdlCtxException : public std::runtime_error
{
    using std::runtime_error::runtime_error;
};


// Opaque texture ID to clients, internally an offset into _loadedTextures
// Use a structure of size_t instead of an alias to ensure type safety
struct SdlTexture
{
    std::size_t _id;
};

// Opaque text cache ID to clients, internally an offset into _cachedText
// Use a structure of size_t instead of an alias to ensure type safety
struct SdlTextCache
{
    std::size_t _id;
};

using SdlDimensions = std::pair<int, int>;
using SdlTextureData = std::pair<SdlTexture, SdlDimensions>;

enum class SdlHorzTextJustify
{
    kLeft,
    kCentre,
    kRight,
};

enum class SdlVertTextJustify
{
    kTop,
    kCentre,
    kBaseline,
    kBottom,
};


class SdlCtx
{
public:

    SdlCtx();
    ~SdlCtx();

    SdlCtx(const SdlCtx&) = delete;
    SdlCtx& operator=(const SdlCtx&) = delete;
    SdlCtx(SdlCtx&&) = delete;
    SdlCtx& operator=(SdlCtx&&) = delete;

    // TIMING:

    // Delay/sleep in milliseconds
    void delayMs(Uint32 iMsecs) const;

    // Get the time since start (SDL init), in ms
    Uint32 getTimeSinceStartMs() const;

    // INPUTS:
    bool pollEvents(SDL_Event& aEvent);

    // GRAPHICS:

    // Set the window size
    void setWindowSize(int iWidth, int iHeight);

    // Get the window size
    SdlDimensions getWindowSize() const;

    // Set the window clip area
    void setWindowClipArea(int iX, int iY, int iW, int iH);

    // Restet the window clip area
    void resetWindowClipArea();

    // Get the origin coordinates of the current window clip area
    SdlDimensions getWindowClipAreaOrigin() const;

    // Start the rendering for this frame
    void renderStart() const;

    // End the rendering for this frame
    void renderEnd() const;

    // Load texture
    SdlTextureData loadTexture(const std::string& iFilename);

    // Set the texture alpha
    // This will return the success if the Throw On Error flag is not set,
    //  else it will throw an SdlCtxException exception
    bool setTextureAlpha(SdlTexture iTextureId, bool iThrowOnError, Uint8 iAlpha = SDL_ALPHA_OPAQUE);

    // Get the texture dimensions
    SdlDimensions getTextureDimensions(SdlTexture iTextureId) const;

    // Blit / render the texture
    void blit(SdlTexture iTextureId, int iX, int iY, int iW, int iH) const;
    void blit(SdlTexture iTextureId, int iX, int iY, SdlDimensions iDimensions) const;

    // Partially blit / render the texture, keeping the texture proportional
    // iDX and iDY are the offsets into the source texture
    void blitPartial(SdlTexture iTextureId, int iX, int iY, int iW, int iH, int iDX, int iDY) const;
    void blitPartial(SdlTexture iTextureId, int iX, int iY, SdlDimensions iDimensions, int iDX, int iDY) const;

    // Blit / render the rotated texture
    // iAngle_deg is the clockwise rotation angle in degrees
    // iCX and iCY define the point of rotation relative to the destination (middle of the image if not specified)
    void blitRotated(SdlTexture iTextureId, int iX, int iY, int iW, int iH, double iAngle_deg) const;
    void blitRotated(SdlTexture iTextureId, int iX, int iY, SdlDimensions iDimensions, double iAngle_deg) const;
    void blitRotated(SdlTexture iTextureId,
                     int iX,
                     int iY,
                     int iW,
                     int iH,
                     double iAngle_deg,
                     int iCX,
                     int iCY) const;
    void blitRotated(SdlTexture iTextureId,
                     int iX,
                     int iY,
                     SdlDimensions iDimensions,
                     double iAngle_deg,
                     int iCX,
                     int iCY) const;

    // Set drawing colour, fully opaque by default
    void setDrawColour(Uint8 iRed, Uint8 iGreen, Uint8 iBlue, Uint8 iAlpha = SDL_ALPHA_OPAQUE);
    void setDrawColour(uint32_t iHexColour);

    // Set the drawing blend mode
    void setBlendMode(SDL_BlendMode iMode);

    // Get the current drawing blend mode
    SDL_BlendMode getBlendMode();

    // Draw a (filled) rectangle
    void drawRectangle(int iX, int iY, int iW, int iH, bool iFill = true);

    // FONTS AND TEXT:

    // Load font
    void loadFont(const std::string& iFilename, int iPointSize);

    // Reserve a cache slot for text
    SdlTextCache reserveTextCache();

    // Print a line of text, using the previously set drawing colour
    void print(SdlTextCache iTextCache,
               const std::string& iText,
               int iX,   // LH/mid/RH point for Left/Centre/Right justify
               int iY,   // Top/mid/base/bottom point for Top/Centre/Baseline/Bottom justify
               SdlHorzTextJustify iHorzJustify,
               SdlVertTextJustify iVertJustify);

    // Print a line of text, allowing colour/alpha to be specified
    void print(SdlTextCache iTextCache,
               const std::string& iText,
               int iX,   // LH/mid/RH point for Left/Centre/Right justify
               int iY,   // Top/mid/base/bottom point for Top/Centre/Baseline/Bottom justify
               SdlHorzTextJustify iHorzJustify,
               SdlVertTextJustify iVertJustify,
               Uint8 iRed,
               Uint8 iGreen,
               Uint8 iBlue,
               Uint8 iAlpha = SDL_ALPHA_OPAQUE);

    // Pre-print (ie prepare) a line of text, without displaying it, using the previously set drawing colour
    void preprint(SdlTextCache iTextCache, const std::string& iText);

    // Pre-print (ie prepare) a line of text, without displaying it, allowing colour/alpha to be specified
    void preprint(SdlTextCache iTextCache,
                  const std::string& iText,
                  Uint8 iRed,
                  Uint8 iGreen,
                  Uint8 iBlue,
                  Uint8 iAlpha = SDL_ALPHA_OPAQUE);

    // Reprint a previously printed or a pre-printed line of text, without content or colour changes
    void reprint(SdlTextCache iTextCache,
                 int iX,   // LH/mid/RH point for Left/Centre/Right justify
                 int iY,   // Top/mid/base/bottom point for Top/Centre/Baseline/Bottom justify
                 SdlHorzTextJustify iHorzJustify,
                 SdlVertTextJustify iVertJustify);

    // Get the existing text dimensions
    SdlDimensions getTextDimensions(SdlTextCache iTextCache);

private:
    // Private type aliases
    using TextInputs = std::pair<std::string, SDL_Color>;
    using TextOutputs = std::pair<SDL_Texture*, SdlDimensions>;
    using TextData = std::pair<TextInputs, TextOutputs>;

    // Used by constructor clean-up and destructor
    void clean();

    // Map the Texture ID to a pointer to the actual texture
    SDL_Texture* getTexture(SdlTexture iTextureId) const;

    // Map the Text Cache ID to a reference to the actual text data
    TextData& getTextData(SdlTextCache iTextCache);

    // If the inputs have changed, print the required text and create the cached texture and dimenions
    // Also responsible for cleaning stale cached textures on changes
    bool printText(TextData& ioTextData, const std::string& iText, Uint8 iRed, Uint8 iGreen, Uint8 iBlue, Uint8 iAlpha);

    // Print the existing cached text texture
    void printToWindow(const TextData& ioTextData,
                       int iX,
                       int iY,
                       SdlHorzTextJustify iHorzJustify,
                       SdlVertTextJustify iVertJustify);

    bool _sdlInit;
    bool _sdlImgInit;
    bool _sdlTtfInit;
    SDL_Window* _window;
    SDL_Renderer* _render;
    std::string _resourcePath;
    std::vector<SDL_Texture*> _loadedTextures;
    TTF_Font* _font;
    std::vector<TextData> _cachedText;
};

#endif   // RL_MAIN_SDL_CTX
