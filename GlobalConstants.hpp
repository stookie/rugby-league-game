/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_MAIN_GLOBAL_CONSTANTS
#define RL_MAIN_GLOBAL_CONSTANTS

#include <SDL2/SDL_keyboard.h>

#include <cstddef>
#include <type_traits>


namespace Constants
{

namespace Maths
{

constexpr double kPi{3.1415926535897932385};
constexpr double kSqrt2{1.4142135623730950488};
constexpr double kInvSqrt2{1.0 / kSqrt2};
constexpr double kSqrt3{1.7320508075688772935};

constexpr double kSin30{0.5};
constexpr double kSin45{kSqrt2 / 2.0};
constexpr double kSin60{kSqrt3 / 2.0};
constexpr double kCos30{kSin60};
constexpr double kCos45{kSin45};
constexpr double kCos60{kSin30};

constexpr double kGravity_mps2{9.80665};

}

namespace Main
{

constexpr double kIterationRate_Hz{60.0};

}

namespace RNG
{

constexpr unsigned int kSeed{4269007};   // RNG engine seed
enum UniRealSize : unsigned char { kUR1, kUR2, kNumUniReal };

}

namespace Field
{

constexpr int kLength_m = 100;
constexpr int kWidth_m = 68;
constexpr int kIngoal_m = 7;

constexpr double kGoalPostSeparation_m = 5.5;
constexpr double kGoalBarHeight_m = 5.0;   // meant to be 3m, but exaggerate as player sprite is too big (~4m)

// The enums defining the direction of an object, such as a player or ball
enum class DirectionEW : unsigned char { kNone, kEast, kWest };
enum class DirectionNS : unsigned char { kNone, kSouth, kNorth };

// The enum defining the two ends of the field
enum EndType : unsigned char { kNorthEnd, kSouthEnd, kNumEnds };

// Coefficients for the y direction based on the field ends
constexpr double kTowardsEndCoeffY[kNumEnds] = { -1.0, 1.0 };
constexpr double kFromEndCoeffY[kNumEnds] = { 1.0, -1.0 };

}

namespace Camera
{

constexpr double kPanDeadZoneX = 0.3;   // the fraction of the display that is in
constexpr double kPanDeadZoneY = 0.3;   //  the middle and does not cause panning
constexpr double kPanMaxSpeed_mps = 15.0;
constexpr double kPanAccel_mps2 = 8.0;

}

namespace Teams
{

// The enum defining the teams and the number of teams
enum TeamsType : unsigned char { kTeamA, kTeamB, kNumTeams };
using TeamsTypeUT = typename std::underlying_type<TeamsType>::type;
constexpr TeamsType kTeam0{kTeamA};

// How each team is controlled
enum TeamsControlType : unsigned char { kHuman, kBot };
constexpr TeamsControlType kTeamController[kNumTeams] = { kHuman, kBot };
//constexpr TeamsControlType kTeamController[kNumTeams] = { kBot, kHuman };

// Team names
constexpr std::size_t kTeamNameMaxLength{10};
constexpr char kTeamNameA[] = "Rats&Filth";
constexpr char kTeamNameB[] = "Two Heads";

// RGBA colours of the two teams
constexpr uint32_t kTeamColourA{0x659CDFFF};   // R=101, G=156, B=223, A=255
constexpr uint32_t kTeamColourB{0x65072EFF};   // R=101, G=7, B=46, A=255

}

namespace Rules   // depends: Field, Teams
{

constexpr std::size_t kPlayers = 2;
constexpr std::size_t kSquad = kPlayers + 4;

constexpr Teams::TeamsType kStartTeamKickingOff = Teams::kTeamA;

constexpr Field::EndType kStartTeamRunningDirection[Teams::kNumTeams] = { Field::kNorthEnd, Field::kSouthEnd };
//constexpr Field::EndType kStartTeamRunningDirection[Teams::kNumTeams] = { Field::kSouthEnd, Field::kNorthEnd };

// The enum defining the state of the game
enum class GameState : unsigned char
{
    kWaiting,   // A pause from the previous state to the next state, when theres a Ruling
    kKickOff,   // A kick-off restart
    kInPlay,    // The game is in-play
};

// The enum defining a ruling, the cause of which triggers the Waiting Game State
enum class Ruling : unsigned char
{
    kNoRuling,
    kFieldGoal,
};

constexpr double kWaitingStateDelay_ms = 3000.0;

// The enum
enum class SelectionType : unsigned char
{
    kSelectNothing,   // No selection is available
    kSelectKick,      // Can select the type of controlled kick
    kSelectPenalty,   // Can select the type of penalty restart
};

// The enum defining the types of kicks that are selectable in the UI
enum SelectableKickType : unsigned char
{
    kControlledKickSelection,   // Long controlled kick (in play, penalty kick for touch)
    kDropKickSelection,         // Drop kick (field goal, goal line drop out)
    kPlaceKickSelection,        // Place kick (kick off, conversion, penalty goal)
    kNumSelectableKicks,
};

}

namespace Input
{

constexpr SDL_Keycode kKeyUp = SDLK_UP;
constexpr SDL_Keycode kKeyDown = SDLK_DOWN;
constexpr SDL_Keycode kKeyLeft = SDLK_LEFT;
constexpr SDL_Keycode kKeyRight = SDLK_RIGHT;
constexpr SDL_Keycode kKeyButtonA = SDLK_z;
constexpr SDL_Keycode kKeyButtonB = SDLK_x;
constexpr SDL_Keycode kKeyButtonC = SDLK_c;
constexpr SDL_Keycode kKeyShift = SDLK_SPACE;

}

namespace Video   // depends: Maths, Field
{

constexpr int kScreenWidth = 1150;
constexpr int kScreenHeight = 930;
constexpr int kPanelWidth = 250;
constexpr int kBarHeight = 30;
constexpr int kCameraWidth = kScreenWidth - kPanelWidth;
constexpr int kCameraHeight = kScreenHeight - kBarHeight;

constexpr double kFieldToVideo = 1500.0 / Field::kWidth_m;   // field is 1500 pixels wide
constexpr double kVideoToField = 1.0 / kFieldToVideo;

constexpr double kZFactor{Maths::kSin30};   // Z dimension contributes this much to Y dimension

// RGBA colours of some display elements
constexpr uint32_t kPanelBgColour{0x7F7F7FFF};   // R=127, G=127, B=127, A=255
constexpr uint32_t kPanelActiveColour{0xDEDEDEFF};   // R=222, G=222, B=222, A=255
constexpr uint32_t kPanelInactiveColour{0x979797FF};   // R=151, G=151, B=151, A=255
constexpr uint32_t kBarBgColour{0x3F3F3FFF};   // R=63, G=63, B=63, A=255
constexpr uint32_t kBarHighlightColour{0xFFFF00FF};   // R=255, G=255, B=0, A=255
constexpr uint32_t kBarTimeColour{0xDEDEDEFF};   // R=222, G=222, B=222, A=255

constexpr double kBarHighlightTime_ms = 3000;

constexpr char kGrass[] = "resources/ground_grass_gen_08_256.png";
constexpr char kLineHorz[] = "resources/line_h.png";
constexpr char kLineVert[] = "resources/line_v.png";
constexpr char kNumbers[] = "resources/numbers.png";

// Note that the constants related to contents of these files are within the using class itself
constexpr char kPlayersTeamA[] = "resources/player_team_a.png";
constexpr char kPlayersTeamB[] = "resources/player_team_b.png";
constexpr char kMarkers[] = "resources/markers.png";
constexpr char kBall[] = "resources/ball.png";
constexpr char kGoalPost[] = "resources/goal_post.png";
constexpr char kGoalBar[] = "resources/goal_bar.png";
constexpr char kGoalDot[] = "resources/goal_dot.png";
constexpr double kGoalDotFraction = 0.2;   // the fraction of the bar that is black dot, [0.0, 1.0]

constexpr char kKickControlLevel[] = "resources/kick_control_level.png";
constexpr char kKickPowerFrame[] = "resources/kick_power_frame.png";
constexpr char kKickPowerLevel[] = "resources/kick_power_level.png";
constexpr char kKickSnapFrame[] = "resources/kick_snap_frame.png";
constexpr char kKickSnapLevel[] = "resources/kick_snap_level.png";
constexpr char kKickDirection[] = "resources/kick_direction.png";
constexpr double kKickControlDisplayTime_ms = 1500.0;   // time to display kick control interface after the kick
constexpr int kKickControlBorder = 10;   // the border in pixels between the kick control and screen edge

constexpr char kFont[] = "resources/PressStart2P.ttf";
constexpr int kFontSize = 20;

}

namespace Ball   // depends: Video
{

constexpr double kBoundingBoxWidth_m = 16.0 * Video::kVideoToField;
constexpr double kBoundingBoxLength_m = kBoundingBoxWidth_m;
constexpr double kBoundingBoxHeight_m = kBoundingBoxWidth_m;

constexpr double kDecel_mps2 = 2.0;   // Deceleration when rolling
constexpr double kBounceCoefficient = 0.7;   // Decrease in velocity for each bounce
constexpr double kRollingVerticalVel_mps = 0.5;   // The vertical velocity that transitions bouncing ball to rolling

constexpr double kGoalReboundFactor = 0.8;   // Reduction in velocity when bouncing off goal posts/bars

}

namespace Players   // depends: Field, Video
{

// Generic:
constexpr double kAccel_mps2 = 7.0;
constexpr double kDecel_mps2 = 10.0;
constexpr double kMaxSpeed_mps = 9.0;

constexpr double kFieldLimit_m = 3.0;   // how much beyond field markings a player can go
constexpr double kFieldLimitWidth_m = kFieldLimit_m + Field::kWidth_m / 2.0;
constexpr double kFieldLimitLength_m = kFieldLimit_m + Field::kLength_m / 2.0 + Field::kIngoal_m;

constexpr double kBoundingBoxWidth_m = 28.0 * Video::kVideoToField;
constexpr double kBoundingBoxLength_m = 16.0 * Video::kVideoToField;
constexpr double kBoundingBoxHeight_m = 3.0;   // Oversized players...

// Controlled:

// The enum defining the types of kicks
enum KickType : unsigned char
{
    kNoKick,           // No kick
    kDropKick,         // Drop kick (field goal, goal line drop out)
    kPlaceKick,        // Place kick (kick off, conversion, penalty goal)
    kLongKick,         // Long controlled kick (in play, penalty kick for touch)
    kQuickKick,        // Quick kick (in play, penalty kick for touch?)
    kNumKicks
};

// Quick kick constants
constexpr double kQuickKickActionMaxTime_ms = 1000.0;   // the time the kick button is held for maximum strength
constexpr double kQuickKickWeakVel_mps = 10.0;   // velocity of a minimum strength kick
constexpr double kQuickKickStrongDeltaVel_mps = 2.0;   // delta velocity of a maximum strength kick (ie += kWeakKickVel_mps)
constexpr double kQuickKickVerticalFactor = 1.0;   // the factor to modify vertical velocity relative to horizontal velocity

// Long kick constants
constexpr double kLongKickWeakVel_mps = 9.0;   // velocity of a minimum strength kick
constexpr double kLongKickStrongDeltaVel_mps = 4.5;   // delta velocity of a maximum strength kick (ie += kWeakKickVel_mps)
constexpr double kLongKickVerticalFactor = 1.35;   // the factor to modify vertical relative to horizontal velocity

// Place kick constants
constexpr double kPlaceKickWeakVel_mps = 9.0;   // velocity of a minimum strength kick
constexpr double kPlaceKickStrongDeltaVel_mps = 4.5;   // delta velocity of a maximum strength kick (ie += kWeakKickVel_mps)
constexpr double kPlaceKickVerticalFactor = 1.35;   // the factor to modify vertical relative to horizontal velocity

// Drop kick constants
constexpr double kDropKickWeakVel_mps = 9.0;   // velocity of a minimum strength kick
constexpr double kDropKickStrongDeltaVel_mps = 4.5;   // delta velocity of a maximum strength kick (ie += kWeakKickVel_mps)
constexpr double kDropKickVerticalFactor = 1.35;   // the factor to modify vertical relative to horizontal velocity

// Common controlled kick constants
constexpr double kControlledKickShankVel_mps = 1.0;   // velocity if the kick is shanked (missed snap completely)
constexpr double kControlledKickShankVerticalFactor = 0.0;   // factor to modify vertical relative to horizontal velocity

// Controlled kick graphical interface properties
constexpr double kControlledKickIncreaseTime_s = 1.0;   // the time it takes the interface to go from no to full power
constexpr double kControlledKickDecreaseTime_s = 0.5;   // the time it takes the interface to go from full to no power
constexpr double kControlledKickSnapDeadzone = 0.1;   // if the snap level is this far from 0 (perfect), treat it as 0
constexpr double kControlledKickDirectionChangeRate_degps = 60.0;   // the rate of change of the aimed direction
constexpr double kControlledKickShankDirectionOffset_deg = 45.0;   // the direction offset due to a shanked kick

// Bot:
constexpr double kBotChaseTargetRange_m = 0.8;   // The distance the bot player will no longer chase the ball
constexpr double kBotChaseAngleDelta_deg = 20.0;   // The angle delta required before the chase angle is updated
constexpr double kBotChaseKickRange_m = 2.0;   // The distance the bot player will kick the ball when chasing the target
constexpr double kBotSwapRange_m = 4.0;   // The distance another player has to be closer by for the bot to swap
constexpr double kBotFieldGoalPowerLevel = 0.8;   // The controlled kick power level used for a field goal
constexpr double kBotFieldGoalSnapLevel = 0.49;   // The controlled kick snap level used for a field goal
constexpr double kBotRestartDelay_ms = 4000.0;   // The delay before the bot restarts, eg at the kickoff

}

}   // namespace Constants

#endif   // RL_MAIN_GLOBAL_CONSTANTS
