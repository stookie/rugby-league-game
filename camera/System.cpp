/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "camera/System.hpp"

#include "camera/Data.hpp"

#include "GlobalData.hpp"

#include <SDL2/SDL.h>

#include <iostream>


// Anonymous namespace
namespace
{

constexpr double kDeadZoneHalfWidth_m{Constants::Camera::kPanDeadZoneX * 0.5 *
                                      Constants::Video::kCameraWidth * Constants::Video::kVideoToField};

constexpr double kDeadZoneHalfLength_m{Constants::Camera::kPanDeadZoneY * 0.5 *
                                       Constants::Video::kCameraHeight * Constants::Video::kVideoToField};

constexpr double kPanZoneHalfWidth_m{(0.5 * Constants::Video::kCameraWidth * Constants::Video::kVideoToField) -
                                     kDeadZoneHalfWidth_m};

constexpr double kPanZoneHalfLength_m{(0.5 * Constants::Video::kCameraHeight * Constants::Video::kVideoToField) -
                                      kDeadZoneHalfLength_m};

}   // Anonymous namespace


namespace Camera
{

void System::initialise(const State::Main& iReadOnlyMainData, ISystemData& oSystemData)
{
    Data& aData = static_cast<Data&>(oSystemData);
    aData._centreX_m = 0.0;
    aData._centreY_m = 0.0;
    aData._centreVelX_mps = 0.0;
    aData._centreVelY_mps = 0.0;

    _maxDeltaVel_mpspIt = Constants::Camera::kPanAccel_mps2 * iReadOnlyMainData._iterationDelay_s;
}

void System::iterate(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
    Data& aData = static_cast<Data&>(oSystemData);

    // Chain the camera to the Ball
    double aDeltaX_m{iReadOnlyData._ball._posX_m - aData._centreX_m};
    double aDeltaY_m{iReadOnlyData._ball._posY_m - aData._centreY_m};

    if (std::abs(aDeltaX_m) >= kDeadZoneHalfWidth_m)
    {
        double aRatioX = (std::abs(aDeltaX_m) - kDeadZoneHalfWidth_m) / (kPanZoneHalfWidth_m - kDeadZoneHalfWidth_m);
        aRatioX = std::max(std::min(aRatioX, 1.0), 0.0) * (aDeltaX_m < 0.0 ? -1.0 : 1.0);

        double aTargetVelX_mps = aRatioX * Constants::Camera::kPanMaxSpeed_mps;

        double aDeltaVelX_mps = aTargetVelX_mps - aData._centreVelX_mps;
        aDeltaVelX_mps = std::max(std::min(aDeltaVelX_mps, _maxDeltaVel_mpspIt), -_maxDeltaVel_mpspIt);

        aData._centreVelX_mps += aDeltaVelX_mps;
    }
    else
    {
        double aDeltaVelX_mps = 0.0 - aData._centreVelX_mps;
        aDeltaVelX_mps = std::max(std::min(aDeltaVelX_mps, _maxDeltaVel_mpspIt), -_maxDeltaVel_mpspIt);
        aData._centreVelX_mps += aDeltaVelX_mps;
    }

    if (std::abs(aDeltaY_m) >= kDeadZoneHalfLength_m)
    {
        double aRatioY = (std::abs(aDeltaY_m) - kDeadZoneHalfLength_m) / (kPanZoneHalfLength_m - kDeadZoneHalfLength_m);
        aRatioY = std::max(std::min(aRatioY, 1.0), 0.0) * (aDeltaY_m < 0.0 ? -1.0 : 1.0);

        double aTargetVelY_mps = aRatioY * Constants::Camera::kPanMaxSpeed_mps;

        double aDeltaVelY_mps = aTargetVelY_mps - aData._centreVelY_mps;
        aDeltaVelY_mps = std::max(std::min(aDeltaVelY_mps, _maxDeltaVel_mpspIt), -_maxDeltaVel_mpspIt);

        aData._centreVelY_mps += aDeltaVelY_mps;
    }
    else
    {
        double aDeltaVelY_mps = 0.0 - aData._centreVelY_mps;
        aDeltaVelY_mps = std::max(std::min(aDeltaVelY_mps, _maxDeltaVel_mpspIt), -_maxDeltaVel_mpspIt);
        aData._centreVelY_mps += aDeltaVelY_mps;
    }

    aData._centreX_m += aData._centreVelX_mps * iReadOnlyData._main._iterationDelay_s;
    aData._centreY_m += aData._centreVelY_mps * iReadOnlyData._main._iterationDelay_s;
}

void System::finalise(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
}

}   // namespace Camera
