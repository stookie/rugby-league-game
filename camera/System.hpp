/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_CAMERA_SYSTEM
#define RL_CAMERA_SYSTEM

#include "ISystem.hpp"


struct ISystemData;


namespace Camera
{

class System : public ISystem
{
public:
    System() = default;
    virtual ~System() = default;

    System(const System&) = delete;
    System& operator=(const System&) = delete;
    System(System&&) = delete;
    System& operator=(System&&) = delete;

    virtual void initialise(const State::Main& iReadOnlyMainData, ISystemData& oSystemData) override;
    virtual void iterate(const State::Global& iReadOnlyData, ISystemData& oSystemData) override;
    virtual void finalise(const State::Global& iReadOnlyData, ISystemData& oSystemData) override;

private:
    double _maxDeltaVel_mpspIt;   // m per sec per It
};

}

#endif   // RL_CAMERA_SYSTEM
