/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_CAMERA_DATA
#define RL_CAMERA_DATA

#include "ISystemData.hpp"


namespace Camera
{

struct Data : ISystemData
{
public:
    Data() = default;
    ~Data() = default;

    Data(const Data&) = delete;
    Data& operator=(const Data&) = default;   // default
    Data(Data&&) = delete;
    Data& operator=(Data&&) = delete;

    double _centreX_m;
    double _centreY_m;

    double _centreVelX_mps;
    double _centreVelY_mps;
};

}

#endif   // RL_CAMERA_DATA
