/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_MAIN_ISYSTEM
#define RL_MAIN_ISYSTEM


struct ISystemData;

namespace State
{

struct Main;
struct Global;

}


class ISystem
{
public:
    ISystem() = default;
    virtual ~ISystem() = default;

    ISystem(const ISystem&) = delete;
    ISystem& operator=(const ISystem&) = delete;
    ISystem(ISystem&&) = delete;
    ISystem& operator=(ISystem&&) = delete;

    virtual void initialise(const State::Main& iReadOnlyMainData, ISystemData& oSystemData) = 0;
    virtual void iterate(const State::Global& iReadOnlyData, ISystemData& oSystemData) = 0;
    virtual void finalise(const State::Global& iReadOnlyData, ISystemData& oSystemData) = 0;
};

#endif   // RL_MAIN_ISYSTEM
