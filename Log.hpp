/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_MAIN_LOG
#define RL_MAIN_LOG

#include <iostream>


// The various types of logs, to allow them to be activated individually
enum class TrcType : unsigned char
{
    kDef,        // Default / non-specified
    kRng,        // Random number generator
    kCntKck,     // Controlled kicks
    kGmSt,       // Game/rule state
    kNumTypes,   // The number of log types, dont use as log type itself
};


namespace
{

// Global log activation
constexpr bool kLog{true};

// Per-type log activation
constexpr bool kTypeLog[static_cast<unsigned char>(TrcType::kNumTypes)]
{
    true,   // kDef
    false,   // kRng
    false,   // kCntKck
    false,   // kGmSt
};

}


template <typename H>
void TRC(TrcType iType, H iH)
{
    if (kLog && kTypeLog[static_cast<unsigned char>(iType)])
        std::cout << iH << std::endl;
}

template <typename H, typename... T>
void TRC(TrcType iType, H iH, T... iT)
{
    if (kLog && kTypeLog[static_cast<unsigned char>(iType)])
    {
        std::cout << iH;
        TRC<T...>(iType, iT...);
    }
}


#endif   // RL_MAIN_LOG
