/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_VIDEO_PLAYERS
#define RL_VIDEO_PLAYERS

#include "GlobalConstants.hpp"
#include "SdlCtx.hpp"

#include <array>
#include <tuple>


struct ISystemData;

namespace State
{

struct Global;

}

namespace Video
{

class Mapper;

}


namespace Video
{

class Players
{
public:
    Players(const Mapper& iMapper);

    ~Players() = default;

    Players(const Players&) = delete;
    Players& operator=(const Players&) = delete;
    Players(Players&&) = delete;
    Players& operator=(Players&&) = delete;

    // Set the Texture data
    void setPlayerATextureData(SdlTextureData&& ioTextureData);
    void setPlayerBTextureData(SdlTextureData&& ioTextureData);
    void setMarkerTextureData(SdlTextureData&& ioTextureData);

    // Update the list of player y-positions, and sort ready for later display
    void sortPlayerList(const State::Global& iReadOnlyData, ISystemData& oSystemData);

    // Draw the players, but only the ones between the y1 and y2 y-positions
    // Requires that sortPlayerList() is called prior, to update the players y-positions
    void drawPlayers(const State::Global& iReadOnlyData, ISystemData& oSystemData, double iY1_m, double iY2_m);

    // Draw the markers
    void drawMarkers(const State::Global& iReadOnlyData, ISystemData& oSystemData);

private:
    const Mapper& _mapper;
    SdlTextureData _playerTextureData[Constants::Teams::kNumTeams];
    SdlTextureData _markerTextureData;

    // The column controls which direction the sprite is running in
    unsigned char _spriteColumn[Constants::Teams::kNumTeams][Constants::Rules::kPlayers];
    // Which frame of the animation is currently used
    unsigned char _spriteAnimationFrame[Constants::Teams::kNumTeams][Constants::Rules::kPlayers];
    // The time at which the current frame was started
    double _spriteFrameStartTime_ms[Constants::Teams::kNumTeams][Constants::Rules::kPlayers];

    // Convenience constant for the sorted playe list
    static constexpr std::size_t kNumAllPlayers{Constants::Teams::kNumTeams * Constants::Rules::kPlayers};
    // An array of player details, sorted by their y-position, to be used to get draw order correct
    std::array<std::tuple<double, std::size_t, std::size_t>, kNumAllPlayers> _sortedPlayerList;
    // Optimise by storing the most recently considered player, in the sorted player list
    std::size_t _sortedPlayerLastIndex;
};

}

#endif   // RL_VIDEO_PLAYERS
