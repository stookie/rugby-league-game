/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_VIDEO_FIELD
#define RL_VIDEO_FIELD

#include "SdlCtx.hpp"


struct ISystemData;

namespace State
{

struct Global;

}

namespace Video
{

class Mapper;

}


namespace Video
{

class Field
{
public:
    Field(const Mapper& iMapper);

    ~Field() = default;

    Field(const Field&) = delete;
    Field& operator=(const Field&) = delete;
    Field(Field&&) = delete;
    Field& operator=(Field&&) = delete;

    // Set the Texture data
    void setGrassTextureData(SdlTextureData&& ioTextureData);
    void setLineHorzTextureData(SdlTextureData&& ioTextureData);
    void setLineVertTextureData(SdlTextureData&& ioTextureData);
    void setNumberTextureData(SdlTextureData&& ioTextureData);

    // Draw the grass
    void drawGrass(const State::Global& iReadOnlyData, ISystemData& oSystemData) const;

    // Draw the lines
    void drawHorizontalLines(const State::Global& iReadOnlyData, ISystemData& oSystemData) const;
    void drawVerticalLines(const State::Global& iReadOnlyData, ISystemData& oSystemData) const;

    // Draw the numbers
    void drawNumbers(const State::Global& iReadOnlyData, ISystemData& oSystemData) const;

private:
    const Mapper& _mapper;
    SdlTextureData _grassTextureData;
    SdlTextureData _lineHorzTextureData;
    SdlTextureData _lineVertTextureData;
    SdlTextureData _numbersTextureData;
};

}

#endif   // RL_VIDEO_FIELD
