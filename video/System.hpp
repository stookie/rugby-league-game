/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_VIDEO_SYSTEM
#define RL_VIDEO_SYSTEM

#include "ISdlSystem.hpp"

#include "video/Field.hpp"
#include "video/Goals.hpp"
#include "video/Players.hpp"
#include "video/Ball.hpp"
#include "video/KickControl.hpp"
#include "video/Panel.hpp"
#include "video/Bar.hpp"
#include "video/Mapper.hpp"

#include <functional>


struct ISystemData;


namespace Video
{

class System : public ISdlSystem
{
public:
    System(SdlCtx& ioSdlCtx);

    virtual ~System() = default;

    System(const System&) = delete;
    System& operator=(const System&) = delete;
    System(System&&) = delete;
    System& operator=(System&&) = delete;

    virtual void initialise(const State::Main& iReadOnlyMainData, ISystemData& oSystemData) override;
    virtual void iterate(const State::Global& iReadOnlyData, ISystemData& oSystemData) override;
    virtual void finalise(const State::Global& iReadOnlyData, ISystemData& oSystemData) override;

private:
    // A type alias used to wrap class draw methods so they can be treated conistently
    using IterativeDrawFunction = std::function<void(const State::Global&, ISystemData&)>;

    Mapper _mapper;
    Field _field;
    Goals _goals;
    Players _players;
    Ball _ball;
    KickControl _kickControls;
    Panel _panel;
    Bar _bar;

    // These wrap the attribute draw methods, so they can be drawn so as to support ordering
    const IterativeDrawFunction _northGoalsDrawer;
    const IterativeDrawFunction _southGoalsDrawer;
    const IterativeDrawFunction _ballDrawer;
};

}

#endif   // RL_VIDEO_SYSTEM
