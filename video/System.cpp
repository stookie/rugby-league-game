/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "video/System.hpp"

#include "video/Data.hpp"

#include "GlobalData.hpp"
#include "GlobalConstants.hpp"
#include "SdlCtx.hpp"

#include <SDL2/SDL.h>

#include <set>


namespace Video
{

System::System(SdlCtx& ioSdlCtx) :
    ISdlSystem{ioSdlCtx},
    _mapper{ioSdlCtx},
    _field{_mapper},
    _goals{_mapper},
    _players{_mapper},
    _ball{_mapper},
    _kickControls{_mapper},
    _panel{_mapper},
    _bar{_mapper},
    _northGoalsDrawer{std::bind(&Goals::drawGoals,
                                &_goals,
                                std::placeholders::_1,
                                std::placeholders::_2,
                                Constants::Field::kNorthEnd)},
    _southGoalsDrawer{std::bind(&Goals::drawGoals,
                                &_goals,
                                std::placeholders::_1,
                                std::placeholders::_2,
                                Constants::Field::kSouthEnd)},
    _ballDrawer{std::bind(&Ball::drawBall, &_ball, std::placeholders::_1, std::placeholders::_2)}
{
}

void System::initialise(const State::Main& iReadOnlyMainData, ISystemData& oSystemData)
{
    _sdlCtx.setWindowSize(Constants::Video::kScreenWidth, Constants::Video::kScreenHeight);
    _mapper.resizeCameraRegion(Constants::Video::kCameraWidth, Constants::Video::kCameraHeight);

    _field.setGrassTextureData(_sdlCtx.loadTexture(Constants::Video::kGrass));
    _field.setLineHorzTextureData(_sdlCtx.loadTexture(Constants::Video::kLineHorz));
    _field.setLineVertTextureData(_sdlCtx.loadTexture(Constants::Video::kLineVert));
    _field.setNumberTextureData(_sdlCtx.loadTexture(Constants::Video::kNumbers));

    _goals.setGoalPostTextureData(_sdlCtx.loadTexture(Constants::Video::kGoalPost));
    _goals.setGoalBarTextureData(_sdlCtx.loadTexture(Constants::Video::kGoalBar));
    _goals.setGoalDotTextureData(_sdlCtx.loadTexture(Constants::Video::kGoalDot));
    _goals.initialise(iReadOnlyMainData, oSystemData);

    _players.setPlayerATextureData(_sdlCtx.loadTexture(Constants::Video::kPlayersTeamA));
    _players.setPlayerBTextureData(_sdlCtx.loadTexture(Constants::Video::kPlayersTeamB));
    _players.setMarkerTextureData(_sdlCtx.loadTexture(Constants::Video::kMarkers));

    _ball.setBallTextureData(_sdlCtx.loadTexture(Constants::Video::kBall));

    _kickControls.setKickControlLevelTextureData(_sdlCtx.loadTexture(Constants::Video::kKickControlLevel));
    _kickControls.setKickPowerTexturesData(_sdlCtx.loadTexture(Constants::Video::kKickPowerFrame),
                                           _sdlCtx.loadTexture(Constants::Video::kKickPowerLevel));
    _kickControls.setKickSnapTexturesData(_sdlCtx.loadTexture(Constants::Video::kKickSnapFrame),
                                          _sdlCtx.loadTexture(Constants::Video::kKickSnapLevel));
    _kickControls.setKickDirectionTextureData(_sdlCtx.loadTexture(Constants::Video::kKickDirection));

    _sdlCtx.loadFont(Constants::Video::kFont, Constants::Video::kFontSize);

    std::array<SdlTextCache, Panel::kMaxNumChoices> aPanelChoiceTextCaches{};
    for (std::size_t aPanelChoiceNumber = 0; aPanelChoiceNumber < Panel::kMaxNumChoices; aPanelChoiceNumber++)
    {
        aPanelChoiceTextCaches[aPanelChoiceNumber] = _sdlCtx.reserveTextCache();
    }

    _panel.setPanelTextCaches(_sdlCtx.reserveTextCache(), aPanelChoiceTextCaches);

    _bar.setBarTextCaches(_sdlCtx.reserveTextCache(),
                          _sdlCtx.reserveTextCache(),
                          _sdlCtx.reserveTextCache(),
                          _sdlCtx.reserveTextCache(),
                          _sdlCtx.reserveTextCache());
}

void System::iterate(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
    _sdlCtx.renderStart();

    _mapper.setCameraRegionActive(true);
    _mapper.setCameraCentre(iReadOnlyData._camera._centreX_m, iReadOnlyData._camera._centreY_m);

    // These are lowest priority, so drawn first, everything else is drawn over the top
    _field.drawGrass(iReadOnlyData, oSystemData);
    _field.drawHorizontalLines(iReadOnlyData, oSystemData);
    _field.drawVerticalLines(iReadOnlyData, oSystemData);
    _field.drawNumbers(iReadOnlyData, oSystemData);
    _players.drawMarkers(iReadOnlyData, oSystemData);

    _players.sortPlayerList(iReadOnlyData, oSystemData);
    using Constants::Players::kFieldLimitLength_m;
    double aY1_m = -kFieldLimitLength_m - 1.0;

    std::multiset<std::pair<double, const IterativeDrawFunction*>> aDrawers =
        { {-Constants::Field::kLength_m / 2.0, &_northGoalsDrawer},
          {Constants::Field::kLength_m / 2.0, &_southGoalsDrawer},
          {iReadOnlyData._ball._posY_m, &_ballDrawer} };

    for (const auto& aDrawer : aDrawers)
    {
        _players.drawPlayers(iReadOnlyData, oSystemData, aY1_m, aDrawer.first);
        (*aDrawer.second)(iReadOnlyData, oSystemData);
        aY1_m = aDrawer.first;
    }

    _players.drawPlayers(iReadOnlyData, oSystemData, aY1_m, kFieldLimitLength_m + 1.0);

    _kickControls.drawKickControls(iReadOnlyData, oSystemData);

    SdlDimensions aWindowDims{_sdlCtx.getWindowSize()};

    _mapper.setPanelRegionActive(true);
    _sdlCtx.setDrawColour(Constants::Video::kPanelBgColour);
    _sdlCtx.drawRectangle(0, 0, aWindowDims.first, aWindowDims.second);
    _panel.drawPanel(iReadOnlyData, oSystemData);

    _mapper.setBarRegionActive(true);
    _sdlCtx.setDrawColour(Constants::Video::kBarBgColour);
    _sdlCtx.drawRectangle(0, 0, aWindowDims.first, aWindowDims.second);
    _bar.drawBar(iReadOnlyData, oSystemData);

    _sdlCtx.resetWindowClipArea();
    _sdlCtx.renderEnd();
}

void System::finalise(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
}

}   // namespace Video
