/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "video/KickControl.hpp"

#include "video/Mapper.hpp"

#include "GlobalData.hpp"
#include "GlobalConstants.hpp"

#include <cassert>


// Anonymous namespace
namespace
{

// Compile-time implementation of string compare, for static_assert
constexpr bool constStrCmp(const char* iS1, const char* iS2)
{
    return (*iS1 && *iS2) ? (*iS1 == *iS2 && constStrCmp(iS1 + 1, iS2 + 1)) : (!*iS1 && !*iS2);
}

}   // Anonymous namespace


namespace Video
{

KickControl::KickControl(const Mapper& iMapper) :
    _mapper{iMapper},
    _kickControlLevelTextureData{},
    _kickPowerFrameTextureData{},
    _kickPowerLevelTextureData{},
    _kickSnapFrameTextureData{},
    _kickSnapLevelTextureData{},
    _kickDirectionTextureData{},
    _remainOnScreenTimer_ms{},
    _prevAngle_deg{},
    _prevIndicator{},
    _prevPower{},
    _prevSnap{}
{
    static_assert(constStrCmp(Constants::Video::kKickControlLevel , "resources/kick_control_level.png"),
                  "Unexpected kick control level image");
    static_assert(constStrCmp(Constants::Video::kKickPowerFrame, "resources/kick_power_frame.png"),
                  "Unexpected kick control power frame image");
    static_assert(constStrCmp(Constants::Video::kKickPowerLevel, "resources/kick_power_level.png"),
                  "Unexpected kick control power level image");
    static_assert(constStrCmp(Constants::Video::kKickSnapFrame, "resources/kick_snap_frame.png"),
                  "Unexpected kick control snap frame image");
    static_assert(constStrCmp(Constants::Video::kKickSnapLevel, "resources/kick_snap_level.png"),
                  "Unexpected kick control snap level image");
    static_assert(constStrCmp(Constants::Video::kKickDirection, "resources/kick_direction.png"),
                  "Unexpected kick control direction arrow image");
}

void KickControl::setKickControlLevelTextureData(SdlTextureData&& ioKickControlLevelTextureData)
{
    _kickControlLevelTextureData = ioKickControlLevelTextureData;

    // The control level indicator logic is simplified below by using an image that is 1 pixel high
    assert(_kickControlLevelTextureData.second.second == 1);
}

void KickControl::setKickPowerTexturesData(SdlTextureData&& ioKickPowerFrameTextureData,
                                           SdlTextureData&& ioKickPowerLevelTextureData)
{
    _kickPowerFrameTextureData = ioKickPowerFrameTextureData;
    _kickPowerLevelTextureData = ioKickPowerLevelTextureData;

    assert(_kickPowerFrameTextureData.second.first == _kickPowerLevelTextureData.second.first &&
           _kickPowerFrameTextureData.second.second == _kickPowerLevelTextureData.second.second);
}

void KickControl::setKickSnapTexturesData(SdlTextureData&& ioKickSnapFrameTextureData,
                                          SdlTextureData&& ioKickSnapLevelTextureData)
{
    _kickSnapFrameTextureData = ioKickSnapFrameTextureData;
    _kickSnapLevelTextureData = ioKickSnapLevelTextureData;

    assert(_kickSnapFrameTextureData.second.first == _kickSnapLevelTextureData.second.first &&
           _kickSnapFrameTextureData.second.second == _kickSnapLevelTextureData.second.second);
}

void KickControl::setKickDirectionTextureData(SdlTextureData&& ioKickDirectionTextureData)
{
    _kickDirectionTextureData = ioKickDirectionTextureData;
}

void KickControl::drawKickControls(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
    for (std::size_t aTeam = Constants::Teams::kTeam0; aTeam < Constants::Teams::kNumTeams; aTeam++)
    {
        bool aDraw{false};

        if (iReadOnlyData._players._controlled[aTeam]._kickControlShow)
        {
            aDraw = true;
            _prevAngle_deg[aTeam] = iReadOnlyData._players._controlled[aTeam]._kickControlAngle_deg;
            _prevIndicator[aTeam] = iReadOnlyData._players._controlled[aTeam]._kickControlIndicator;
            _prevPower[aTeam] = iReadOnlyData._players._controlled[aTeam]._kickControlPower;
            _prevSnap[aTeam] = iReadOnlyData._players._controlled[aTeam]._kickControlSnap;
            _remainOnScreenTimer_ms[aTeam] = Constants::Video::kKickControlDisplayTime_ms;
        }
        else if (_remainOnScreenTimer_ms[aTeam] > 0.0)
        {
            aDraw = true;
            _remainOnScreenTimer_ms[aTeam] -= iReadOnlyData._main._iterationDelay_ms;
            _remainOnScreenTimer_ms[aTeam] = std::max(_remainOnScreenTimer_ms[aTeam], 0.0);
        }

        if (aDraw)
        {
            drawKickPowerAndSnap(iReadOnlyData, aTeam);
            drawKickDirection(iReadOnlyData, aTeam);
        }
    }
}

void KickControl::drawKickPowerAndSnap(const State::Global& iReadOnlyData, std::size_t iTeam)
{
    const int kPowerX{(Constants::Video::kCameraWidth - _kickPowerFrameTextureData.second.first) / 2};
    const int kSnapX{(Constants::Video::kCameraWidth - _kickSnapFrameTextureData.second.first) / 2};
    const int kControlX{(Constants::Video::kCameraWidth - _kickControlLevelTextureData.second.first) / 2};

    int kPowerY{};
    int kSnapY{};

    if (iReadOnlyData._rules._directionOfTeam[iTeam] == Constants::Field::kNorthEnd)
    {
        kSnapY = Constants::Video::kCameraHeight -
                 Constants::Video::kKickControlBorder -
                 _kickSnapFrameTextureData.second.second;
        kPowerY = kSnapY - _kickPowerFrameTextureData.second.second;
    }
    else
    {
        kPowerY = Constants::Video::kKickControlBorder;
        kSnapY = kPowerY + _kickPowerFrameTextureData.second.second;
    }

    _mapper.blitRelative(_kickPowerFrameTextureData.first, kPowerX, kPowerY, _kickPowerFrameTextureData.second);
    _mapper.blitRelative(_kickSnapFrameTextureData.first, kSnapX, kSnapY, _kickSnapFrameTextureData.second);

    // Power level starts from bottom of power frame and goes up, but screen coordinates go down.
    // Work backwards, and offset the power bar from the top of the power frame.
    const int kPowerLevelOffsetY = (1.0 - _prevPower[iTeam]) * _kickPowerLevelTextureData.second.second;

    _mapper.blitRelativePartial(_kickPowerLevelTextureData.first,
                                kPowerX,
                                kPowerY + kPowerLevelOffsetY,
                                _kickPowerLevelTextureData.second.first,
                                _kickPowerLevelTextureData.second.second - kPowerLevelOffsetY,
                                0,
                                kPowerLevelOffsetY);

    // Snap level starts from top of snap frame and goes down, same as screen coordinates.
    const int kSnapLevelOffsetY = _prevSnap[iTeam] * _kickSnapLevelTextureData.second.second;

    _mapper.blitRelativePartial(_kickSnapLevelTextureData.first,
                                kSnapX,
                                kSnapY,
                                _kickSnapLevelTextureData.second.first,
                                kSnapLevelOffsetY,
                                0,
                                0);

    if (_prevIndicator[iTeam] > 0.0)
    {
        // Similar logic for offset as power level.
        // Note control indicator image is uniform, and so stretched as required here in y-direction.
        const int kControlLevelOffsetY = (1.0 - _prevIndicator[iTeam]) * _kickPowerLevelTextureData.second.second;

        const int kControlLevelV = _prevIndicator[iTeam] * _kickPowerLevelTextureData.second.second;

        _mapper.blitRelative(_kickControlLevelTextureData.first,
                             kControlX,
                             kPowerY + kControlLevelOffsetY,
                             _kickControlLevelTextureData.second.first,
                             kControlLevelV);
    }
    else if (_prevIndicator[iTeam] < 0.0)
    {
        const int kControlLevelV = -1.0 * _prevIndicator[iTeam] * _kickSnapLevelTextureData.second.second;

        _mapper.blitRelative(_kickControlLevelTextureData.first,
                             kControlX,
                             kSnapY,
                             _kickControlLevelTextureData.second.first,
                             kControlLevelV);
    }
}

void KickControl::drawKickDirection(const State::Global& iReadOnlyData, std::size_t iTeam)
{
    SdlDimensions aDims{};
    aDims = _mapper.fieldToCameraView(iReadOnlyData._players._controlled[iTeam]._posX_m,
                                      iReadOnlyData._players._controlled[iTeam]._posY_m);

    const int kArrowX{aDims.first - _kickDirectionTextureData.second.first / 2};
    const int kArrowY{aDims.second - _kickDirectionTextureData.second.second};

    // Arrow image points up, which is -90 degrees in screen coordinates, so offset by 90 below
    _mapper.blitRelativeRotated(_kickDirectionTextureData.first,
                                kArrowX,
                                kArrowY,
                                _kickDirectionTextureData.second,
                                _prevAngle_deg[iTeam] + 90.0,
                                _kickDirectionTextureData.second.first / 2,
                                _kickDirectionTextureData.second.second);
}

}   // namespace Video
