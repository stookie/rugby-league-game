/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "video/Field.hpp"

#include "video/Mapper.hpp"

#include "GlobalData.hpp"

#include <cassert>


namespace Video
{

Field::Field(const Mapper& iMapper) :
    _mapper{iMapper}
{
}

void Field::setGrassTextureData(SdlTextureData&& ioTextureData)
{
    _grassTextureData = ioTextureData;
}

void Field::setLineHorzTextureData(SdlTextureData&& ioTextureData)
{
    _lineHorzTextureData = ioTextureData;
}

void Field::setLineVertTextureData(SdlTextureData&& ioTextureData)
{
    _lineVertTextureData = ioTextureData;
}

void Field::setNumberTextureData(SdlTextureData&& ioTextureData)
{
    _numbersTextureData = ioTextureData;

    // Numbers are already rotated and hence sideways/vertical; need 0..5
    assert(_numbersTextureData.second.second % 6 == 0);
}

void Field::drawGrass(const State::Global& iReadOnlyData, ISystemData& oSystemData) const
{
    const SdlDimensions& aDims{_grassTextureData.second};

    int aDx = static_cast<int>(std::lround(iReadOnlyData._camera._centreX_m * Constants::Video::kFieldToVideo * -1.0));
    aDx %= aDims.first;

    int aDy = static_cast<int>(std::lround(iReadOnlyData._camera._centreY_m * Constants::Video::kFieldToVideo * -1.0));
    aDy %= aDims.second;

    int aY = (aDy > 0) ? aDy - aDims.second : aDy;

    while (aY < Constants::Video::kCameraHeight)
    {
        int aX = (aDx > 0) ? aDx - aDims.first : aDx;

        while (aX < Constants::Video::kCameraWidth)
        {
            _mapper.blitRelative(_grassTextureData.first, aX, aY, aDims);
            aX += aDims.first;
        }

        aY += aDims.second;
    }
}

void Field::drawHorizontalLines(const State::Global& iReadOnlyData, ISystemData& oSystemData) const
{
    const double aLineWidthField = _lineHorzTextureData.second.first * Constants::Video::kVideoToField;
    const double aLineLengthField = _lineHorzTextureData.second.second * Constants::Video::kVideoToField;

    double aX{Constants::Field::kWidth_m / -2.0};
    double aY{};
    SdlDimensions aDims{};

    while (aX < Constants::Field::kWidth_m / 2.0)
    {
        //XYZ Improve this, it is awful:
        if (aX + aLineWidthField >= Constants::Field::kWidth_m / 2.0)
        {
            double aFieldDeltaX{Constants::Field::kWidth_m / 2.0 - aX};
            int aCameraDeltaX = static_cast<int>(std::lround(aFieldDeltaX * Constants::Video::kFieldToVideo));

            // Top in-goal
            aY = Constants::Field::kLength_m / -2.0 - Constants::Field::kIngoal_m - aLineLengthField;
            aDims = _mapper.fieldToCameraView(aX, aY);
            _mapper.blitRelativePartial(_lineHorzTextureData.first,
                                        aDims.first,
                                        aDims.second,
                                        aCameraDeltaX,
                                        _lineHorzTextureData.second.second,
                                        aCameraDeltaX,
                                        0.0);

            // Bottom in-goal
            aY = Constants::Field::kLength_m / 2.0 + Constants::Field::kIngoal_m;
            aDims = _mapper.fieldToCameraView(aX, aY);
            _mapper.blitRelativePartial(_lineHorzTextureData.first,
                                        aDims.first,
                                        aDims.second,
                                        aCameraDeltaX,
                                        _lineHorzTextureData.second.second,
                                        aCameraDeltaX,
                                        0.0);

            // 10 metre lines
            for (int aLine = -5; aLine <= 5; aLine++)
            {
                aY = Constants::Field::kLength_m / 10.0 * aLine;

                if (aLine < 0)
                {
                    aY -= aLineLengthField;
                }
                else if (aLine == 0)
                {
                    aY -= aLineLengthField / 2.0;
                }

                aDims = _mapper.fieldToCameraView(aX, aY);
                _mapper.blitRelativePartial(_lineHorzTextureData.first,
                                            aDims.first,
                                            aDims.second,
                                            aCameraDeltaX,
                                            _lineHorzTextureData.second.second,
                                            aCameraDeltaX,
                                            0.0);
            }
        }
        else
        {
            // Top in-goal
            aY = Constants::Field::kLength_m / -2.0 - Constants::Field::kIngoal_m - aLineLengthField;
            aDims = _mapper.fieldToCameraView(aX, aY);
            _mapper.blitRelative(_lineHorzTextureData.first, aDims.first, aDims.second, _lineHorzTextureData.second);

            // Bottom in-goal
            aY = Constants::Field::kLength_m / 2.0 + Constants::Field::kIngoal_m;
            aDims = _mapper.fieldToCameraView(aX, aY);
            _mapper.blitRelative(_lineHorzTextureData.first, aDims.first, aDims.second, _lineHorzTextureData.second);

            // 10 metre lines
            for (int aLine = -5; aLine <= 5; aLine++)
            {
                aY = Constants::Field::kLength_m / 10.0 * aLine;

                if (aLine < 0)
                {
                    aY -= aLineLengthField;
                }
                else if (aLine == 0)
                {
                    aY -= aLineLengthField / 2.0;
                }

                aDims = _mapper.fieldToCameraView(aX, aY);

                _mapper.blitRelative(_lineHorzTextureData.first,
                                     aDims.first,
                                     aDims.second,
                                     _lineHorzTextureData.second);
            }
        }

        aX += aLineWidthField;
    }
}

void Field::drawVerticalLines(const State::Global& iReadOnlyData, ISystemData& oSystemData) const
{
    const double aLineWidthField = _lineVertTextureData.second.first * Constants::Video::kVideoToField;
    const double aLineLengthField = _lineVertTextureData.second.second * Constants::Video::kVideoToField;
    const double aHorzLineLengthField = _lineHorzTextureData.second.second * Constants::Video::kVideoToField;

    const double aLhFieldX{Constants::Field::kWidth_m / -2.0 - aLineWidthField};
    constexpr double aRhFieldX{Constants::Field::kWidth_m / 2.0};

    double aY{Constants::Field::kLength_m / -2.0 - Constants::Field::kIngoal_m - aHorzLineLengthField};
    SdlDimensions aDims{};

    while (aY < Constants::Field::kLength_m / 2.0 + Constants::Field::kIngoal_m)
    {
        if (aY + aLineLengthField >=
            Constants::Field::kLength_m / 2.0 + Constants::Field::kIngoal_m + aHorzLineLengthField)
        {
            double aFieldDeltaY{Constants::Field::kLength_m / 2.0 +
                                Constants::Field::kIngoal_m +
                                aHorzLineLengthField - aY};

            int aCameraDeltaY = static_cast<int>(std::lround(aFieldDeltaY * Constants::Video::kFieldToVideo));

            aDims = _mapper.fieldToCameraView(aLhFieldX, aY);
            _mapper.blitRelativePartial(_lineVertTextureData.first,
                                        aDims.first,
                                        aDims.second,
                                        _lineVertTextureData.second.first,
                                        aCameraDeltaY,
                                        0.0,
                                        aCameraDeltaY);

            aDims = _mapper.fieldToCameraView(aRhFieldX, aY);
            _mapper.blitRelativePartial(_lineVertTextureData.first,
                                        aDims.first,
                                        aDims.second,
                                        _lineVertTextureData.second.first,
                                        aCameraDeltaY,
                                        0.0,
                                        aCameraDeltaY);
        }
        else
        {
            aDims = _mapper.fieldToCameraView(aLhFieldX, aY);
            _mapper.blitRelative(_lineVertTextureData.first, aDims.first, aDims.second, _lineVertTextureData.second);

            aDims = _mapper.fieldToCameraView(aRhFieldX, aY);
            _mapper.blitRelative(_lineVertTextureData.first, aDims.first, aDims.second, _lineVertTextureData.second);
        }

        aY += aLineLengthField;
    }
}

void Field::drawNumbers(const State::Global& iReadOnlyData, ISystemData& oSystemData) const
{
    const double aNumberWidthField = _numbersTextureData.second.first * Constants::Video::kVideoToField;
    const int aNumberTextureSpacing = _numbersTextureData.second.second / 6;
    SdlDimensions aDims{};

    constexpr double aX{Constants::Field::kWidth_m / 2.0 - 15.0};
    const double aDeltaFieldY{_lineHorzTextureData.second.second * Constants::Video::kVideoToField};

    for (int aLine = -4; aLine <= 4; aLine++)
    {
        // Zeroes

        double aY{Constants::Field::kLength_m / 10.0 * aLine};

        if (aLine < 0)
        {
            aY += aDeltaFieldY / 2.0;
        }
        else if (aLine == 0)
        {
            aY += aDeltaFieldY;
        }
        else if (aLine > 0)
        {
            aY += aDeltaFieldY / 2.0 * 3.0;
        }

        aDims = _mapper.fieldToCameraView(-aX - aNumberWidthField / 2.0, aY);
        _mapper.blitRelativePartial(_numbersTextureData.first,
                                    aDims.first,
                                    aDims.second,
                                    _numbersTextureData.second.first,
                                    aNumberTextureSpacing,
                                    0.0,
                                    0.0);

        aDims = _mapper.fieldToCameraView(aX - aNumberWidthField / 2.0, aY);
        _mapper.blitRelativePartial(_numbersTextureData.first,
                                    aDims.first,
                                    aDims.second,
                                    _numbersTextureData.second.first,
                                    aNumberTextureSpacing,
                                    0.0,
                                    0.0);

        // Non-Zeroes

        aY = Constants::Field::kLength_m / 10.0 * aLine - aNumberTextureSpacing * Constants::Video::kVideoToField;

        if (aLine < 0)
        {
            aY -= aDeltaFieldY / 2.0 * 3.0;
        }
        else if (aLine == 0)
        {
            aY -= aDeltaFieldY;
        }
        else if (aLine > 0)
        {
            aY -= aDeltaFieldY / 2.0;
        }

        // -4 = 2nd, -3 = 3rd , -2 = 4th, -1 = 5th, 0 = 6th, 1 = 5th, 2 = 4th, 3 = 3rd, 4 = 2nd
        const int aTexture{(5 - std::abs(aLine)) * aNumberTextureSpacing};

        aDims = _mapper.fieldToCameraView(-aX - aNumberWidthField / 2.0, aY);
        _mapper.blitRelativePartial(_numbersTextureData.first,
                                    aDims.first,
                                    aDims.second,
                                    _numbersTextureData.second.first,
                                    aNumberTextureSpacing,
                                    0.0,
                                    aTexture);

        aDims = _mapper.fieldToCameraView(aX - aNumberWidthField / 2.0, aY);
        _mapper.blitRelativePartial(_numbersTextureData.first,
                                    aDims.first,
                                    aDims.second,
                                    _numbersTextureData.second.first,
                                    aNumberTextureSpacing,
                                    0.0,
                                    aTexture);
    }
}

}   // namespace Video
