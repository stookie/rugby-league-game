/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "video/Ball.hpp"

#include "video/Mapper.hpp"

#include "GlobalData.hpp"
#include "GlobalConstants.hpp"

#include <cassert>


// Anonymous namespace
namespace
{

//XYZ 6 constants to move to global constants
// Sprite graphic properties
constexpr int kSpriteFrames{4};
constexpr int kSpriteShadowFrame{kSpriteFrames};   // ie 5th
constexpr int kSpriteShadowOffset{3};   // pixels offset from ball image
constexpr Uint8 kSpriteShadowAlpha{127};   // alpha mask for ball shadow

// Minimum speed to trigger sprite animation
const double kMinAnimationSpeed_mps{0.01};

// A tuning value to map the ball speed to the time per animation frame.
// See usage for more details.
const double kTimePerFrameTuningValue{150.0};

// Compile-time implementation of string compare, for static_assert
constexpr bool constStrCmp(const char* iS1, const char* iS2)
{
    return (*iS1 && *iS2) ? (*iS1 == *iS2 && constStrCmp(iS1 + 1, iS2 + 1)) : (!*iS1 && !*iS2);
}

}   // Anonymous namespace


namespace Video
{

Ball::Ball(const Mapper& iMapper) :
    _mapper{iMapper},
    _spriteAnimationFrame{0},
    _spriteFrameStartTime_ms{}
{
    // For now, everything related to turning the ball image into a sprite is contained in this
    // file only, as constants, so check the ball image is the one expected.
    static_assert(constStrCmp(Constants::Video::kBall, "resources/ball.png"), "Unexpected ball sprite");
}

void Ball::setBallTextureData(SdlTextureData&& ioTextureData)
{
    _ballTextureData = ioTextureData;

    // Sprite has 5 images (4 normal, 1 shadow), horizontally placed
    assert(_ballTextureData.second.first % (kSpriteFrames + 1) == 0);
}

void Ball::drawBall(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
    if (!iReadOnlyData._rules._currentlyPossessed)
    {
        drawBallAlways(iReadOnlyData, oSystemData);
    }
}

void Ball::drawBallAlways(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
    // Ball speed, used for animation
    double aSpeed_mps = std::sqrt(iReadOnlyData._ball._velX_mps * iReadOnlyData._ball._velX_mps +
                                  iReadOnlyData._ball._velY_mps * iReadOnlyData._ball._velY_mps +
                                  iReadOnlyData._ball._velZ_mps * iReadOnlyData._ball._velZ_mps);

    // Row determination: used to animate the player running
    double aTimePerFrame_msec{0.0};

    if (aSpeed_mps > kMinAnimationSpeed_mps)
        aTimePerFrame_msec = kTimePerFrameTuningValue / std::sqrt(aSpeed_mps);

    if (aTimePerFrame_msec != 0.0 &&
        iReadOnlyData._main._gameTimeSinceStart_ms - _spriteFrameStartTime_ms > aTimePerFrame_msec)
    {
        _spriteFrameStartTime_ms = iReadOnlyData._main._gameTimeSinceStart_ms;
        _spriteAnimationFrame++;

        if (_spriteAnimationFrame >= kSpriteFrames)
            _spriteAnimationFrame = 0;
    }

    const int aSpriteSquarePixels = _ballTextureData.second.second;

    _mapper.setTextureAlpha(_ballTextureData.first, kSpriteShadowAlpha);

    // Shadow
    SdlDimensions aDims{};
    aDims = _mapper.fieldToCameraView(iReadOnlyData._ball._posX_m, iReadOnlyData._ball._posY_m);
    _mapper.blitRelativePartial(_ballTextureData.first,
                                aDims.first - aSpriteSquarePixels / 2 + kSpriteShadowOffset,
                                aDims.second - aSpriteSquarePixels / 2 + kSpriteShadowOffset,
                                aSpriteSquarePixels,
                                aSpriteSquarePixels,
                                aSpriteSquarePixels * kSpriteShadowFrame,
                                0);

    _mapper.setTextureAlpha(_ballTextureData.first);

    // Ball
    aDims = _mapper.fieldToCameraView(iReadOnlyData._ball._posX_m,
                                      iReadOnlyData._ball._posY_m,
                                      iReadOnlyData._ball._posZ_m);

    _mapper.blitRelativePartial(_ballTextureData.first,
                                aDims.first - aSpriteSquarePixels / 2,
                                aDims.second - aSpriteSquarePixels / 2,
                                aSpriteSquarePixels,
                                aSpriteSquarePixels,
                                aSpriteSquarePixels * _spriteAnimationFrame,
                                0);
}

}   // namespace Video
