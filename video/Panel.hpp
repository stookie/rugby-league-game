/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_VIDEO_PANEL
#define RL_VIDEO_PANEL

#include "GlobalConstants.hpp"
#include "SdlCtx.hpp"

#include <array>


struct ISystemData;

namespace State
{

struct Global;

}

namespace Video
{

class Mapper;

}


namespace Video
{

class Panel
{
public:
    Panel(const Mapper& iMapper);

    ~Panel() = default;

    Panel(const Panel&) = delete;
    Panel& operator=(const Panel&) = delete;
    Panel(Panel&&) = delete;
    Panel& operator=(Panel&&) = delete;

    // The maximum number of choices, amongst all selections
    static constexpr std::size_t kMaxNumChoices{Constants::Rules::kNumSelectableKicks};//XYZ MAKE MAX OF ALL SELECTIONS

    // Set the Texture data
    void setPanelTextCaches(SdlTextCache&& ioTextCacheGameState,
                            const std::array<SdlTextCache, kMaxNumChoices>& iTextChoices);

    // Draw the panel
    void drawPanel(const State::Global& iReadOnlyData, ISystemData& oSystemData);

private:
    const Mapper& _mapper;

    SdlTextCache _textGameState;

    // Generic choice text, populated as required
    std::array<SdlTextCache, kMaxNumChoices> _textChoices;
};

}

#endif   // RL_VIDEO_PANEL
