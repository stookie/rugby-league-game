/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "video/Goals.hpp"

#include "video/Mapper.hpp"

#include "GlobalData.hpp"
#include "GlobalConstants.hpp"

#include <cassert>


// Anonymous namespace
namespace
{

// Visually overlap the goal bar slightly into the goal posts.
// This is a cosmetic effect only, therefore dont add to global constants.
constexpr double kGoalBarPostOverlap{0.25};   // 1/4 of the posts are overlapped by the bar

// The Y Coordinate for the goals
constexpr double kGoalCoordY_m[Constants::Field::kNumEnds] = {-Constants::Field::kLength_m / 2.0,
                                                              Constants::Field::kLength_m / 2.0};

// Compile-time implementation of string compare, for static_assert
constexpr bool constStrCmp(const char* iS1, const char* iS2)
{
    return (*iS1 && *iS2) ? (*iS1 == *iS2 && constStrCmp(iS1 + 1, iS2 + 1)) : (!*iS1 && !*iS2);
}

}   // Anonymous namespace


namespace Video
{

Goals::Goals(const Mapper& iMapper) :
    _mapper{iMapper}
{
    static_assert(constStrCmp(Constants::Video::kGoalPost, "resources/goal_post.png"), "Unexpected goal post image");
    static_assert(constStrCmp(Constants::Video::kGoalBar, "resources/goal_bar.png"), "Unexpected goal bar image");
    static_assert(constStrCmp(Constants::Video::kGoalDot, "resources/goal_dot.png"), "Unexpected goal dot image");
    static_assert(Constants::Video::kGoalDotFraction >= 0.0, "Goal dot is to be between 0 and 1");
    static_assert(Constants::Video::kGoalDotFraction <= 1.0, "Goal dot is to be between 0 and 1");
}

void Goals::setGoalPostTextureData(SdlTextureData&& ioTextureData)
{
    _goalPostTextureData = ioTextureData;
}

void Goals::setGoalBarTextureData(SdlTextureData&& ioTextureData)
{
    _goalBarTextureData = ioTextureData;

    // If both bar and dot are now set, assert they have the same height
    if (_goalDotTextureData.first._id != SdlTexture{}._id)
        assert(_goalBarTextureData.second.second == _goalDotTextureData.second.second);
}

void Goals::setGoalDotTextureData(SdlTextureData&& ioTextureData)
{
    _goalDotTextureData = ioTextureData;

    // If both bar and dot are now set, assert they have the same height
    if (_goalBarTextureData.first._id != SdlTexture{}._id)
        assert(_goalBarTextureData.second.second == _goalDotTextureData.second.second);
}

void Goals::initialise(const State::Main& iReadOnlyMainData, ISystemData& oSystemData)
{
    Data& aData = static_cast<Data&>(oSystemData);
    aData._goalPostRadius_m = Constants::Video::kVideoToField * _goalPostTextureData.second.first / 2.0;
    aData._goalPostHeight_m = Constants::Video::kVideoToField * _goalPostTextureData.second.second /
                              Constants::Video::kZFactor;
    aData._goalBarRadius_m = Constants::Video::kVideoToField * _goalBarTextureData.second.second / 2.0;
}

void Goals::drawGoals(const State::Global& iReadOnlyData, ISystemData& oSystemData, Constants::Field::EndType iFieldEnd)
{
    //XYZ NEED PADDING AND SHADOW GRAPHICS
    drawGoalPosts(iReadOnlyData, oSystemData, iFieldEnd);
    drawGoalBars(iReadOnlyData, oSystemData, iFieldEnd);
}

void Goals::drawGoalPosts(const State::Global& iReadOnlyData,
                          ISystemData& oSystemData,
                          Constants::Field::EndType iFieldEnd)
{
    // The half post-width accounts for the separation being inside edge to inside edge (post thickness).

    const double kGoalPostHalfSep_m = Constants::Field::kGoalPostSeparation_m / 2.0 +
                                      iReadOnlyData._video._goalPostRadius_m;

    // The Y-offset represents the post being circular, touching the try line, and extending outwards.
    // When viewed from the south, the post image is thus visibly south of the try line.
    const double kOffsetY_m = (iFieldEnd == Constants::Field::kNorthEnd) ? 0.0 : _goalPostTextureData.second.first;

    SdlDimensions aDims{};

    aDims = _mapper.fieldToCameraView(-kGoalPostHalfSep_m, kGoalCoordY_m[iFieldEnd]);

    // The half post-width use offsets the image so half the post is on either side of the X-location.

    _mapper.blitRelative(_goalPostTextureData.first,
                         aDims.first - _goalPostTextureData.second.first / 2,
                         aDims.second - _goalPostTextureData.second.second + kOffsetY_m,
                         _goalPostTextureData.second);

    aDims = _mapper.fieldToCameraView(kGoalPostHalfSep_m, kGoalCoordY_m[iFieldEnd]);

    _mapper.blitRelative(_goalPostTextureData.first,
                         aDims.first - _goalPostTextureData.second.first / 2,
                         aDims.second - _goalPostTextureData.second.second + kOffsetY_m,
                         _goalPostTextureData.second);
}

void Goals::drawGoalBars(const State::Global& iReadOnlyData,
                         ISystemData& oSystemData,
                         Constants::Field::EndType iFieldEnd)
{
    // When drawing, allow a little overlap between the bar and post, nice cosmetic effect
    const double kBarWidth_m = Constants::Field::kGoalPostSeparation_m +
                               kGoalBarPostOverlap * 2.0 * _goalPostTextureData.second.first *
                               Constants::Video::kVideoToField;

    // The Y-offset represents the post being circular, touching the try line, and extending outwards.
    // When viewed from the south, the post image is thus visibly south of the try line.
    const double kOffsetY_m = (iFieldEnd == Constants::Field::kNorthEnd) ? 0.0 : _goalPostTextureData.second.first;

    SdlDimensions aDims{};

    aDims = _mapper.fieldToCameraView(-kBarWidth_m / 2.0,
                                      kGoalCoordY_m[iFieldEnd],
                                      Constants::Field::kGoalBarHeight_m);

    _mapper.blitRelative(_goalBarTextureData.first,
                         aDims.first,
                         aDims.second - _goalBarTextureData.second.second + kOffsetY_m,
                         Constants::Video::kFieldToVideo * kBarWidth_m,
                         _goalBarTextureData.second.second);

    aDims = _mapper.fieldToCameraView(-kBarWidth_m / 2.0 * Constants::Video::kGoalDotFraction,
                                      kGoalCoordY_m[iFieldEnd],
                                      Constants::Field::kGoalBarHeight_m);

    _mapper.blitRelative(_goalDotTextureData.first,
                         aDims.first,
                         aDims.second - _goalDotTextureData.second.second + kOffsetY_m,
                         Constants::Video::kFieldToVideo * kBarWidth_m * Constants::Video::kGoalDotFraction,
                         _goalBarTextureData.second.second);
}

}   // namespace Video
