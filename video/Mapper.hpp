/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_VIDEO_MAPPER
#define RL_VIDEO_MAPPER

#include "SdlCtx.hpp"

#include <string>


namespace Video
{

class Mapper
{
public:
    Mapper(SdlCtx& ioSdlCtx);

    ~Mapper() = default;

    Mapper(const Mapper&) = delete;
    Mapper& operator=(const Mapper&) = delete;
    Mapper(Mapper&&) = delete;
    Mapper& operator=(Mapper&&) = delete;

    // Set the dimensions of the camera region, and by deduction, the other regions
    void resizeCameraRegion(int iWidth, int iHeight);

    // Retrieve the dimensions of the camera region
    SdlDimensions getCameraDimensions() const;

    // Set a region to be active
    void setCameraRegionActive(bool iForce);   // force == redo even if current
    void setPanelRegionActive(bool iForce);    // force == redo even if current
    void setBarRegionActive(bool iForce);      // force == redo even if current

    // Set the centre of the camera, in field units (metres)
    void setCameraCentre(double iCentreX, double iCentreY);

    // Set the texture alpha
    bool setTextureAlpha(SdlTexture iTextureId, Uint8 iAlpha = SDL_ALPHA_OPAQUE) const;

    // Blit using SDL context, but relative to the active region.
    // This method scales the blitted image if the output size is different to the texture size.
    // This method translates to the proper window coordinates.
    void blitRelative(SdlTexture iTextureId, int iX, int iY, int iW, int iH) const;
    void blitRelative(SdlTexture iTextureId, int iX, int iY, SdlDimensions iDimensions) const;

    // Partially blit using SDL context, but relative to the active region.
    // This method does not scale the blitted image.
    // This method translates to the proper window coordinates.
    void blitRelativePartial(SdlTexture iTextureId, int iX, int iY, int iW, int iH, int iDX, int iDY) const;
    void blitRelativePartial(SdlTexture iTextureId, int iX, int iY, SdlDimensions iDimensions, int iDX, int iDY) const;

    // Blit using SDL context, rotated around the image centre, but relative to the active region.
    // This method translates to the proper window coordinates.
    void blitRelativeRotated(SdlTexture iTextureId, int iX, int iY, int iW, int iH, double iAngle_deg) const;
    void blitRelativeRotated(SdlTexture iTextureId, int iX, int iY, SdlDimensions iDimensions, double iAngle_deg) const;

    // Blit using SDL context, rotated around the specified point, but relative to the active region.
    // The rotation point is relative to the blitted destination.
    // This method translates to the proper window coordinates.
    void blitRelativeRotated(SdlTexture iTextureId, int iX, int iY, int iW, int iH, double iAngle_deg, int iCX, int iCY) const;
    void blitRelativeRotated(SdlTexture iTextureId, int iX, int iY, SdlDimensions iDimensions, double iAngle_deg, int iCX,int iCY) const;

    // Set drawing colour, fully opaque by default.
    // Used by text printing, drawing marks.
    void setDrawColour(Uint8 iRed, Uint8 iGreen, Uint8 iBlue, Uint8 iAlpha = SDL_ALPHA_OPAQUE) const;
    void setDrawColour(uint32_t iHexColour) const;

    // Set the drawing blend mode
    void setBlendMode(SDL_BlendMode iMode) const;

    // Get the current drawing blend mode
    SDL_BlendMode getBlendMode() const;

    // Print using SDL context, relative to the active region, a line of text using the previously set drawing colour
    void printRelative(SdlTextCache iTextCache,
                       const std::string& iText,
                       int iX,   // LH/mid/RH point for Left/Centre/Right justify
                       int iY,   // Top/mid/base/bottom point for Top/Centre/Baseline/Bottom justify
                       SdlHorzTextJustify iHorzJustify,
                       SdlVertTextJustify iVertJustify) const;

    // Print using SDL context, relative to the active region, a line of text, allowing colour/alpha to be specified
    void printRelative(SdlTextCache iTextCache,
                       const std::string& iText,
                       int iX,   // LH/mid/RH point for Left/Centre/Right justify
                       int iY,   // Top/mid/base/bottom point for Top/Centre/Baseline/Bottom justify
                       SdlHorzTextJustify iHorzJustify,
                       SdlVertTextJustify iVertJustify,
                       Uint8 iRed,
                       Uint8 iGreen,
                       Uint8 iBlue,
                       Uint8 iAlpha = SDL_ALPHA_OPAQUE) const;

    // Pre-print using SDL context a line of text, without displaying it, using the previously set drawing colour
    void preprint(SdlTextCache iTextCache, const std::string& iText) const;

    // Pre-print using SDL context a line of text, without displaying it, allowing colour/alpha to be specified
    void preprint(SdlTextCache iTextCache,
                  const std::string& iText,
                  Uint8 iRed,
                  Uint8 iGreen,
                  Uint8 iBlue,
                  Uint8 iAlpha = SDL_ALPHA_OPAQUE) const;

    // Reprint using SDL context, relative to the active region, a previously printed or a pre-printed line of text
    void reprintRelative(SdlTextCache iTextCache,
                         int iX,   // LH/mid/RH point for Left/Centre/Right justify
                         int iY,   // Top/mid/base/bottom point for Top/Centre/Baseline/Bottom justify
                         SdlHorzTextJustify iHorzJustify,
                         SdlVertTextJustify iVertJustify) const;

    // Get the dimensions of pre-existing existing text output
    SdlDimensions getTextDimensions(SdlTextCache iTextCache) const;

    // Draws a simple mark (rectangle), in the current drawing colour, for debugging.
    // To use this correctly, the mapper must have had the camera centre set.
    void drawMarkCameraView1(double iFieldPosX, double iFieldPosY) const;   // dot
    void drawMarkCameraView2(double iFieldPosX, double iFieldPosY) const;   // square

    // Draws a rectangle, relative to the active region, in the current drawing colour.
    // This method translates to the proper window coordinates.
    void drawRectangleRelative(int iX, int iY, int iW, int iH, bool iFill = true) const;
    void drawRectangleRelative(int iX, int iY, SdlDimensions iDimensions, bool iFill = true) const;

    // Convert field deltas to relative camera region coordinates.
    // Deltas are the distance in field units from the centre of the camera postion.
    SdlDimensions fieldToCameraWindow(double iFieldDeltaX, double iFieldDeltaY) const;

    // Convert field positions to relative camera region coordinates.
    // Positions are absolute in field units, considering the camera postion.
    // To use this correctly, the mapper must have had the camera centre set.
    SdlDimensions fieldToCameraView(double iFieldPosX, double iFieldPosY) const;
    SdlDimensions fieldToCameraView(double iFieldPosX, double iFieldPosY, double iFieldPosZ) const;

private:
    // The window is split into 3 regions:
    enum WindowRegion : unsigned char { kCamera, kPanel, kBar };

    SdlCtx& _sdlCtx;
    SdlDimensions _windowTriPoint;   // the intersection of the 3 regions of the window
    WindowRegion _activeRegion;
    double _cameraCentreX_m;
    double _cameraCentreY_m;
};

}

#endif   // RL_VIDEO_MAPPER
