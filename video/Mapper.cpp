/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "video/Mapper.hpp"

#include "GlobalConstants.hpp"

#include <cmath>


namespace Video
{

Mapper::Mapper(SdlCtx& ioSdlCtx) :
    _sdlCtx{ioSdlCtx},
    _windowTriPoint{0, 0},
    _activeRegion{kCamera},
    _cameraCentreX_m{0.0},
    _cameraCentreY_m{0.0}
{
    resizeCameraRegion(1, 1);
}

void Mapper::resizeCameraRegion(int iWidth, int iHeight)
{
    SdlDimensions aWindowDims{_sdlCtx.getWindowSize()};

    if (iWidth < 1 || iHeight < 1 || (iWidth + 1) > aWindowDims.first || (iHeight + 1) > aWindowDims.second)
        throw SdlCtxException{"Invalid window region dimensions"};

    _windowTriPoint.first = iWidth;
    _windowTriPoint.second = iHeight;
}

SdlDimensions Mapper::getCameraDimensions() const
{
    return _windowTriPoint;
}

void Mapper::setCameraRegionActive(bool iForce)
{
    if (iForce || _activeRegion != kCamera)
    {
        _sdlCtx.setWindowClipArea(0, 0, _windowTriPoint.first, _windowTriPoint.second);
    }
}

void Mapper::setPanelRegionActive(bool iForce)
{
    SdlDimensions aDims = _sdlCtx.getWindowSize();

    if (iForce || _activeRegion != kPanel)
    {
        _sdlCtx.setWindowClipArea(_windowTriPoint.first, 0, aDims.first, aDims.second);
    }
}

void Mapper::setBarRegionActive(bool iForce)
{
    SdlDimensions aDims = _sdlCtx.getWindowSize();

    if (iForce || _activeRegion != kBar)
    {
        _sdlCtx.setWindowClipArea(0, _windowTriPoint.second, _windowTriPoint.first, aDims.second);
    }
}

void Mapper::setCameraCentre(double iCentreX, double iCentreY)
{
    _cameraCentreX_m = iCentreX;
    _cameraCentreY_m = iCentreY;
}

bool Mapper::setTextureAlpha(SdlTexture iTextureId, Uint8 iAlpha) const
{
    return _sdlCtx.setTextureAlpha(iTextureId, false, iAlpha);   // Dont throw exception, return success flag
}

void Mapper::blitRelative(SdlTexture iTextureId, int iX, int iY, int iW, int iH) const
{
    SdlDimensions aOffset = _sdlCtx.getWindowClipAreaOrigin();
    _sdlCtx.blit(iTextureId, iX + aOffset.first, iY + aOffset.second, iW, iH);
}

void Mapper::blitRelative(SdlTexture iTextureId, int iX, int iY, SdlDimensions iDimensions) const
{
    SdlDimensions aOffset = _sdlCtx.getWindowClipAreaOrigin();
    _sdlCtx.blit(iTextureId, iX + aOffset.first, iY + aOffset.second, iDimensions);
}

void Mapper::blitRelativePartial(SdlTexture iTextureId, int iX, int iY, int iW, int iH, int iDX, int iDY) const
{
    SdlDimensions aOffset = _sdlCtx.getWindowClipAreaOrigin();
    _sdlCtx.blitPartial(iTextureId, iX + aOffset.first, iY + aOffset.second, iW, iH, iDX, iDY);
}

void Mapper::blitRelativePartial(SdlTexture iTextureId,
                                 int iX,
                                 int iY,
                                 SdlDimensions iDimensions,
                                 int iDX,
                                 int iDY) const
{
    SdlDimensions aOffset = _sdlCtx.getWindowClipAreaOrigin();
    _sdlCtx.blitPartial(iTextureId, iX + aOffset.first, iY + aOffset.second, iDimensions, iDX, iDY);
}

void Mapper::blitRelativeRotated(SdlTexture iTextureId, int iX, int iY, int iW, int iH, double iAngle_deg) const
{
    SdlDimensions aOffset = _sdlCtx.getWindowClipAreaOrigin();
    _sdlCtx.blitRotated(iTextureId, iX + aOffset.first, iY + aOffset.second, iW, iH, iAngle_deg);
}

void Mapper::blitRelativeRotated(SdlTexture iTextureId,
                                 int iX,
                                 int iY,
                                 SdlDimensions iDimensions,
                                 double iAngle_deg) const
{
    SdlDimensions aOffset = _sdlCtx.getWindowClipAreaOrigin();
    _sdlCtx.blitRotated(iTextureId, iX + aOffset.first, iY + aOffset.second, iDimensions, iAngle_deg);
}

void Mapper::blitRelativeRotated(SdlTexture iTextureId,
                                 int iX,
                                 int iY,
                                 int iW,
                                 int iH,
                                 double iAngle_deg,
                                 int iCX,
                                 int iCY) const
{
    SdlDimensions aOffset = _sdlCtx.getWindowClipAreaOrigin();
    _sdlCtx.blitRotated(iTextureId, iX + aOffset.first, iY + aOffset.second, iW, iH, iAngle_deg, iCX, iCY);
}

void Mapper::blitRelativeRotated(SdlTexture iTextureId,
                                 int iX,
                                 int iY,
                                 SdlDimensions iDimensions,
                                 double iAngle_deg,
                                 int iCX,
                                 int iCY) const
{
    SdlDimensions aOffset = _sdlCtx.getWindowClipAreaOrigin();
    _sdlCtx.blitRotated(iTextureId, iX + aOffset.first, iY + aOffset.second, iDimensions, iAngle_deg, iCX, iCY);
}

void Mapper::setDrawColour(Uint8 iRed, Uint8 iGreen, Uint8 iBlue, Uint8 iAlpha) const
{
    _sdlCtx.setDrawColour(iRed, iGreen, iBlue, iAlpha);
}

void Mapper::setDrawColour(uint32_t iHexColour) const
{
    _sdlCtx.setDrawColour(iHexColour);
}

void Mapper::setBlendMode(SDL_BlendMode iMode) const
{
    _sdlCtx.setBlendMode(iMode);
}

SDL_BlendMode Mapper::getBlendMode() const
{
    return _sdlCtx.getBlendMode();
}

void Mapper::printRelative(SdlTextCache iTextCache,
                           const std::string& iText,
                           int iX,   // LH/mid/RH point for Left/Centre/Right justify
                           int iY,   // Top/mid/base/bottom point for Top/Centre/Baseline/Bottom justify
                           SdlHorzTextJustify iHorzJustify,
                           SdlVertTextJustify iVertJustify) const
{
    SdlDimensions aOffset = _sdlCtx.getWindowClipAreaOrigin();
    _sdlCtx.print(iTextCache,
                  iText,
                  iX + aOffset.first,   // LH/mid/RH point for Left/Centre/Right justify
                  iY + aOffset.second,   // Top/mid/base/bottom point for Top/Centre/Baseline/Bottom justify
                  iHorzJustify,
                  iVertJustify);
}

void Mapper::printRelative(SdlTextCache iTextCache,
                           const std::string& iText,
                           int iX,   // LH/mid/RH point for Left/Centre/Right justify
                           int iY,   // Top/mid/base/bottom point for Top/Centre/Baseline/Bottom justify
                           SdlHorzTextJustify iHorzJustify,
                           SdlVertTextJustify iVertJustify,
                           Uint8 iRed,
                           Uint8 iGreen,
                           Uint8 iBlue,
                           Uint8 iAlpha) const
{
    SdlDimensions aOffset = _sdlCtx.getWindowClipAreaOrigin();
    _sdlCtx.print(iTextCache,
                  iText,
                  iX + aOffset.first,   // LH/mid/RH point for Left/Centre/Right justify
                  iY + aOffset.second,   // Top/mid/base/bottom point for Top/Centre/Baseline/Bottom justify
                  iHorzJustify,
                  iVertJustify,
                  iRed,
                  iGreen,
                  iBlue,
                  iAlpha);
}

void Mapper::preprint(SdlTextCache iTextCache, const std::string& iText) const
{
    _sdlCtx.preprint(iTextCache, iText);
}

void Mapper::preprint(SdlTextCache iTextCache,
                      const std::string& iText,
                      Uint8 iRed,
                      Uint8 iGreen,
                      Uint8 iBlue,
                      Uint8 iAlpha) const
{
    _sdlCtx.preprint(iTextCache, iText, iRed, iGreen, iBlue, iAlpha);
}

void Mapper::reprintRelative(SdlTextCache iTextCache,
                             int iX,   // LH/mid/RH point for Left/Centre/Right justify
                             int iY,   // Top/mid/base/bottom point for Top/Centre/Baseline/Bottom justify
                             SdlHorzTextJustify iHorzJustify,
                             SdlVertTextJustify iVertJustify) const
{
    SdlDimensions aOffset = _sdlCtx.getWindowClipAreaOrigin();
    _sdlCtx.reprint(iTextCache,
                    iX + aOffset.first,   // LH/mid/RH point for Left/Centre/Right justify
                    iY + aOffset.second,   // Top/mid/base/bottom point for Top/Centre/Baseline/Bottom justify
                    iHorzJustify,
                    iVertJustify);
}

SdlDimensions Mapper::getTextDimensions(SdlTextCache iTextCache) const
{
    return _sdlCtx.getTextDimensions(iTextCache);
}

void Mapper::drawMarkCameraView1(double iFieldPosX, double iFieldPosY) const
{
    SdlDimensions aDims{fieldToCameraWindow(iFieldPosX - _cameraCentreX_m, iFieldPosY - _cameraCentreY_m)};
    _sdlCtx.drawRectangle(aDims.first - 1, aDims.second - 1, 2, 2);
}

void Mapper::drawMarkCameraView2(double iFieldPosX, double iFieldPosY) const
{
    SdlDimensions aDims{fieldToCameraWindow(iFieldPosX - _cameraCentreX_m, iFieldPosY - _cameraCentreY_m)};
    _sdlCtx.drawRectangle(aDims.first - 10, aDims.second - 10, 20, 20);
}

void Mapper::drawRectangleRelative(int iX, int iY, int iW, int iH, bool iFill) const
{
    SdlDimensions aOffset = _sdlCtx.getWindowClipAreaOrigin();
    _sdlCtx.drawRectangle(iX + aOffset.first,
                          iY + aOffset.second,
                          iW,
                          iH,
                          iFill);
}

void Mapper::drawRectangleRelative(int iX, int iY, SdlDimensions iDimensions, bool iFill) const
{
    SdlDimensions aOffset = _sdlCtx.getWindowClipAreaOrigin();
    _sdlCtx.drawRectangle(iX + aOffset.first,
                          iY + aOffset.second,
                          iDimensions.first,
                          iDimensions.second,
                          iFill);
}

SdlDimensions Mapper::fieldToCameraWindow(double iFieldDeltaX, double iFieldDeltaY) const
{
    SdlDimensions aCameraCoords{_windowTriPoint.first / 2, _windowTriPoint.second / 2};
    aCameraCoords.first += static_cast<int>(std::lround(iFieldDeltaX * Constants::Video::kFieldToVideo));
    aCameraCoords.second += static_cast<int>(std::lround(iFieldDeltaY * Constants::Video::kFieldToVideo));
    return aCameraCoords;
}

SdlDimensions Mapper::fieldToCameraView(double iFieldPosX, double iFieldPosY) const
{
    return fieldToCameraWindow(iFieldPosX - _cameraCentreX_m, iFieldPosY - _cameraCentreY_m);
}

SdlDimensions Mapper::fieldToCameraView(double iFieldPosX, double iFieldPosY, double iFieldPosZ) const
{
    const double aPseudoY_m = iFieldPosY - iFieldPosZ * Constants::Video::kZFactor;
    return fieldToCameraWindow(iFieldPosX - _cameraCentreX_m, aPseudoY_m - _cameraCentreY_m);
}

}
