/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_VIDEO_GOALS
#define RL_VIDEO_GOALS

#include "GlobalConstants.hpp"
#include "SdlCtx.hpp"


struct ISystemData;

namespace State
{

struct Main;
struct Global;

}

namespace Video
{

class Mapper;

}


namespace Video
{

class Goals
{
public:
    Goals(const Mapper& iMapper);

    ~Goals() = default;

    Goals(const Goals&) = delete;
    Goals& operator=(const Goals&) = delete;
    Goals(Goals&&) = delete;
    Goals& operator=(Goals&&) = delete;

    // Set the Texture data
    void setGoalPostTextureData(SdlTextureData&& ioTextureData);
    void setGoalBarTextureData(SdlTextureData&& ioTextureData);
    void setGoalDotTextureData(SdlTextureData&& ioTextureData);

    // Initialise goal data (only need to do this once)
    void initialise(const State::Main& iReadOnlyMainData, ISystemData& oSystemData);

    // Draw the goals
    void drawGoals(const State::Global& iReadOnlyData, ISystemData& oSystemData, Constants::Field::EndType iFieldEnd);

private:
    // Draw the goal posts
    void drawGoalPosts(const State::Global& iReadOnlyData,
                       ISystemData& oSystemData,
                       Constants::Field::EndType iFieldEnd);

    // Draw the goal cross-bars, including the black dot
    void drawGoalBars(const State::Global& iReadOnlyData,
                      ISystemData& oSystemData,
                      Constants::Field::EndType iFieldEnd);

    const Mapper& _mapper;
    SdlTextureData _goalPostTextureData;
    SdlTextureData _goalBarTextureData;
    SdlTextureData _goalDotTextureData;
};

}

#endif   // RL_VIDEO_GOALS
