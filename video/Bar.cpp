/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "video/Bar.hpp"

#include "video/Mapper.hpp"

#include "GlobalConstants.hpp"
#include "GlobalData.hpp"

#include <cmath>
#include <iomanip>
#include <sstream>


namespace Video
{

Bar::Bar(const Mapper& iMapper) :
    _mapper{iMapper},
    _firstDraw{true},
    _textScore{},
    _textName{},
    _textTime{},
    _teamScore{},
    _teamName{},
    _previousScore{},
    _highlightStartTime_ms{}
{
    // Add spaces to separate team score from team name
    _teamName[Constants::Teams::kTeamA] = std::string{" "} + std::string{Constants::Teams::kTeamNameA};
    _teamName[Constants::Teams::kTeamA] =
        _teamName[Constants::Teams::kTeamA].substr(0, Constants::Teams::kTeamNameMaxLength + 1);

    _teamName[Constants::Teams::kTeamB] = std::string{Constants::Teams::kTeamNameB} + std::string{" "};
    _teamName[Constants::Teams::kTeamB] =
        _teamName[Constants::Teams::kTeamB].substr(0, Constants::Teams::kTeamNameMaxLength + 1);
}

void Bar::setBarTextCaches(SdlTextCache&& ioTextCacheScoreLH,
                           SdlTextCache&& ioTextCacheNameLH,
                           SdlTextCache&& ioTextCacheScoreRH,
                           SdlTextCache&& ioTextCacheNameRH,
                           SdlTextCache&& ioTextCacheTime)
{
    _textScore[Constants::Teams::kTeamA] = ioTextCacheScoreLH;
    _textName[Constants::Teams::kTeamA] = ioTextCacheNameLH;
    _textScore[Constants::Teams::kTeamB] = ioTextCacheScoreRH;
    _textName[Constants::Teams::kTeamB] = ioTextCacheNameRH;
    _textTime = ioTextCacheTime;
}

void Bar::drawBar(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
    // Currently hard coded so Team A is LH side, Team B is RH side

    // Draw a background highlight box around the score when a teams score changes
    for (std::size_t aTeam = Constants::Teams::kTeam0; aTeam < Constants::Teams::kNumTeams; aTeam++)
    {
        using Constants::Video::kBarHighlightTime_ms;

        bool aHighlight{false};
        double aHighlightFactor{0.0};   // The factor of the highlight colour (ie fade in and out)

        if (_firstDraw)
        {
            _previousScore[aTeam] = iReadOnlyData._rules._score[aTeam];

            // Dummy value, to bypass later comparison to game time
            _highlightStartTime_ms[aTeam] = -kBarHighlightTime_ms - 1000;

            std::ostringstream aTeamScoreStr{};
            aTeamScoreStr << std::setfill('0') << std::setw(2) << iReadOnlyData._rules._score[aTeam];
            _teamScore[aTeam] = aTeamScoreStr.str();
        }
        else if (_previousScore[aTeam] != iReadOnlyData._rules._score[aTeam])
        {
            _previousScore[aTeam] = iReadOnlyData._rules._score[aTeam];
            _highlightStartTime_ms[aTeam] = iReadOnlyData._main._gameTimeSinceStart_ms;
            aHighlight = true;

            std::ostringstream aTeamScoreStr{};
            aTeamScoreStr << std::setfill('0') << std::setw(2) << iReadOnlyData._rules._score[aTeam];
            _teamScore[aTeam] = aTeamScoreStr.str();
        }
        else if (iReadOnlyData._main._gameTimeSinceStart_ms - _highlightStartTime_ms[aTeam] < kBarHighlightTime_ms)
        {
            aHighlight = true;

            // The highlight colour is linear and piecemeal, ramping up from 0.0 to 1.0, then back
            //  down to 0.0 again.
            aHighlightFactor = iReadOnlyData._main._gameTimeSinceStart_ms - _highlightStartTime_ms[aTeam];
            aHighlightFactor = (aHighlightFactor * 2.0 / kBarHighlightTime_ms) - 1.0;   // -1 .. 1
            aHighlightFactor = 1.0 - std::abs(aHighlightFactor);   // 0 .. 1 .. 0
        }

        if (aHighlight && !_firstDraw)
        {
            SDL_BlendMode aCurrentMode = _mapper.getBlendMode();
            _mapper.setBlendMode(SDL_BLENDMODE_BLEND);
            using Constants::Video::kBarHighlightColour;

            _mapper.setDrawColour(static_cast<Uint8>((kBarHighlightColour & 0xff000000) >> 24),
                                  static_cast<Uint8>((kBarHighlightColour & 0x00ff0000) >> 16),
                                  static_cast<Uint8>((kBarHighlightColour & 0x0000ff00) >> 8),
                                  static_cast<Uint8>((kBarHighlightColour & 0x000000ff)) * aHighlightFactor);

            SdlDimensions aDim = _mapper.getTextDimensions(_textScore[aTeam]);

            const int aX{(aTeam == Constants::Teams::kTeamA ? 0 : Constants::Video::kCameraWidth - aDim.first)};

            _mapper.drawRectangleRelative(aX, 0, aDim.first, Constants::Video::kBarHeight);
            _mapper.setBlendMode(aCurrentMode);
        }
    }

    // Team A score and name
    _mapper.setDrawColour(Constants::Teams::kTeamColourA);

    _mapper.printRelative(_textScore[Constants::Teams::kTeamA],
                          _teamScore[Constants::Teams::kTeamA],
                          0,
                          Constants::Video::kBarHeight / 2,
                          SdlHorzTextJustify::kLeft,
                          SdlVertTextJustify::kCentre);

    _mapper.printRelative(_textName[Constants::Teams::kTeamA],
                          _teamName[Constants::Teams::kTeamA],
                          0 + _mapper.getTextDimensions(_textScore[Constants::Teams::kTeamA]).first,
                          Constants::Video::kBarHeight / 2,
                          SdlHorzTextJustify::kLeft,
                          SdlVertTextJustify::kCentre);

    // Team B score and name
    _mapper.setDrawColour(Constants::Teams::kTeamColourB);

    _mapper.printRelative(_textScore[Constants::Teams::kTeamB],
                          _teamScore[Constants::Teams::kTeamB],
                          Constants::Video::kCameraWidth,
                          Constants::Video::kBarHeight / 2,
                          SdlHorzTextJustify::kRight,
                          SdlVertTextJustify::kCentre);

    _mapper.printRelative(_textName[Constants::Teams::kTeamB],
                          _teamName[Constants::Teams::kTeamB],
                          Constants::Video::kCameraWidth -
                              _mapper.getTextDimensions(_textScore[Constants::Teams::kTeamB]).first,
                          Constants::Video::kBarHeight / 2,
                          SdlHorzTextJustify::kRight,
                          SdlVertTextJustify::kCentre);

    // Time display
    int aGameTime_s = static_cast<int>(std::floor(iReadOnlyData._main._gameTimeSinceStart_ms / 1000.0));
    int aGameTimeLimited_min = (aGameTime_s / 60) % 100;   // wraps every 100 minutes
    int aGameTimeRemainder_s = aGameTime_s % 60;
    std::ostringstream aTimeStr{};
    aTimeStr << std::setfill('0') << std::setw(2) << aGameTimeLimited_min <<
                std::string{":"} << std::setw(2) << aGameTimeRemainder_s;

    _mapper.setDrawColour(Constants::Video::kBarTimeColour);

    _mapper.printRelative(_textTime,
                          aTimeStr.str(),
                          Constants::Video::kCameraWidth / 2,
                          Constants::Video::kBarHeight / 2,
                          SdlHorzTextJustify::kCentre,
                          SdlVertTextJustify::kCentre);

    _firstDraw = false;
}

}   // namespace Video
