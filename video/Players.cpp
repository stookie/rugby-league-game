/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "video/Players.hpp"

#include "video/Mapper.hpp"

#include "GlobalData.hpp"
#include "GlobalConstants.hpp"

#include <algorithm>
#include <cassert>


// Anonymous namespace
namespace
{

//XYZ Many constants below to move to global constants
// This maps the player direction to the player graphic sprite column number
const unsigned char kSpriteColumns[3][3]{ {0, 2, 6} ,    // None: None, South, North
                                          {0, 1, 7} ,    // East: None, South, North
                                          {4, 3, 5} };   // West: None, South, North

// This maps the transition from the existing player graphics sprite row to the next
const unsigned char kSpriteRowTransition[] {0, 1, 2, 1};
const std::size_t kSpriteRowTransitionSize{sizeof(kSpriteRowTransition) / sizeof(kSpriteRowTransition[0])};

// This row is used by default; ie when the player is not moving
const unsigned char kSpriteDefaultRow{1};

// Sprite graphic properties
const int kSpriteHeight{64};
const int kSpriteWidth{64};
const int kSpriteFeetXOffset{32};
const int kSpriteFeetYOffset{50};

// Marker offset from the players feet (subjectively tuned)
const int kMarkerYOffset{32};

// Minimum speed to trigger sprite animation
const double kMinAnimationSpeed_mps{0.01};

// A tuning value to map the player speed to the time per animation frame.
// See usage for more details.
const double kTimePerFrameTuningValue{100.0};

// Compile-time implementation of string compare, for static_assert
constexpr bool constStrCmp(const char* iS1, const char* iS2)
{
    return (*iS1 && *iS2) ? (*iS1 == *iS2 && constStrCmp(iS1 + 1, iS2 + 1)) : (!*iS1 && !*iS2);
}

}   // Anonymous namespace


namespace Video
{

Players::Players(const Mapper& iMapper) :
    _mapper{iMapper},
    _playerTextureData{},
    _markerTextureData{},
    _spriteColumn{},
    _spriteAnimationFrame{},
    _spriteFrameStartTime_ms{},
    _sortedPlayerList{},
    _sortedPlayerLastIndex{0}
{
    // For now, everything related to turning the player image into a sprite is contained in this
    // file only, as constants, so check the player image is the one expected.
    static_assert(constStrCmp(Constants::Video::kPlayersTeamA, "resources/player_team_a.png"),
                  "Unexpected player A sprite");
    static_assert(constStrCmp(Constants::Video::kPlayersTeamB, "resources/player_team_b.png"),
                  "Unexpected player B sprite");
    static_assert(constStrCmp(Constants::Video::kMarkers, "resources/markers.png"),
                  "Unexpected markers image");

    // Initially, players are not facing East or West
    constexpr std::size_t aIdxEW{static_cast<std::size_t>(Constants::Field::DirectionEW::kNone)};

    for (std::size_t aTeam = Constants::Teams::kTeam0; aTeam < Constants::Teams::kNumTeams; aTeam++)
    {
        // Initially, players face North or South depending on which team they are on
        const std::size_t aIdxNS{aTeam == Constants::Teams::kTeamA ?
                                 static_cast<std::size_t>(Constants::Field::DirectionNS::kNorth) :
                                 static_cast<std::size_t>(Constants::Field::DirectionNS::kSouth)};

        for (std::size_t aPlayer = 0; aPlayer < Constants::Rules::kPlayers; aPlayer++)
        {
            _spriteColumn[aTeam][aPlayer] = kSpriteColumns[aIdxEW][aIdxNS];
            _spriteAnimationFrame[aTeam][aPlayer] = kSpriteDefaultRow;
            _spriteFrameStartTime_ms[aTeam][aPlayer] = 0.0;

            _sortedPlayerList[_sortedPlayerLastIndex] = std::make_tuple(0.0, aTeam, aPlayer);
            _sortedPlayerLastIndex++;
        }
    }

    // Reset the last used index
    _sortedPlayerLastIndex = 0;
}

void Players::setPlayerATextureData(SdlTextureData&& ioTextureData)
{
    _playerTextureData[Constants::Teams::kTeamA] = ioTextureData;
}

void Players::setPlayerBTextureData(SdlTextureData&& ioTextureData)
{
    _playerTextureData[Constants::Teams::kTeamB] = ioTextureData;
}

void Players::setMarkerTextureData(SdlTextureData&& ioTextureData)
{
    _markerTextureData = ioTextureData;
}

void Players::sortPlayerList(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
    // First, reset the index for the last considered player
    _sortedPlayerLastIndex = 0;

    // Repopulate the array with the latest y-position of the players
    for (std::size_t aListIdx = 0; aListIdx < kNumAllPlayers; aListIdx++)
    {
        auto& aPlayerTuple{_sortedPlayerList[aListIdx]};
        const auto& aTeam{std::get<1>(aPlayerTuple)};
        const auto& aPlayer{std::get<2>(aPlayerTuple)};
        std::get<0>(aPlayerTuple) = iReadOnlyData._players._players[aTeam][aPlayer]._posY_m;
    }

    // Finally, sort the array to order the players by y position
    std::sort(_sortedPlayerList.begin(), _sortedPlayerList.end());
}

void Players::drawPlayers(const State::Global& iReadOnlyData, ISystemData& oSystemData, double iY1_m, double iY2_m)
{
    assert(_sortedPlayerLastIndex < kNumAllPlayers);

    while (_sortedPlayerLastIndex < kNumAllPlayers)
    {
        const auto& aPlayerTuple{_sortedPlayerList[_sortedPlayerLastIndex]};
        const auto& aPosY_m{std::get<0>(aPlayerTuple)};
        const auto& aTeam{std::get<1>(aPlayerTuple)};
        const auto& aPlayer{std::get<2>(aPlayerTuple)};

        if (aPosY_m < iY1_m)
        {
            // Player is before the range of y-positions; simply skip this player and continue
            _sortedPlayerLastIndex++;
            continue;
        }
        else if (aPosY_m >= iY2_m)
        {
            // Player is after the range of y-positions; as this list is sorted can break the loop here
            // Dont increament the index, as this player need to be displayed on the next call
            break;
        }

        const ::Players::DroneData& aPlayerData{iReadOnlyData._players._players[aTeam][aPlayer]};

        SdlDimensions aDims{};
        aDims = _mapper.fieldToCameraView(aPlayerData._posX_m, aPlayerData._posY_m);

        // Column determination: which direction the player runs in
        const Constants::Field::DirectionEW& aDirEW{aPlayerData._directionEW};
        const Constants::Field::DirectionNS& aDirNS{aPlayerData._directionNS};

        if (aDirEW != Constants::Field::DirectionEW::kNone || aDirNS != Constants::Field::DirectionNS::kNone)
        {
            auto aSpriteColumn{kSpriteColumns[static_cast<std::size_t>(aDirEW)][static_cast<std::size_t>(aDirNS)]};
            _spriteColumn[aTeam][aPlayer] = aSpriteColumn;
        }

        // Player speed, used for running animation
        double aSpeed_mps = std::sqrt(aPlayerData._velX_mps * aPlayerData._velX_mps +
                                        aPlayerData._velY_mps * aPlayerData._velY_mps);

        // Row determination: used to animate the player running
        double aTimePerFrame_msec{0.0};

        if (aSpeed_mps > kMinAnimationSpeed_mps)
            aTimePerFrame_msec = kTimePerFrameTuningValue / std::sqrt(aSpeed_mps);

        if (aTimePerFrame_msec != 0.0 &&
            iReadOnlyData._main._gameTimeSinceStart_ms - _spriteFrameStartTime_ms[aTeam][aPlayer] > aTimePerFrame_msec)
        {
            _spriteFrameStartTime_ms[aTeam][aPlayer] = iReadOnlyData._main._gameTimeSinceStart_ms;
            (_spriteAnimationFrame[aTeam][aPlayer])++;

            if (_spriteAnimationFrame[aTeam][aPlayer] > kSpriteRowTransitionSize)
                _spriteAnimationFrame[aTeam][aPlayer] = 0;
        }
        else if (aTimePerFrame_msec == 0.0)
        {
            _spriteAnimationFrame[aTeam][aPlayer] = kSpriteDefaultRow;
        }

        unsigned char aSpriteRow{kSpriteRowTransition[_spriteAnimationFrame[aTeam][aPlayer]]};

        _mapper.blitRelativePartial(_playerTextureData[aTeam].first,
                                    aDims.first - kSpriteFeetXOffset,
                                    aDims.second - kSpriteFeetYOffset,
                                    kSpriteWidth,
                                    kSpriteHeight,
                                    kSpriteWidth * _spriteColumn[aTeam][aPlayer],
                                    kSpriteHeight * aSpriteRow);

        _sortedPlayerLastIndex++;
    }

    if (_sortedPlayerLastIndex == kNumAllPlayers)
        _sortedPlayerLastIndex = 0;
}

void Players::drawMarkers(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
    for (std::size_t aTeam = Constants::Teams::kTeam0; aTeam < Constants::Teams::kNumTeams; aTeam++)
    {
        const std::size_t aPlayerIdx{iReadOnlyData._players._controlledPlayer[aTeam]};
        const ::Players::DroneData& aPlayer{iReadOnlyData._players._players[aTeam][aPlayerIdx]};

        SdlDimensions aDims{};
        aDims = _mapper.fieldToCameraView(aPlayer._posX_m, aPlayer._posY_m);

        // The player marker is capped at the camera edges to indicate where the player is
        SdlDimensions aCameraDims{_mapper.getCameraDimensions()};
        aDims.first = std::max(std::min(aDims.first, aCameraDims.first), 0);
        aDims.second = std::max(std::min(aDims.second, aCameraDims.second), 0);

        // There are two different types of markers: no possession, and possessing the ball
        // There are different colour markers, for each of the teams also
        // Possession markers are along the image width, the different teams are along the image height
        const int aMarkerWidth = _markerTextureData.second.first / 2;
        const int aMarkerHeight = _markerTextureData.second.second / 2;

        const bool aInPossession{iReadOnlyData._rules._currentlyPossessed &&
                                 iReadOnlyData._rules._possessionTeam == aTeam};

        _mapper.blitRelativePartial(_markerTextureData.first,
                                    aDims.first - kSpriteFeetXOffset,
                                    aDims.second - kMarkerYOffset,
                                    aMarkerWidth,
                                    aMarkerHeight,
                                    (aInPossession ? aMarkerWidth : 0),
                                    aTeam * aMarkerHeight);
    }
}

}   // namespace Video
