/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_VIDEO_BAR
#define RL_VIDEO_BAR

#include "GlobalConstants.hpp"
#include "SdlCtx.hpp"

#include <string>


struct ISystemData;

namespace State
{

struct Global;

}

namespace Video
{

class Mapper;

}


namespace Video
{

class Bar
{
public:
    Bar(const Mapper& iMapper);

    ~Bar() = default;

    Bar(const Bar&) = delete;
    Bar& operator=(const Bar&) = delete;
    Bar(Bar&&) = delete;
    Bar& operator=(Bar&&) = delete;

    //Set the Texture data
    void setBarTextCaches(SdlTextCache&& ioTextCacheScoreLH,
                          SdlTextCache&& ioTextCacheNameLH,
                          SdlTextCache&& ioTextCacheScoreRH,
                          SdlTextCache&& ioTextCacheNameRH,
                          SdlTextCache&& ioTextCacheTime);

    // Draw the bar
    void drawBar(const State::Global& iReadOnlyData, ISystemData& oSystemData);

private:
    const Mapper& _mapper;

    bool _firstDraw;

    SdlTextCache _textScore[Constants::Teams::kNumTeams];
    SdlTextCache _textName[Constants::Teams::kNumTeams];
    SdlTextCache _textTime;

    std::string _teamScore[Constants::Teams::kNumTeams];
    std::string _teamName[Constants::Teams::kNumTeams];

    unsigned short _previousScore[Constants::Teams::kNumTeams];
    double _highlightStartTime_ms[Constants::Teams::kNumTeams];
};

}

#endif   // RL_VIDEO_BAR
