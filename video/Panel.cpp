/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "video/Panel.hpp"

#include "video/Mapper.hpp"

#include "GlobalConstants.hpp"
#include "GlobalData.hpp"

#include <string>


// Anonymous namespace
namespace
{

//XYZ FOR NOW, HARD CODE THE TEAM THE PANEL FOCUSES ON, UNTIL A BETTER METHOD IS IMPLEMENTED
constexpr Constants::Teams::TeamsType kTeam{Constants::Teams::kTeamA};

//XYZ MOVE TO CONSTS
constexpr int kMarginSpacingHorizontal{20};
constexpr int kLineOneSpacingVertical{10};
constexpr int kLineTwoSpacingVertical{kLineOneSpacingVertical * 2};

// Populated in class constructor, so not actually constant
std::array<std::string, Constants::Rules::kNumSelectableKicks> kSelectableKickStrings{};

}   // Anonymous namespace


namespace Video
{

Panel::Panel(const Mapper& iMapper) :
    _mapper{iMapper},
    _textGameState{},
    _textChoices{}
{
    kSelectableKickStrings[Constants::Rules::kControlledKickSelection] = "Long Kick";
    kSelectableKickStrings[Constants::Rules::kDropKickSelection] = "Drop Kick";
    kSelectableKickStrings[Constants::Rules::kPlaceKickSelection] = "Place Kick";
}

void Panel::setPanelTextCaches(SdlTextCache&& ioTextCacheGameState,
                               const std::array<SdlTextCache, kMaxNumChoices>& iTextChoices)
{
    _textGameState = ioTextCacheGameState;
    _textChoices = iTextChoices;
}

void Panel::drawPanel(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
    bool aRulingMade{false};
    std::string aState{};   // with current config, supports 12 chars

    switch (iReadOnlyData._rules._gameState)
    {
        case Constants::Rules::GameState::kInPlay:
            aState = "In Play";
            break;
        case Constants::Rules::GameState::kKickOff:
            aState = "Kick-Off";
            break;
        case Constants::Rules::GameState::kWaiting:
            aRulingMade = true;
            break;
        default:
            aState = "<UNKNOWN>";
            break;
    }

    if (aRulingMade)
    {
        switch (iReadOnlyData._rules._ruling)
        {
            case Constants::Rules::Ruling::kFieldGoal:
                aState = "Field Goal";
                break;
            case Constants::Rules::Ruling::kNoRuling:
                aState = "<UNKNOWN>";
                break;
            default:
                aState = "<UNKNOWN>";
                break;
        }
    }

    int aOffsetY{kLineTwoSpacingVertical};

    _mapper.setDrawColour(Constants::Video::kPanelActiveColour);

    _mapper.printRelative(_textGameState,
                          aState,
                          Constants::Video::kPanelWidth / 2,
                          aOffsetY,
                          SdlHorzTextJustify::kCentre,
                          SdlVertTextJustify::kTop);

    aOffsetY += _mapper.getTextDimensions(_textGameState).second + kLineTwoSpacingVertical;

    if (iReadOnlyData._rules._selectionType[kTeam] == Constants::Rules::SelectionType::kSelectKick)
    {
        for (std::size_t aSelectableKickIdx = 0;
             aSelectableKickIdx < Constants::Rules::kNumSelectableKicks;
             aSelectableKickIdx++)
        {
            _mapper.setDrawColour(iReadOnlyData._rules._kickTypeSelectable[kTeam][aSelectableKickIdx] ?
                                  Constants::Video::kPanelActiveColour :
                                  Constants::Video::kPanelInactiveColour);

            _mapper.preprint(_textChoices[aSelectableKickIdx], kSelectableKickStrings[aSelectableKickIdx]);

            //XYZ HACKY PRETEND SELECTION MARKINGS:
            if ((aSelectableKickIdx == Constants::Rules::kDropKickSelection ||
                 aSelectableKickIdx == Constants::Rules::kPlaceKickSelection) &&
                 iReadOnlyData._rules._kickTypeSelectable[kTeam][aSelectableKickIdx])
            {
                _mapper.setDrawColour(Constants::Video::kPanelInactiveColour);
                SdlDimensions aDims = _mapper.getTextDimensions(_textChoices[aSelectableKickIdx]);
                _mapper.drawRectangleRelative(kMarginSpacingHorizontal, aOffsetY, aDims);
            }
            //XYZ HACKY PRETEND SELECTION MARKINGS:

            _mapper.reprintRelative(_textChoices[aSelectableKickIdx],
                                    kMarginSpacingHorizontal,
                                    aOffsetY,
                                    SdlHorzTextJustify::kLeft,
                                    SdlVertTextJustify::kTop);

            aOffsetY += _mapper.getTextDimensions(_textChoices[aSelectableKickIdx]).second + kLineOneSpacingVertical;
        }
    }
}

}   // namespace Video
