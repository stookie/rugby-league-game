/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_VIDEO_BALL
#define RL_VIDEO_BALL

#include "SdlCtx.hpp"


struct ISystemData;

namespace State
{

struct Global;

}

namespace Video
{

class Mapper;

}


namespace Video
{

class Ball
{
public:
    Ball(const Mapper& iMapper);

    ~Ball() = default;

    Ball(const Ball&) = delete;
    Ball& operator=(const Ball&) = delete;
    Ball(Ball&&) = delete;
    Ball& operator=(Ball&&) = delete;

    // Set the Texture data
    void setBallTextureData(SdlTextureData&& ioTextureData);

    // Draw the ball
    void drawBall(const State::Global& iReadOnlyData, ISystemData& oSystemData);

private:
    // Draw the ball, for sure (no hiding)
    void drawBallAlways(const State::Global& iReadOnlyData, ISystemData& oSystemData);

    const Mapper& _mapper;
    SdlTextureData _ballTextureData;

    unsigned char _spriteAnimationFrame;   // which frame of the animation is currently used

    double _spriteFrameStartTime_ms;   // time at which the current frame was started
};

}

#endif   // RL_VIDEO_BALL
