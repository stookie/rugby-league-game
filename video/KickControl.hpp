/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_VIDEO_KICK_CONTROL
#define RL_VIDEO_KICK_CONTROL

#include "GlobalConstants.hpp"
#include "SdlCtx.hpp"


struct ISystemData;

namespace State
{

struct Global;

}

namespace Video
{

class Mapper;

}


namespace Video
{

class KickControl
{
public:
    KickControl(const Mapper& iMapper);

    ~KickControl() = default;

    KickControl(const KickControl&) = delete;
    KickControl& operator=(const KickControl&) = delete;
    KickControl(KickControl&&) = delete;
    KickControl& operator=(KickControl&&) = delete;

    // Set the Texture data
    void setKickControlLevelTextureData(SdlTextureData&& ioKickControlLevelTextureData);

    void setKickPowerTexturesData(SdlTextureData&& ioKickPowerFrameTextureData,
                                  SdlTextureData&& ioKickPowerLevelTextureData);

    void setKickSnapTexturesData(SdlTextureData&& ioKickSnapFrameTextureData,
                                 SdlTextureData&& ioKickSnapLevelTextureData);

    void setKickDirectionTextureData(SdlTextureData&& ioKickDirectionTextureData);

    // Draw the kick controls
    void drawKickControls(const State::Global& iReadOnlyData, ISystemData& oSystemData);

private:
    // Draw the power frame/level and the snap frame/level
    void drawKickPowerAndSnap(const State::Global& iReadOnlyData, std::size_t iTeam);

    // Draw the directional aiming arrow
    void drawKickDirection(const State::Global& iReadOnlyData, std::size_t iTeam);

    const Mapper& _mapper;

    SdlTextureData _kickControlLevelTextureData;
    SdlTextureData _kickPowerFrameTextureData;
    SdlTextureData _kickPowerLevelTextureData;
    SdlTextureData _kickSnapFrameTextureData;
    SdlTextureData _kickSnapLevelTextureData;
    SdlTextureData _kickDirectionTextureData;

    double _remainOnScreenTimer_ms[Constants::Teams::kNumTeams];   // timer to leave kick control on screen for a while
    double _prevAngle_deg[Constants::Teams::kNumTeams];
    double _prevIndicator[Constants::Teams::kNumTeams];
    double _prevPower[Constants::Teams::kNumTeams];
    double _prevSnap[Constants::Teams::kNumTeams];
};

}

#endif   // RL_VIDEO_KICK_CONTROL
