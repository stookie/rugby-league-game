/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_VIDEO_DATA
#define RL_VIDEO_DATA

#include "ISystemData.hpp"


namespace Video
{

struct Data : ISystemData
{
public:
    Data() = default;
    ~Data() = default;

    Data(const Data&) = delete;
    Data& operator=(const Data&) = default;   // default
    Data(Data&&) = delete;
    Data& operator=(Data&&) = delete;

    double _goalPostRadius_m;
    double _goalPostHeight_m;
    double _goalBarRadius_m;
};

}

#endif   // RL_VIDEO_DATA
