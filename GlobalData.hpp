/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_MAIN_GLOBAL_STATE
#define RL_MAIN_GLOBAL_STATE

#include "ball/Data.hpp"
#include "camera/Data.hpp"
#include "collision/Data.hpp"
#include "input/Data.hpp"
#include "players/Data.hpp"
#include "rng/Data.hpp"
#include "rules/Data.hpp"
#include "video/Data.hpp"

#include <SDL2/SDL.h>

#include <cstddef>


namespace State
{

struct Main
{
    double _iterationRate_Hz;
    double _iterationDelay_s;
    double _iterationDelay_ms;

    Uint32 _timeSinceStart_ms;   // real time since start
    double _gameTimeSinceStart_ms;   // game time since start
};

struct Global
{
    Main _main;
    Ball::Data _ball;
    Camera::Data _camera;
    Collision::Data _collision;
    Input::Data _input;
    Players::Data _players;
    RNG::Data _rng;
    Rules::Data _rules;
    Video::Data _video;
};

}

#endif   // RL_MAIN_GLOBAL_STATE
