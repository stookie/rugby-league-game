/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_MAIN_DISPATCHER
#define RL_MAIN_DISPATCHER

#include "SdlCtx.hpp"
#include "GlobalData.hpp"

#include <memory>
#include <stdexcept>
#include <vector>


class ISystem;
class ISdlSystem;

namespace State
{

struct Global;

}


class DispatcherException : public std::runtime_error
{
    using std::runtime_error::runtime_error;
};


class Dispatcher
{
public:
    Dispatcher(double iIterationRate_Hz = 30.0);
    ~Dispatcher();

    Dispatcher(const Dispatcher&) = delete;
    Dispatcher& operator=(const Dispatcher&) = delete;
    Dispatcher(Dispatcher&&) = delete;
    Dispatcher& operator=(Dispatcher&&) = delete;

    // Run the dispather
    void run();

private:
    // Type aliases to represent a system, and its structure in the global write state
    using SystemDataPair = std::pair<std::unique_ptr<ISystem>, ISystemData&>;
    using SdlSystemDataPair = std::pair<std::unique_ptr<ISdlSystem>, ISystemData&>;

    // Populate the vector of systems
    void populateSystems();

    // Initialise all of the systems
    void initialiseAll();

    // Execute each of the systems, iteratively
    void iterateAll();

    // Finalise all of the systems
    void finaliseAll();

    // Copy the latest write state to the read state
    void updateGlobalReadState();

    SdlCtx _sdlCtx;
    std::vector<SystemDataPair> _systems;
    std::vector<SdlSystemDataPair> _inputSdlSystems;
    std::vector<SdlSystemDataPair> _outputSdlSystems;
    State::Global _readGlobalStateWriteAccess;
    const State::Global& _readGlobalState;
    State::Global _writeGlobalState;
};

#endif   // RL_MAIN_DISPATCHER
