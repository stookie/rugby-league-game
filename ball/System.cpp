/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "ball/System.hpp"

#include "ball/Data.hpp"

#include "GlobalData.hpp"


namespace Ball
{

void System::initialise(const State::Main& iReadOnlyMainData, ISystemData& oSystemData)
{
    Data& aData = static_cast<Data&>(oSystemData);

    // This initial movement is just temporary as a demo, so dont add to global constants
    aData._posX_m = 0.0;
    aData._posY_m = -5.0;
    aData._posZ_m = 0.0;
    aData._velX_mps = 0.0;
    aData._velY_mps = 4.5;
    aData._velZ_mps = 0.0;

    aData._bouncing = false;

    _deltaDecel_mpspIt = Constants::Ball::kDecel_mps2 * iReadOnlyMainData._iterationDelay_s;
}

void System::iterate(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
    Data& aData = static_cast<Data&>(oSystemData);

    bool aHeldByAnyTeam{false};

    for (std::size_t aTeam = Constants::Teams::kTeam0; aTeam < Constants::Teams::kNumTeams && !aHeldByAnyTeam; aTeam++)
    {
        if (iReadOnlyData._rules._currentlyPossessed && iReadOnlyData._rules._possessionTeam == aTeam)
        {
            aHeldByAnyTeam = true;

            if (iReadOnlyData._players._controlled[aTeam]._kickBall)
            {
                // Set ball velocity to the pre-determined release velocity
                aData._velX_mps = iReadOnlyData._players._controlled[aTeam]._ballReleaseVelX_mps;
                aData._velY_mps = iReadOnlyData._players._controlled[aTeam]._ballReleaseVelY_mps;
                aData._velZ_mps = iReadOnlyData._players._controlled[aTeam]._ballReleaseVelZ_mps;
                aData._bouncing = true;
            }
            else
            {
                // Same velocity as player
                aData._velX_mps = iReadOnlyData._players._controlled[aTeam]._velX_mps;
                aData._velY_mps = iReadOnlyData._players._controlled[aTeam]._velY_mps;
                aData._velZ_mps = 0.0;
                aData._bouncing = true;
            }

            // Same position as player
            aData._posX_m = iReadOnlyData._players._controlled[aTeam]._posX_m;
            aData._posY_m = iReadOnlyData._players._controlled[aTeam]._posY_m;
            aData._posZ_m = Constants::Players::kBoundingBoxHeight_m / 2.0;

            _prevBallGoalPostCollision = false;
            _prevBallGoalBarCollision = false;
        }
    }

    if (!aHeldByAnyTeam)
    {
        if (aData._bouncing == true)
        {
            // No change to X and Y velocity
            aData._velZ_mps -= Constants::Maths::kGravity_mps2 * iReadOnlyData._main._iterationDelay_s;
        }
        else
        {
            // No Z velocity if rolling and not bouncing
            aData._velZ_mps = 0.0;

            // Ball speed
            double aSpeed_mps = std::sqrt(iReadOnlyData._ball._velX_mps * iReadOnlyData._ball._velX_mps +
                                          iReadOnlyData._ball._velY_mps * iReadOnlyData._ball._velY_mps);

            double aNewSpeed_mps = std::max(aSpeed_mps - _deltaDecel_mpspIt, 0.0);
            double aRatio{0.0};

            if (aNewSpeed_mps > _deltaDecel_mpspIt / 2.0)
            {
                aRatio = aNewSpeed_mps / aSpeed_mps;
            }

            aData._velX_mps = aData._velX_mps * aRatio;
            aData._velY_mps = aData._velY_mps * aRatio;
        }

        // Temporarily, treat the side and dead-ball lines as bouncy walls
        // It is clumsy, but will not be required later, so live with it for now
        constexpr double kFieldLimitWidth_m = Constants::Field::kWidth_m / 2.0;
        constexpr double kFieldLimitLength_m = Constants::Field::kLength_m / 2.0 + Constants::Field::kIngoal_m;

        if (aData._posX_m < -kFieldLimitWidth_m)
            aData._velX_mps = std::abs(aData._velX_mps);
        else if (aData._posX_m > kFieldLimitWidth_m)
            aData._velX_mps = -std::abs(aData._velX_mps);

        if (aData._posY_m < -kFieldLimitLength_m)
            aData._velY_mps = std::abs(aData._velY_mps);
        else if (aData._posY_m > kFieldLimitLength_m)
            aData._velY_mps = -std::abs(aData._velY_mps);
        // End of bouncy wall logic

        // For the moment, when the ball collides with the goal posts/bar, it
        //  just rebounds perfectly backwards.
        if ((iReadOnlyData._collision._ballGoalPostCollision[Constants::Field::kNorthEnd] ||
             iReadOnlyData._collision._ballGoalPostCollision[Constants::Field::kSouthEnd]) &&
            !_prevBallGoalPostCollision)
        {
            aData._velX_mps *= -Constants::Ball::kGoalReboundFactor;
            aData._velY_mps *= -Constants::Ball::kGoalReboundFactor;
            aData._velZ_mps *= Constants::Ball::kGoalReboundFactor;
        }
        else if ((iReadOnlyData._collision._ballGoalBarCollision[Constants::Field::kNorthEnd] ||
                  iReadOnlyData._collision._ballGoalBarCollision[Constants::Field::kSouthEnd]) &&
                 !_prevBallGoalBarCollision)
        {
            aData._velX_mps *= Constants::Ball::kGoalReboundFactor;
            aData._velY_mps *= -Constants::Ball::kGoalReboundFactor;
            aData._velZ_mps *= -Constants::Ball::kGoalReboundFactor;
        }

        _prevBallGoalPostCollision = iReadOnlyData._collision._ballGoalPostCollision[Constants::Field::kNorthEnd] ||
                                     iReadOnlyData._collision._ballGoalPostCollision[Constants::Field::kSouthEnd];

        _prevBallGoalBarCollision = iReadOnlyData._collision._ballGoalBarCollision[Constants::Field::kNorthEnd] ||
                                    iReadOnlyData._collision._ballGoalBarCollision[Constants::Field::kSouthEnd];

        aData._posX_m += aData._velX_mps * iReadOnlyData._main._iterationDelay_s;
        aData._posY_m += aData._velY_mps * iReadOnlyData._main._iterationDelay_s;
        aData._posZ_m += aData._velZ_mps * iReadOnlyData._main._iterationDelay_s;

        // If not rolling, but ball position is below ground level, then bounce the ball
        if (aData._posZ_m < 0.0 && aData._bouncing == true)
        {
            aData._posZ_m = 0.0;

            aData._velX_mps *= Constants::Ball::kBounceCoefficient;
            aData._velY_mps *= Constants::Ball::kBounceCoefficient;
            aData._velZ_mps = Constants::Ball::kBounceCoefficient * std::fabs(aData._velZ_mps);

            if (aData._velZ_mps < Constants::Ball::kRollingVerticalVel_mps)
            {
                aData._bouncing = false;
                aData._posZ_m = 0.0;
                aData._velZ_mps = 0.0;
            }
        }
    }
}

void System::finalise(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
}

}   // namespace Ball
