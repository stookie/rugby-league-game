/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_BALL_DATA
#define RL_BALL_DATA

#include "ISystemData.hpp"


namespace Ball
{

struct Data : ISystemData
{
public:
    Data() = default;
    ~Data() = default;

    Data(const Data&) = delete;
    Data& operator=(const Data&) = default;   // default
    Data(Data&&) = delete;
    Data& operator=(Data&&) = delete;

    double _posX_m;
    double _posY_m;
    double _posZ_m;

    double _velX_mps;
    double _velY_mps;
    double _velZ_mps;

    bool _bouncing;
};

}

#endif   // RL_BALL_DATA
