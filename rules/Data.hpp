/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_RULES_DATA
#define RL_RULES_DATA

#include "ISystemData.hpp"

#include "GlobalConstants.hpp"


namespace Rules
{

struct Data : ISystemData
{
public:
    Data() = default;
    ~Data() = default;

    Data(const Data&) = delete;
    Data& operator=(const Data&) = default;   // default
    Data(Data&&) = delete;
    Data& operator=(Data&&) = delete;

    unsigned short _score[Constants::Teams::kNumTeams];

    Constants::Rules::Ruling _ruling;
    Constants::Rules::GameState _gameState;
    bool _gameStateChanged;

    Constants::Rules::SelectionType _selectionType[Constants::Teams::kNumTeams];
    bool _kickTypeSelectable[Constants::Teams::kNumTeams][Constants::Rules::kNumSelectableKicks];

    bool _currentlyPossessed;
    Constants::Teams::TeamsType _possessionTeam;

    Constants::Field::EndType _directionOfTeam[Constants::Teams::kNumTeams];
    Constants::Teams::TeamsType _teamOfDirection[Constants::Field::kNumEnds];
};

}

#endif   // RL_RULES_DATA
