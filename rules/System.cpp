/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "rules/System.hpp"

#include "rules/Data.hpp"

#include "GlobalData.hpp"
#include "Log.hpp"


namespace Rules
{

System::System() :
    _nextRuling{Constants::Rules::Ruling::kNoRuling},
    _nextGameState{Constants::Rules::GameState::kInPlay},
    _nextGameStateChanged{false},
    _restartPossessionTeam{Constants::Rules::kStartTeamKickingOff},
    _restartGameState{Constants::Rules::GameState::kKickOff},
    _restartTimer_ms{0.0},
    _currentKickTeam{Constants::Teams::kNumTeams},
    _currentKickType{Constants::Players::kNoKick}
{
}

void System::initialise(const State::Main& iReadOnlyMainData, ISystemData& oSystemData)
{
    Data& aData = static_cast<Data&>(oSystemData);

    aData._score[Constants::Teams::kTeamA] = 0;
    aData._score[Constants::Teams::kTeamB] = 0;

    aData._ruling = Constants::Rules::Ruling::kNoRuling;
    aData._gameState = Constants::Rules::GameState::kKickOff;
    aData._gameStateChanged = true;
    _nextRuling = Constants::Rules::Ruling::kNoRuling;
    _nextGameState = Constants::Rules::GameState::kInPlay;
    _nextGameStateChanged = false;
    _restartPossessionTeam = Constants::Rules::kStartTeamKickingOff;
    _restartGameState = Constants::Rules::GameState::kInPlay;
    _restartTimer_ms = 0.0;

    aData._currentlyPossessed = true;
    aData._possessionTeam = Constants::Rules::kStartTeamKickingOff;

    for (Constants::Teams::TeamsTypeUT aTeam = Constants::Teams::kTeam0; aTeam < Constants::Teams::kNumTeams; aTeam++)
    {
        if (aTeam == Constants::Rules::kStartTeamKickingOff)
        {
            aData._selectionType[aTeam] = Constants::Rules::SelectionType::kSelectKick;
            aData._kickTypeSelectable[aTeam][Constants::Rules::kControlledKickSelection] = false;
            aData._kickTypeSelectable[aTeam][Constants::Rules::kDropKickSelection] = false;
            aData._kickTypeSelectable[aTeam][Constants::Rules::kPlaceKickSelection] = true;
        }
        else
        {
            aData._selectionType[aTeam] = Constants::Rules::SelectionType::kSelectNothing;
        }

        aData._directionOfTeam[aTeam] = Constants::Rules::kStartTeamRunningDirection[aTeam];

        if (aData._directionOfTeam[aTeam] == Constants::Field::kNorthEnd)
            aData._teamOfDirection[Constants::Field::kNorthEnd] = static_cast<Constants::Teams::TeamsType>(aTeam);
        else
            aData._teamOfDirection[Constants::Field::kSouthEnd] = static_cast<Constants::Teams::TeamsType>(aTeam);
    }
}

void System::iterate(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
    Data& aData = static_cast<Data&>(oSystemData);

    //XYZ REFACTOR THIS INTO MULTIPLE SUB METHODS

    if (_nextGameStateChanged)
    {
        aData._gameState = _nextGameState;
    }

    if (aData._gameState == Constants::Rules::GameState::kWaiting)
    {
        if (_nextGameStateChanged == true)
        {
            _nextGameStateChanged = false;
            aData._gameStateChanged = true;
            aData._ruling = _nextRuling;
            _nextRuling = Constants::Rules::Ruling::kNoRuling;
            TRC(TrcType::kGmSt, "Game state: Waiting");
            _restartTimer_ms = Constants::Rules::kWaitingStateDelay_ms;

            aData._selectionType[Constants::Teams::kTeamA] = Constants::Rules::SelectionType::kSelectNothing;
            aData._selectionType[Constants::Teams::kTeamB] = Constants::Rules::SelectionType::kSelectNothing;
        }
        else if (aData._gameStateChanged == true)
        {
            aData._gameStateChanged = false;
        }

        _restartTimer_ms -= iReadOnlyData._main._iterationDelay_ms;

        if (_restartTimer_ms <= 0.0)
        {
            _restartTimer_ms = 0.0;
            _nextGameState = _restartGameState;
            _nextGameStateChanged = true;
        }
    }
    else if (aData._gameState == Constants::Rules::GameState::kKickOff)
    {
        if (_nextGameStateChanged == true)
        {
            _nextGameStateChanged = false;
            aData._gameStateChanged = true;
            aData._ruling = Constants::Rules::Ruling::kNoRuling;
            TRC(TrcType::kGmSt, "Game state: Kick-Off");
            aData._currentlyPossessed = true;
            aData._possessionTeam = _restartPossessionTeam;
            _restartGameState = Constants::Rules::GameState::kInPlay;
            _restartPossessionTeam = Constants::Teams::kNumTeams;
            _currentKickTeam = Constants::Teams::kNumTeams;
            _currentKickType = Constants::Players::kNoKick;

            for (std::size_t aTeam = Constants::Teams::kTeam0; aTeam < Constants::Teams::kNumTeams; aTeam++)
            {
                if (aTeam == aData._possessionTeam)
                {
                    aData._selectionType[aTeam] = Constants::Rules::SelectionType::kSelectKick;
                    aData._kickTypeSelectable[aTeam][Constants::Rules::kControlledKickSelection] = false;
                    aData._kickTypeSelectable[aTeam][Constants::Rules::kDropKickSelection] = false;
                    aData._kickTypeSelectable[aTeam][Constants::Rules::kPlaceKickSelection] = true;
                }
                else
                {
                    aData._selectionType[aTeam] = Constants::Rules::SelectionType::kSelectNothing;
                }
            }
        }
        else if (aData._gameStateChanged == true)
        {
            aData._gameStateChanged = false;
        }

        if (iReadOnlyData._players._controlled[aData._possessionTeam]._kickBall)
        {
            _nextGameState = Constants::Rules::GameState::kInPlay;
            _nextGameStateChanged = true;
            aData._currentlyPossessed = false;
            _currentKickTeam = aData._possessionTeam;
            _currentKickType = iReadOnlyData._players._controlled[aData._possessionTeam]._kickType;
            //XYZ NEED TO TRACK KICK OFF SPECIFICALLY THOUGH, EG OUT ON FULL HAS DIFFERENT PENALTIES, CANT KICK GOAL AT KICKOFF
        }
    }
    else if (aData._gameState == Constants::Rules::GameState::kInPlay)
    {
        if (_nextGameStateChanged == true)
        {
            _nextGameStateChanged = false;
            aData._gameStateChanged = true;
            aData._ruling = Constants::Rules::Ruling::kNoRuling;
            TRC(TrcType::kGmSt, "Game state: In-Play");
        }
        else if (aData._gameStateChanged == true)
        {
            aData._gameStateChanged = false;
        }

        if (aData._currentlyPossessed)
        {
            for (std::size_t aTeam = Constants::Teams::kTeam0; aTeam < Constants::Teams::kNumTeams; aTeam++)
            {
                if (aTeam == aData._possessionTeam)
                {
                    aData._selectionType[aTeam] = Constants::Rules::SelectionType::kSelectKick;
                    aData._kickTypeSelectable[aTeam][Constants::Rules::kControlledKickSelection] = true;
                    aData._kickTypeSelectable[aTeam][Constants::Rules::kDropKickSelection] = true;
                    aData._kickTypeSelectable[aTeam][Constants::Rules::kPlaceKickSelection] = false;
                }
                else
                {
                    aData._selectionType[aTeam] = Constants::Rules::SelectionType::kSelectNothing;
                }
            }

            if (iReadOnlyData._players._controlled[aData._possessionTeam]._kickBall)
            {
                aData._currentlyPossessed = false;
                _currentKickTeam = aData._possessionTeam;
                _currentKickType = iReadOnlyData._players._controlled[aData._possessionTeam]._kickType;
            }
        }
        else
        {
            aData._selectionType[Constants::Teams::kTeamA] = Constants::Rules::SelectionType::kSelectNothing;
            aData._selectionType[Constants::Teams::kTeamB] = Constants::Rules::SelectionType::kSelectNothing;

            for (Constants::Teams::TeamsTypeUT aTeam = Constants::Teams::kTeam0;
                aTeam < Constants::Teams::kNumTeams;
                aTeam++)
            {
                if (iReadOnlyData._collision._ballPlayerCollision[aTeam] &&
                    !iReadOnlyData._players._controlled[aTeam]._releasingBall)
                {
                    aData._currentlyPossessed = true;
                    aData._possessionTeam = static_cast<Constants::Teams::TeamsType>(aTeam);
                    _currentKickTeam = Constants::Teams::kNumTeams;
                    _currentKickType = Constants::Players::kNoKick;
                    break;
                }
            }
        }

        if (_currentKickType == Constants::Players::kDropKick)
        {
            for (std::size_t aEnd = 0; aEnd < Constants::Field::kNumEnds; aEnd++)
            {
                if (iReadOnlyData._collision._potentialGoal[aEnd] && aData._directionOfTeam[_currentKickTeam] == aEnd)
                {
                    ++(aData._score[_currentKickTeam]);
                    _nextRuling = Constants::Rules::Ruling::kFieldGoal;
                    _nextGameState = Constants::Rules::GameState::kWaiting;
                    _nextGameStateChanged = true;
                    _restartGameState = Constants::Rules::GameState::kKickOff;
                    _restartPossessionTeam = (_currentKickTeam == Constants::Teams::kTeamA ?
                                              Constants::Teams::kTeamB :
                                              Constants::Teams::kTeamA);
                }
            }
        }
    }
}

void System::finalise(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
}

}   // namespace Rules
