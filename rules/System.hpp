/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_RULES_SYSTEM
#define RL_RULES_SYSTEM

#include "ISystem.hpp"

#include "GlobalConstants.hpp"


struct ISystemData;


namespace Rules
{

class System : public ISystem
{
public:
    System();
    virtual ~System() = default;

    System(const System&) = delete;
    System& operator=(const System&) = delete;
    System(System&&) = delete;
    System& operator=(System&&) = delete;

    virtual void initialise(const State::Main& iReadOnlyMainData, ISystemData& oSystemData) override;
    virtual void iterate(const State::Global& iReadOnlyData, ISystemData& oSystemData) override;
    virtual void finalise(const State::Global& iReadOnlyData, ISystemData& oSystemData) override;

private:
    // These are the internal controls to change the game state. They lead the global data equivalents by 1 iteration
    Constants::Rules::Ruling _nextRuling;
    Constants::Rules::GameState _nextGameState;
    bool _nextGameStateChanged;

    // These are set if the game start transitions and includes a restart
    Constants::Teams::TeamsType _restartPossessionTeam;   // the team in possession at the restart
    Constants::Rules::GameState _restartGameState;   // the type of restart
    double _restartTimer_ms;   // the time remaining before the restart

    Constants::Teams::TeamsType _currentKickTeam;
    Constants::Players::KickType _currentKickType;
};

}

#endif   // RL_RULES_SYSTEM
