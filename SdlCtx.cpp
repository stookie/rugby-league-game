/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "SdlCtx.hpp"

// #include <iostream>
#include <string>


SdlCtx::SdlCtx() :
    _sdlInit{false},
    _sdlImgInit{false},
    _sdlTtfInit{false},
    _window{nullptr},
    _render{nullptr},
    _resourcePath{},
    _loadedTextures{nullptr},   // reserve the first slot as a null entry
    _font{nullptr},
    _cachedText{TextData{}}   // reserve the first slot as a null entry
{
    bool aOk{true};
    std::string aError{};

    if (aOk && SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        aError = std::string{"SDL_Init Error: "} + SDL_GetError();
        aOk = false;
    }
    else
    {
        _sdlInit = true;
    }

    if (aOk && (IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG)
    {
        aError = std::string{"SDL IMG_Init Error: "} + IMG_GetError();
        aOk = false;
    }
    else
    {
        _sdlImgInit = true;
    }

    if (TTF_Init() != 0)
    {
        aError = std::string{"SDL TTF_Init Error: "} + TTF_GetError();
        aOk = false;
    }
    else
    {
        _sdlTtfInit = true;
    }

    if (aOk)
        _window = SDL_CreateWindow("Rugby League",
                                   SDL_WINDOWPOS_CENTERED,
                                   SDL_WINDOWPOS_CENTERED,
                                   50,
                                   50,
                                   SDL_WINDOW_SHOWN);

    if (aOk && _window == nullptr)
    {
        aError = std::string{"SDL_CreateWindow Error: "} + SDL_GetError();
        aOk = false;
    }

    if (aOk)
        _render = SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    if (aOk && _render == nullptr)
    {
        aError = std::string{"SDL_CreateRenderer Error: "} + SDL_GetError();
        aOk = false;
    }

    if (aOk)
    {
        char* aResourcePathTmp = SDL_GetBasePath();   // build directory
        if (aResourcePathTmp == nullptr)
        {
            aError = std::string{"SDL_GetBasePath Error: "} + SDL_GetError();
            aOk = false;
        }
        else
        {
            _resourcePath = aResourcePathTmp;
            SDL_free(aResourcePathTmp);
            _resourcePath += "/../";
        }
    }

    if (!aOk)
    {
        clean();
        throw SdlCtxException{aError};
    }

    // Give the window dummy dimensions
    setWindowSize(50, 50);
}

SdlCtx::~SdlCtx()
{
    clean();
}

void SdlCtx::delayMs(Uint32 iMsecs) const
{
    SDL_Delay(iMsecs);
}

Uint32 SdlCtx::getTimeSinceStartMs() const
{
    return SDL_GetTicks();
}

bool SdlCtx::pollEvents(SDL_Event& aEvent)
{
    return SDL_PollEvent(&aEvent);
}

void SdlCtx::setWindowSize(int iWidth, int iHeight)
{
    if (iWidth < 2 || iHeight < 2)
        throw SdlCtxException{"Invalid window dimensions"};

    SDL_SetWindowSize(_window, iWidth, iHeight);
    SDL_SetWindowPosition(_window, SDL_WINDOWPOS_CENTERED, 0 /*SDL_WINDOWPOS_CENTERED*/);
}

SdlDimensions SdlCtx::getWindowSize() const
{
    SdlDimensions aDims{};
    SDL_GetWindowSize(_window, &aDims.first, &aDims.second);
    return aDims;
}

void SdlCtx::setWindowClipArea(int iX, int iY, int iW, int iH)
{
    SDL_Rect aClipArea{iX, iY, iW, iH};
    if (SDL_RenderSetClipRect(_render, &aClipArea) != 0)
    {
        std::string aError{"SDL_RenderSetClipRect: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }
}

void SdlCtx::resetWindowClipArea()
{
    if (SDL_RenderSetClipRect(_render, nullptr) != 0)
    {
        std::string aError{"SDL_RenderSetClipRect: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }
}

SdlDimensions SdlCtx::getWindowClipAreaOrigin() const
{
    SDL_Rect aClipArea{};
    SDL_RenderGetClipRect(_render, &aClipArea);
    return SdlDimensions{aClipArea.x, aClipArea.y};
}

void SdlCtx::renderStart() const
{
    SDL_RenderClear(_render);
}

void SdlCtx::renderEnd() const
{
    SDL_RenderPresent(_render);
}

SdlTextureData SdlCtx::loadTexture(const std::string& iFilename)
{
    SDL_Texture* aTex{IMG_LoadTexture(_render, (_resourcePath + iFilename).c_str())};

    if (aTex == nullptr)
    {
        std::string aError{"SDL IMG_LoadTexture Error: "};
        throw SdlCtxException{aError + IMG_GetError()};
    }

    _loadedTextures.push_back(aTex);
    SdlTexture aTexture{_loadedTextures.size() - 1};
    return SdlTextureData{aTexture, getTextureDimensions(aTexture)};
}

bool SdlCtx::setTextureAlpha(SdlTexture iTextureId, bool iThrowOnError, Uint8 iAlpha)
{
    SDL_Texture* aTexture{getTexture(iTextureId)};

    const bool aOk = (SDL_SetTextureAlphaMod(aTexture, iAlpha) == 0);

    if (!aOk && iThrowOnError)
    {
        std::string aError{"SDL_SetTextureAlphaMod Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }

    return aOk;
}

SdlDimensions SdlCtx::getTextureDimensions(SdlTexture iTextureId) const
{
    Uint32 aFormat;
    int aAccess;
    SdlDimensions aDimensions{};

    SDL_Texture* aTexture{getTexture(iTextureId)};

    if (SDL_QueryTexture(aTexture, &aFormat, &aAccess, &aDimensions.first, &aDimensions.second) != 0)
    {
        std::string aError{"SDL_QueryTexture Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }

    return aDimensions;
}

void SdlCtx::blit(SdlTexture iTextureId, int iX, int iY, int iW, int iH) const
{
    SDL_Texture* aTexture{getTexture(iTextureId)};
    SDL_Rect aDestination{iX, iY, iW, iH};

    if (SDL_RenderCopy(_render, aTexture, nullptr, &aDestination) != 0)
    {
        std::string aError{"SDL_RenderCopy Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }
}

void SdlCtx::blit(SdlTexture iTextureId, int iX, int iY, SdlDimensions iDimensions) const
{
    blit(iTextureId, iX, iY, iDimensions.first, iDimensions.second);
}

void SdlCtx::blitPartial(SdlTexture iTextureId, int iX, int iY, int iW, int iH, int iDX, int iDY) const
{
    SDL_Texture* aTexture{getTexture(iTextureId)};
    SDL_Rect aSource{iDX, iDY, iW, iH};
    SDL_Rect aDestination{iX, iY, iW, iH};

    if (SDL_RenderCopy(_render, aTexture, &aSource, &aDestination) != 0)
    {
        std::string aError{"SDL_RenderCopy Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }
}

void SdlCtx::blitPartial(SdlTexture iTextureId, int iX, int iY, SdlDimensions iDimensions, int iDX, int iDY) const
{
    blitPartial(iTextureId, iX, iY, iDimensions.first, iDimensions.second, iDX, iDY);
}

void SdlCtx::blitRotated(SdlTexture iTextureId, int iX, int iY, int iW, int iH, double iAngle_deg) const
{
    SDL_Texture* aTexture{getTexture(iTextureId)};
    SDL_Rect aDestination{iX, iY, iW, iH};

    if (SDL_RenderCopyEx(_render, aTexture, nullptr, &aDestination, iAngle_deg, nullptr, SDL_FLIP_NONE) != 0)
    {
        std::string aError{"SDL_RenderCopyEx Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }
}

void SdlCtx::blitRotated(SdlTexture iTextureId, int iX, int iY, SdlDimensions iDimensions, double iAngle_deg) const
{
    blitRotated(iTextureId, iX, iY, iDimensions.first, iDimensions.second, iAngle_deg);
}

void SdlCtx::blitRotated(SdlTexture iTextureId,
                         int iX,
                         int iY,
                         int iW,
                         int iH,
                         double iAngle_deg,
                         int iCX,
                         int iCY) const
{
    SDL_Texture* aTexture{getTexture(iTextureId)};
    SDL_Rect aDestination{iX, iY, iW, iH};
    SDL_Point aRotationPoint{iCX, iCY};

    if (SDL_RenderCopyEx(_render, aTexture, nullptr, &aDestination, iAngle_deg, &aRotationPoint, SDL_FLIP_NONE) != 0)
    {
        std::string aError{"SDL_RenderCopyEx Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }
}

void SdlCtx::blitRotated(SdlTexture iTextureId,
                         int iX,
                         int iY,
                         SdlDimensions iDimensions,
                         double iAngle_deg,
                         int iCX,
                         int iCY) const
{
    blitRotated(iTextureId, iX, iY, iDimensions.first, iDimensions.second, iAngle_deg, iCX, iCY);
}

void SdlCtx::setDrawColour(Uint8 iRed, Uint8 iGreen, Uint8 iBlue, Uint8 iAlpha)
{
    if (SDL_SetRenderDrawColor(_render, iRed, iGreen, iBlue, iAlpha) != 0)
    {
        std::string aError{"SDL_SetRenderDrawColor Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }
}

void SdlCtx::setDrawColour(uint32_t iHexColour)
{
    if (SDL_SetRenderDrawColor(_render,
                               static_cast<Uint8>((iHexColour & 0xff000000) >> 24),
                               static_cast<Uint8>((iHexColour & 0x00ff0000) >> 16),
                               static_cast<Uint8>((iHexColour & 0x0000ff00) >> 8),
                               static_cast<Uint8>((iHexColour & 0x000000ff))) != 0)
    {
        std::string aError{"SDL_SetRenderDrawColor Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }
}

void SdlCtx::setBlendMode(SDL_BlendMode iMode)
{
    if (SDL_SetRenderDrawBlendMode(_render, iMode) != 0)
    {
        std::string aError{"SDL_SetRenderDrawBlendMode Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }
}

SDL_BlendMode SdlCtx::getBlendMode()
{
    SDL_BlendMode aMode{};

    if (SDL_GetRenderDrawBlendMode(_render, &aMode) != 0)
    {
        std::string aError{"SDL_GetRenderDrawBlendMode Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }

    return aMode;
}

void SdlCtx::drawRectangle(int iX, int iY, int iW, int iH, bool iFill)
{
    SDL_Rect aRectangle{iX, iY, iW, iH};
    if (iFill && SDL_RenderFillRect(_render, &aRectangle) != 0)
    {
        std::string aError{"SDL_RenderFillRect Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }
    else if (!iFill && SDL_RenderDrawRect(_render, &aRectangle) != 0)
    {
        std::string aError{"SDL_RenderDrawRect Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }
}

void SdlCtx::loadFont(const std::string& iFilename, int iPointSize)
{
    if (_font != nullptr)
    {
        TTF_CloseFont(_font);
        _font = nullptr;
    }

    _font = TTF_OpenFont((_resourcePath + iFilename).c_str(), iPointSize);

    if (_font == nullptr)
    {
        std::string aError{"SDL TTF_OpenFont Error: "};
        throw SdlCtxException{aError + TTF_GetError()};
    }
}

SdlTextCache SdlCtx::reserveTextCache()
{
    _cachedText.push_back(TextData{});
    return SdlTextCache{_cachedText.size() - 1};
}

void SdlCtx::print(SdlTextCache iTextCache,
                   const std::string& iText,
                   int iX,   // LH/mid/RH point for Left/Centre/Right justify
                   int iY,   // Top/mid/base/bottom point for Top/Centre/Baseline/Bottom justify
                   SdlHorzTextJustify iHorzJustify,
                   SdlVertTextJustify iVertJustify)
{
    Uint8 aRed;
    Uint8 aGreen;
    Uint8 aBlue;
    Uint8 aAlpha;

    if (SDL_GetRenderDrawColor(_render, &aRed, &aGreen, &aBlue, &aAlpha) != 0)
    {
        std::string aError{"SDL_GetRenderDrawColor Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }

    print(iTextCache, iText, iX, iY, iHorzJustify, iVertJustify, aRed, aGreen, aBlue, aAlpha);
}

void SdlCtx::print(SdlTextCache iTextCache,
                   const std::string& iText,
                   int iX,   // LH/mid/RH point for Left/Centre/Right justify
                   int iY,   // Top/mid/base/bottom point for Top/Centre/Baseline/Bottom justify
                   SdlHorzTextJustify iHorzJustify,
                   SdlVertTextJustify iVertJustify,
                   Uint8 iRed,
                   Uint8 iGreen,
                   Uint8 iBlue,
                   Uint8 iAlpha)
{
    TextData& aTextData = getTextData(iTextCache);

    printText(aTextData, iText, iRed, iGreen, iBlue, iAlpha);
    printToWindow(aTextData, iX, iY, iHorzJustify, iVertJustify);
}

void SdlCtx::preprint(SdlTextCache iTextCache, const std::string& iText)
{
    Uint8 aRed;
    Uint8 aGreen;
    Uint8 aBlue;
    Uint8 aAlpha;

    if (SDL_GetRenderDrawColor(_render, &aRed, &aGreen, &aBlue, &aAlpha) != 0)
    {
        std::string aError{"SDL_GetRenderDrawColor Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }

    preprint(iTextCache, iText, aRed, aGreen, aBlue, aAlpha);
}

void SdlCtx::preprint(SdlTextCache iTextCache,
                      const std::string& iText,
                      Uint8 iRed,
                      Uint8 iGreen,
                      Uint8 iBlue,
                      Uint8 iAlpha)
{
    TextData& aTextData = getTextData(iTextCache);

    printText(aTextData, iText, iRed, iGreen, iBlue, iAlpha);
}

void SdlCtx::reprint(SdlTextCache iTextCache,
                     int iX,   // LH/mid/RH point for Left/Centre/Right justify
                     int iY,   // Top/mid/base/bottom point for Top/Centre/Baseline/Bottom justify
                     SdlHorzTextJustify iHorzJustify,
                     SdlVertTextJustify iVertJustify)
{
    const TextData& aTextData = getTextData(iTextCache);

    printToWindow(aTextData, iX, iY, iHorzJustify, iVertJustify);
}

SdlDimensions SdlCtx::getTextDimensions(SdlTextCache iTextCache)
{
    TextData& aTextData = getTextData(iTextCache);
    return aTextData.second.second;
}

void SdlCtx::clean()
{
    for (auto& aText : _cachedText)
    {
        SDL_DestroyTexture(aText.second.first);
        aText.second.first = nullptr;
    }

    for (auto& aTex : _loadedTextures)
    {
        SDL_DestroyTexture(aTex);
        aTex = nullptr;
    }

    if (_font)
        TTF_CloseFont(_font);

    if (_render)
        SDL_DestroyRenderer(_render);

    if (_window)
        SDL_DestroyWindow(_window);

    if (_sdlTtfInit)
        TTF_Quit();

    if (_sdlImgInit)
        IMG_Quit();

    if (_sdlInit)
        SDL_Quit();
}

SDL_Texture* SdlCtx::getTexture(SdlTexture iTextureId) const
{
    if (iTextureId._id >= _loadedTextures.size())
        throw SdlCtxException{"Invalid texture ID"};

    return _loadedTextures[iTextureId._id];
}

SdlCtx::TextData& SdlCtx::getTextData(SdlTextCache iTextCache)
{
    if (iTextCache._id >= _cachedText.size() || iTextCache._id == SdlTextCache{}._id)
        throw SdlCtxException{"Invalid text cache"};

    return _cachedText[iTextCache._id];
}

bool SdlCtx::printText(TextData& ioTextData,
                       const std::string& iText,
                       Uint8 iRed,
                       Uint8 iGreen,
                       Uint8 iBlue,
                       Uint8 iAlpha)
{
    if (ioTextData.first.first != iText ||
        ioTextData.first.second.r != iRed || ioTextData.first.second.g != iGreen ||
        ioTextData.first.second.b != iBlue || ioTextData.first.second.a != iAlpha)
    {
        if (_font == nullptr)
        {
            throw SdlCtxException{"Font not loaded"};
        }

        // Destroy the stale texture
        SDL_DestroyTexture(ioTextData.second.first);
        ioTextData.second.first = nullptr;

        // Regenerate the new cache data
        SDL_Color aColour{iRed, iGreen, iBlue, iAlpha};
        SDL_Surface* aSurface = TTF_RenderText_Blended(_font, iText.c_str(), aColour);

        if (aSurface == nullptr)
        {
            std::string aError{"SDL TTF_RenderText_Blended Error: "};
            throw SdlCtxException{aError + TTF_GetError()};
        }

        SDL_Texture* aTexture = SDL_CreateTextureFromSurface(_render, aSurface);
        SDL_FreeSurface(aSurface);
        aSurface = nullptr;

        if (aTexture == nullptr)
        {
            std::string aError{"SDL_CreateTextureFromSurface Error: "};
            throw SdlCtxException{aError + SDL_GetError()};
        }

        int aW{};
        int aH{};
        Uint32 aFormat{};
        int aAccess{};

        if (SDL_QueryTexture(aTexture, &aFormat, &aAccess, &aW, &aH) != 0)
        {
            SDL_DestroyTexture(aTexture);
            std::string aError{"SDL_QueryTexture Error: "};
            throw SdlCtxException{aError + SDL_GetError()};
        }

        ioTextData.first.first = iText;
        ioTextData.first.second = aColour;
        ioTextData.second.first = aTexture;
        ioTextData.second.second = SdlDimensions{aW, aH};
        return true;
    }
    else
    {
        // Cached text data is current, do nothing
        return false;
    }
}

void SdlCtx::printToWindow(const TextData& ioTextData,
                           int iX,
                           int iY,
                           SdlHorzTextJustify iHorzJustify,
                           SdlVertTextJustify iVertJustify)
{
    SDL_Rect aDimensions{iX, iY, ioTextData.second.second.first, ioTextData.second.second.second};

    switch (iHorzJustify)
    {
        case SdlHorzTextJustify::kLeft:
            // Nothing required
            break;
        case SdlHorzTextJustify::kRight:
            aDimensions.x -= aDimensions.w;
            break;
        case SdlHorzTextJustify::kCentre:
        default:
            aDimensions.x -= aDimensions.w / 2;
            break;
    }

    switch (iVertJustify)
    {
        case SdlVertTextJustify::kTop:
            // Nothing required
            break;
        case SdlVertTextJustify::kBottom:
            aDimensions.y -= aDimensions.h;
            break;
        case SdlVertTextJustify::kCentre:
        default:
            aDimensions.y -= aDimensions.h / 2;
            break;
    }

    if (SDL_RenderCopy(_render, ioTextData.second.first, nullptr, &aDimensions) != 0)
    {
        std::string aError{"SDL_RenderCopy Error: "};
        throw SdlCtxException{aError + SDL_GetError()};
    }
}
