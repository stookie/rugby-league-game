/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "collision/PlayerProxy.hpp"

#include "collision/Data.hpp"
#include "collision/BallProxy.hpp"

#include "GlobalConstants.hpp"
#include "GlobalData.hpp"

#include <cassert>


namespace Collision
{
PlayerProxy::PlayerProxy(Constants::Teams::TeamsType iTeam, std::size_t iNumber) :
    _boundingBox{},
    _team{iTeam},
    _number{iNumber},
    _ballCollision{}
{
    assert(iTeam == Constants::Teams::kTeamA || iTeam == Constants::Teams::kTeamB);
    assert(iNumber < Constants::Rules::kPlayers);
}

void PlayerProxy::getInputs(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
    constexpr double kPlayerBoundingBoxHalfWidth_m{Constants::Players::kBoundingBoxWidth_m / 2.0};
    constexpr double kPlayerBoundingBoxHalfLength_m{Constants::Players::kBoundingBoxLength_m / 2.0};

    // All coordinates for bounding box point 0 must be less than for point 1
    _boundingBox[kPt0][kX] = iReadOnlyData._players._players[_team][_number]._posX_m - kPlayerBoundingBoxHalfWidth_m;
    _boundingBox[kPt1][kX] = iReadOnlyData._players._players[_team][_number]._posX_m + kPlayerBoundingBoxHalfWidth_m;
    _boundingBox[kPt0][kY] = iReadOnlyData._players._players[_team][_number]._posY_m - kPlayerBoundingBoxHalfLength_m;
    _boundingBox[kPt1][kY] = iReadOnlyData._players._players[_team][_number]._posY_m + kPlayerBoundingBoxHalfLength_m;
    _boundingBox[kPt0][kZ] = 0.0;   // Players don't jump or dive (yet)
    _boundingBox[kPt1][kZ] = Constants::Players::kBoundingBoxHeight_m;
}

void PlayerProxy::setOutputs(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
    Data& aData = static_cast<Data&>(oSystemData);

    if (iReadOnlyData._players._controlledPlayer[_team] == _number)
    {
        aData._ballPlayerCollision[_team] = _ballCollision;
    }
}

bool PlayerProxy::collide(ICollisionProxy* iProxy)
{
    PlayerProxy* aPlayerProxy{dynamic_cast<PlayerProxy*>(iProxy)};

    if (aPlayerProxy != nullptr)
    {
        return collide(aPlayerProxy);
    }

    BallProxy* aBallProxy{dynamic_cast<BallProxy*>(iProxy)};

    if (aBallProxy != nullptr)
    {
        return collide(aBallProxy);
    }

    return iProxy->collide(this);
}

bool PlayerProxy::collide(BallProxy* iProxy)
{
    const BoxType& aBallBoundingBox{iProxy->boundingBox()};

    bool aOverlapX = !(aBallBoundingBox[kPt1][kX] < _boundingBox[kPt0][kX] ||
                       _boundingBox[kPt1][kX] < aBallBoundingBox[kPt0][kX]);

    bool aOverlapY = !(aBallBoundingBox[kPt1][kY] < _boundingBox[kPt0][kY] ||
                       _boundingBox[kPt1][kY] < aBallBoundingBox[kPt0][kY]);

    bool aOverlapZ = !(aBallBoundingBox[kPt1][kZ] < _boundingBox[kPt0][kZ] ||
                       _boundingBox[kPt1][kZ] < aBallBoundingBox[kPt0][kZ]);

    _ballCollision = aOverlapX && aOverlapY && aOverlapZ;

    return _ballCollision;
}

bool PlayerProxy::collide(PlayerProxy* iProxy)
{
    return false;   // no-op
}

const PlayerProxy::BoxType& PlayerProxy::boundingBox() const
{
    return _boundingBox;
}

}
