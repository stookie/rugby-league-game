/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_COLLISION_DATA
#define RL_COLLISION_DATA

#include "ISystemData.hpp"

#include "GlobalConstants.hpp"


namespace Collision
{

struct Data : ISystemData
{
public:
    Data() = default;
    ~Data() = default;

    Data(const Data&) = delete;
    Data& operator=(const Data&) = default;   // default
    Data(Data&&) = delete;
    Data& operator=(Data&&) = delete;

    bool _ballPlayerCollision[Constants::Teams::kNumTeams];

    bool _ballGoalPostCollision[Constants::Field::kNumEnds];
    bool _ballGoalBarCollision[Constants::Field::kNumEnds];
    bool _potentialGoal[Constants::Field::kNumEnds];
};

}

#endif   // RL_COLLISION_DATA
