/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "collision/GoalProxy.hpp"

#include "collision/Data.hpp"
#include "collision/BallProxy.hpp"
#include "collision/PlayerProxy.hpp"

#include "GlobalConstants.hpp"
#include "GlobalData.hpp"


namespace Collision
{

GoalProxy::GoalProxy(Constants::Field::EndType iFieldEnd) :
    _firstPass{true},
    _fieldEnd{iFieldEnd},
    _boundingBox{},
    _westPostBoundingBox{},
    _eastPostBoundingBox{},
    _barBoundingBox{},
    _ballPostCollision{},
    _ballBarCollision{},
    _ballPotentialGoal{},
    _ballPosX_m{},
    _ballPosY_m{},
    _ballPosZ_m{},
    _prevBallPosX_m{},
    _prevBallPosY_m{},
    _prevBallPosZ_m{}
{
}

void GoalProxy::getInputs(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
    if (_firstPass)
    {
        _firstPass = false;

        const double aInnerX_m = Constants::Field::kGoalPostSeparation_m / 2.0;
        const double aOuterX_m = aInnerX_m + iReadOnlyData._video._goalPostRadius_m * 2.0;
        const double aInnerY_m = Constants::Field::kLength_m / 2.0;
        const double aOuterY_m = aInnerY_m + iReadOnlyData._video._goalPostRadius_m * 2.0;

        _westPostBoundingBox[kPt0][kX] = -aOuterX_m;
        _westPostBoundingBox[kPt1][kX] = -aInnerX_m;
        _eastPostBoundingBox[kPt0][kX] = aInnerX_m;
        _eastPostBoundingBox[kPt1][kX] = aOuterX_m;

        if (_fieldEnd == Constants::Field::kNorthEnd)
        {
            _westPostBoundingBox[kPt0][kY] = -aOuterY_m;
            _westPostBoundingBox[kPt1][kY] = -aInnerY_m;
            _eastPostBoundingBox[kPt0][kY] = -aOuterY_m;
            _eastPostBoundingBox[kPt1][kY] = -aInnerY_m;
        }
        else
        {
            _westPostBoundingBox[kPt0][kY] = aInnerY_m;
            _westPostBoundingBox[kPt1][kY] = aOuterY_m;
            _eastPostBoundingBox[kPt0][kY] = aInnerY_m;
            _eastPostBoundingBox[kPt1][kY] = aOuterY_m;
        }

        _westPostBoundingBox[kPt0][kZ] = 0.0;
        _westPostBoundingBox[kPt1][kZ] = iReadOnlyData._video._goalPostHeight_m;
        _eastPostBoundingBox[kPt0][kZ] = 0.0;
        _eastPostBoundingBox[kPt1][kZ] = iReadOnlyData._video._goalPostHeight_m;

        _barBoundingBox[kPt0][kX] = -aInnerX_m;
        _barBoundingBox[kPt1][kX] = aInnerX_m;

        if (_fieldEnd == Constants::Field::kNorthEnd)
        {
            _barBoundingBox[kPt0][kY] = -aOuterY_m;
            _barBoundingBox[kPt1][kY] = -aInnerY_m;
        }
        else
        {
            _barBoundingBox[kPt0][kY] = aInnerY_m;
            _barBoundingBox[kPt1][kY] = aOuterY_m;
        }

        _barBoundingBox[kPt0][kZ] = Constants::Field::kGoalBarHeight_m;
        _barBoundingBox[kPt1][kZ] = Constants::Field::kGoalBarHeight_m + iReadOnlyData._video._goalBarRadius_m;

        // The "bounding box" is just the bounding box of the 2 posts and the bar
        _boundingBox[kPt0][kX] = -aOuterX_m;
        _boundingBox[kPt1][kX] = aOuterX_m;

        if (_fieldEnd == Constants::Field::kNorthEnd)
        {
            _boundingBox[kPt0][kY] = -aOuterY_m;
            _boundingBox[kPt1][kY] = -aInnerY_m;
        }
        else
        {
            _boundingBox[kPt0][kY] = aInnerY_m;
            _boundingBox[kPt1][kY] = aOuterY_m;
        }

        _boundingBox[kPt0][kZ] = 0.0;
        _boundingBox[kPt1][kZ] = iReadOnlyData._video._goalPostHeight_m;

        _ballPosX_m = iReadOnlyData._ball._posX_m;
        _ballPosY_m = iReadOnlyData._ball._posY_m;
        _ballPosZ_m = iReadOnlyData._ball._posZ_m;
    }   // first pass

    _prevBallPosX_m = _ballPosX_m;
    _prevBallPosY_m = _ballPosY_m;
    _prevBallPosZ_m = _ballPosZ_m;
    _ballPosX_m = iReadOnlyData._ball._posX_m;
    _ballPosY_m = iReadOnlyData._ball._posY_m;
    _ballPosZ_m = iReadOnlyData._ball._posZ_m;
}

void GoalProxy::setOutputs(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
    Data& aData = static_cast<Data&>(oSystemData);

    aData._ballGoalPostCollision[_fieldEnd] = _ballPostCollision;
    aData._ballGoalBarCollision[_fieldEnd] = _ballBarCollision;
    aData._potentialGoal[_fieldEnd] = _ballPotentialGoal;
}

bool GoalProxy::collide(ICollisionProxy* iProxy)
{
    PlayerProxy* aPlayerProxy{dynamic_cast<PlayerProxy*>(iProxy)};

    if (aPlayerProxy != nullptr)
    {
        return collide(aPlayerProxy);
    }

    BallProxy* aBallProxy{dynamic_cast<BallProxy*>(iProxy)};

    if (aBallProxy != nullptr)
    {
        return collide(aBallProxy);
    }

    // The general case, as well as checking if the 2 goals collide with each other(!)
    return false;
}

bool GoalProxy::collide(BallProxy* iProxy)
{
    // Potential goal determination handled in a separate method
    checkPotentialGoal(iProxy);


    const BoxType& aBallBoundingBox{iProxy->boundingBox()};

    // West post
    bool aOverlapX = !(aBallBoundingBox[kPt1][kX] < _westPostBoundingBox[kPt0][kX] ||
                       _westPostBoundingBox[kPt1][kX] < aBallBoundingBox[kPt0][kX]);

    bool aOverlapY = !(aBallBoundingBox[kPt1][kY] < _westPostBoundingBox[kPt0][kY] ||
                       _westPostBoundingBox[kPt1][kY] < aBallBoundingBox[kPt0][kY]);

    bool aOverlapZ = !(aBallBoundingBox[kPt1][kZ] < _westPostBoundingBox[kPt0][kZ] ||
                       _westPostBoundingBox[kPt1][kZ] < aBallBoundingBox[kPt0][kZ]);

    if (aOverlapX && aOverlapY && aOverlapZ)
    {
        _ballPostCollision = true;
        _ballBarCollision = false;
        return true;
    }

    // East post
    aOverlapX = !(aBallBoundingBox[kPt1][kX] < _eastPostBoundingBox[kPt0][kX] ||
                  _eastPostBoundingBox[kPt1][kX] < aBallBoundingBox[kPt0][kX]);

    aOverlapY = !(aBallBoundingBox[kPt1][kY] < _eastPostBoundingBox[kPt0][kY] ||
                  _eastPostBoundingBox[kPt1][kY] < aBallBoundingBox[kPt0][kY]);

    aOverlapZ = !(aBallBoundingBox[kPt1][kZ] < _eastPostBoundingBox[kPt0][kZ] ||
                  _eastPostBoundingBox[kPt1][kZ] < aBallBoundingBox[kPt0][kZ]);

    if (aOverlapX && aOverlapY && aOverlapZ)
    {
        _ballPostCollision = true;
        _ballBarCollision = false;
        return true;
    }

    // Bar
    aOverlapX = !(aBallBoundingBox[kPt1][kX] < _barBoundingBox[kPt0][kX] ||
                  _barBoundingBox[kPt1][kX] < aBallBoundingBox[kPt0][kX]);

    aOverlapY = !(aBallBoundingBox[kPt1][kY] < _barBoundingBox[kPt0][kY] ||
                  _barBoundingBox[kPt1][kY] < aBallBoundingBox[kPt0][kY]);

    aOverlapZ = !(aBallBoundingBox[kPt1][kZ] < _barBoundingBox[kPt0][kZ] ||
                  _barBoundingBox[kPt1][kZ] < aBallBoundingBox[kPt0][kZ]);

    if (aOverlapX && aOverlapY && aOverlapZ)
    {
        _ballPostCollision = false;
        _ballBarCollision = true;
        return true;
    }

    _ballPostCollision = false;
    _ballBarCollision = false;
    return false;
}

bool GoalProxy::collide(PlayerProxy* iProxy)
{
    return false;   // no-op
}

const GoalProxy::BoxType& GoalProxy::boundingBox() const
{
    return _boundingBox;
}

bool GoalProxy::checkPotentialGoal(BallProxy* iProxy)
{
    constexpr double kFieldHalfLength_m{Constants::Field::kLength_m / 2.0};

    double aInterpolation{};   // from old to new position

    if (_fieldEnd == Constants::Field::kNorthEnd &&
        (_prevBallPosY_m > _ballPosY_m) &&
        ((_prevBallPosY_m + kFieldHalfLength_m) * (_ballPosY_m + kFieldHalfLength_m) <= 0.0))
    {
        aInterpolation = (_prevBallPosY_m + kFieldHalfLength_m) / (_prevBallPosY_m - _ballPosY_m);
        // -49 -52 : int = (-49+50)/(-49--52) = 1/3
        // -52 -49 : int = (-52+50)/(-52--49) = -2/-3
    }
    else if (_fieldEnd == Constants::Field::kSouthEnd &&
             (_prevBallPosY_m < _ballPosY_m) &&
             ((_prevBallPosY_m - kFieldHalfLength_m) * (_ballPosY_m - kFieldHalfLength_m) <= 0.0))
    {
        aInterpolation = (kFieldHalfLength_m - _prevBallPosY_m) / (_ballPosY_m - _prevBallPosY_m);
        // 49 52 : int = (50-49)/(52-49) = 1/3
        // 52 49 : int = (50-52)/(49-52) = -2/-3
    }
    else
    {
        _ballPotentialGoal = false;
        return _ballPotentialGoal;
    }

    double aInterX_m{_prevBallPosX_m + (_ballPosX_m - _prevBallPosX_m) * aInterpolation};
    double aInterZ_m{_prevBallPosZ_m + (_ballPosZ_m - _prevBallPosZ_m) * aInterpolation};
    // 1/3 10 40 : inter = 10 + (40-10)*1/3 = 20
    // 2/3 -10 50 : inter = -10 + (50--10)*2/3 = 30

    _ballPotentialGoal = (aInterX_m > _westPostBoundingBox[kPt1][kX]) &&
                         (aInterX_m < _eastPostBoundingBox[kPt0][kX]) &&
                         (aInterZ_m > _barBoundingBox[kPt1][kZ]);

    return _ballPotentialGoal;
}

}
