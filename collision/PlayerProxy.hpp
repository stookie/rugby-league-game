/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_COLLISION_PLAYERPROXY
#define RL_COLLISION_PLAYERPROXY


#include "collision/ICollisionProxy.hpp"

#include "GlobalConstants.hpp"


struct ISystemData;

namespace State
{

struct Global;

}

namespace Collision
{

class BallProxy;

}

namespace Collision
{

class PlayerProxy : public ICollisionProxy
{
public:
    using ICollisionProxy::BoxType;


    PlayerProxy(Constants::Teams::TeamsType iTeam, std::size_t iNumber);

    virtual ~PlayerProxy() = default;

    PlayerProxy(const PlayerProxy&) = delete;
    PlayerProxy& operator=(const PlayerProxy&) = delete;
    PlayerProxy(PlayerProxy&&) = default;   // Used for constructing in arrays
    PlayerProxy& operator=(PlayerProxy&&) = delete;

    virtual void getInputs(const State::Global& iReadOnlyData, ISystemData& oSystemData) override;
    virtual void setOutputs(const State::Global& iReadOnlyData, ISystemData& oSystemData) override;
    virtual bool collide(ICollisionProxy* iProxy) override;
    virtual const BoxType& boundingBox() const override;

    // The Player proxy is responsible for collision logic with other players and the ball
    virtual bool collide(BallProxy* iProxy);
    virtual bool collide(PlayerProxy* iProxy);

    friend class System;

private:
    BoxType _boundingBox;

    const Constants::Teams::TeamsType _team;   // which team the player is assigned to
    const std::size_t _number;   // the number of the player on the team

    bool _ballCollision;   // set if the player has collided with the ball
};

}

#endif   // RL_COLLISION_PLAYERPROXY
