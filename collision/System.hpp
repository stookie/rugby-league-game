/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_COLLISION_SYSTEM
#define RL_COLLISION_SYSTEM

#include "ISystem.hpp"

#include "collision/BallProxy.hpp"
#include "collision/GoalProxy.hpp"
#include "collision/PlayerProxy.hpp"

#include "GlobalConstants.hpp"

#include <array>


struct ISystemData;


namespace Collision
{

class System : public ISystem
{
public:
    System();

    virtual ~System() = default;

    System(const System&) = delete;
    System& operator=(const System&) = delete;
    System(System&&) = delete;
    System& operator=(System&&) = delete;

    virtual void initialise(const State::Main& iReadOnlyMainData, ISystemData& oSystemData) override;
    virtual void iterate(const State::Global& iReadOnlyData, ISystemData& oSystemData) override;
    virtual void finalise(const State::Global& iReadOnlyData, ISystemData& oSystemData) override;

private:
    BallProxy _ballProxy;
    GoalProxy _northGoalProxy;
    GoalProxy _southGoalProxy;
    std::array<std::array<PlayerProxy, Constants::Rules::kPlayers>, Constants::Teams::kNumTeams> _playerProxies;

    std::array<ICollisionProxy*, Constants::Rules::kPlayers * Constants::Teams::kNumTeams + 3> _proxies;
};

}

#endif   // RL_COLLISION_SYSTEM
