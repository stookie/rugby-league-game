/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "collision/System.hpp"

#include "collision/Data.hpp"
#include "collision/BallProxy.hpp"
#include "collision/PlayerProxy.hpp"

#include "GlobalConstants.hpp"
#include "GlobalData.hpp"

#include <cassert>


namespace Collision
{


/* XYZ

// Abstract class representing an object that collides
// Methods to input data, output data based on underlying
ICollisionProxy
//PlayerProxy, BallProxy, GoalProxy...

void getInputs();   // populate proxy data based on underlying
void setOutputs();   // populate underlying based on proxy data

attributes:
- what is necessary for input-proxy-output translation with global data, eg player team/number
- collision parameters (or else use global constants)
- is fixed, such as for goals etc
- has rebound coefficient (padding is very low, naked posts/bar are high, players are medium-low, ball is medium-high)
- priority level: posts are high and override everything else, ball can rebound off goals and still be possessed

bool collide(const ICollisionProxy&);
- returns collision or not
- sets rebound angle and strength (add combined speed and split?) in attributes
-- takes lowest rebound coefficient
--= per ICollisionProxy and cumulative, taking note of fixed
--= OR cumulative only, but with a fixed rebound flag that takes priority
-- ball rebound of player optional, indicate for possession
-- ball/post collision secondary functionality is field goal

data outputs:
- player rebound velocity components
- ball rebound velocity components (including with player for later uses)
- ball collision per player, as/with distance (to decide in case of multi collision)
- field goal

Use:
Construct an array of ICollisionProxy instances: ball, goals, players
for-each: i.getInputs();
double-for-each: i.collied(j);   // sets collision details for both i and j
for-each: i.setOutputs();

XYZ */

System::System() :
    _ballProxy{},
    _northGoalProxy{Constants::Field::kNorthEnd},
    _southGoalProxy{Constants::Field::kSouthEnd},
    _playerProxies{
        // TeamA (Human) Team:
        PlayerProxy{Constants::Teams::kTeamA, 0},
        PlayerProxy{Constants::Teams::kTeamA, 1},
        // TeamB (Bot) Team:
        PlayerProxy{Constants::Teams::kTeamB, 0},
        PlayerProxy{Constants::Teams::kTeamB, 1} },
    _proxies{&_northGoalProxy,
             &_southGoalProxy,
             &_playerProxies[Constants::Teams::kTeamA][0],
             &_playerProxies[Constants::Teams::kTeamA][1],
             &_playerProxies[Constants::Teams::kTeamB][0],
             &_playerProxies[Constants::Teams::kTeamB][1],
             &_ballProxy}
{
    static_assert(Constants::Teams::kNumTeams == 2, "Number of teams must be 2");

    for (std::size_t aTeam = 0; aTeam < Constants::Teams::kNumTeams; aTeam++)
    {
        for (std::size_t aPlayer = 0; aPlayer < Constants::Rules::kPlayers; aPlayer++)
        {
            assert(_playerProxies[aTeam][aPlayer]._team == aTeam);
            assert(_playerProxies[aTeam][aPlayer]._number == aPlayer);
        }
    }
}

void System::initialise(const State::Main& iReadOnlyMainData, ISystemData& oSystemData)
{
}

void System::iterate(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
    for (auto& aProxy : _proxies)
    {
        aProxy->getInputs(iReadOnlyData, oSystemData);
    }

    for (std::size_t aProxy1 = 0; aProxy1 < _proxies.size(); aProxy1++)
    {
        for (std::size_t aProxy2 = aProxy1 + 1; aProxy2 < _proxies.size(); aProxy2++)
        {
            _proxies[aProxy1]->collide(_proxies[aProxy2]);
        }
    }

    for (auto& aProxy : _proxies)
    {
        aProxy->setOutputs(iReadOnlyData, oSystemData);
    }
}

void System::finalise(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
}

}   // namespace Collision
