/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_COLLISION_BALLPROXY
#define RL_COLLISION_BALLPROXY


#include "collision/ICollisionProxy.hpp"


struct ISystemData;

namespace State
{

struct Global;

}


namespace Collision
{

class BallProxy : public ICollisionProxy
{
public:
    using ICollisionProxy::BoxType;


    BallProxy() = default;

    virtual ~BallProxy() = default;

    BallProxy(const BallProxy&) = delete;
    BallProxy& operator=(const BallProxy&) = delete;
    BallProxy(BallProxy&&) = delete;
    BallProxy& operator=(BallProxy&&) = delete;

    virtual void getInputs(const State::Global& iReadOnlyData, ISystemData& oSystemData) override;
    virtual void setOutputs(const State::Global& iReadOnlyData, ISystemData& oSystemData) override;
    virtual bool collide(ICollisionProxy* iProxy) override;
    virtual const BoxType& boundingBox() const override;

    // The Ball proxy delegates all collision logic to the alternate proxy.
    // Note not specific collide() overloads here.

private:
    BoxType _boundingBox;
};

}

#endif   // RL_COLLISION_BALLPROXY
