/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "collision/BallProxy.hpp"

#include "collision/Data.hpp"

#include "GlobalConstants.hpp"
#include "GlobalData.hpp"


namespace Collision
{

void BallProxy::getInputs(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
    constexpr double kBallBoundingBoxHalfWidth_m{Constants::Ball::kBoundingBoxWidth_m / 2.0};
    constexpr double kBallBoundingBoxHalfLength_m{Constants::Ball::kBoundingBoxLength_m / 2.0};

    // All coordinates for bounding box point 0 must be less than for point 1
    _boundingBox[kPt0][kX] = iReadOnlyData._ball._posX_m - kBallBoundingBoxHalfWidth_m;
    _boundingBox[kPt1][kX] = iReadOnlyData._ball._posX_m + kBallBoundingBoxHalfWidth_m;
    _boundingBox[kPt0][kY] = iReadOnlyData._ball._posY_m - kBallBoundingBoxHalfLength_m;
    _boundingBox[kPt1][kY] = iReadOnlyData._ball._posY_m + kBallBoundingBoxHalfLength_m;
    _boundingBox[kPt0][kZ] = iReadOnlyData._ball._posZ_m;
    _boundingBox[kPt1][kZ] = iReadOnlyData._ball._posZ_m + Constants::Ball::kBoundingBoxHeight_m;
}

void BallProxy::setOutputs(const State::Global& iReadOnlyData, ISystemData& oSystemData)
{
}

bool BallProxy::collide(ICollisionProxy* iProxy)
{
    // The ball-specific collision details are very simple, so delegate the collision detection to every other type.
    return iProxy->collide(this);
}

const BallProxy::BoxType& BallProxy::boundingBox() const
{
    return _boundingBox;
}

}
