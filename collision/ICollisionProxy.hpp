/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_COLLISION_ICOLLISIONPROXY
#define RL_COLLISION_ICOLLISIONPROXY


#include <array>


struct ISystemData;

namespace State
{

struct Global;

}


class ICollisionProxy
{
public:
    // Public Types used for bounding boxes
    enum BoxPoint : unsigned char { kPt0, kPt1, kNumPoints };
    enum PointCoord : unsigned char { kX, kY, kZ, kNumCoords };
    using PointType = std::array<double, kNumCoords>;
    using BoxType = std::array<PointType, kNumPoints>;


    ICollisionProxy() = default;
    virtual ~ICollisionProxy() = default;

    ICollisionProxy(const ICollisionProxy&) = delete;
    ICollisionProxy& operator=(const ICollisionProxy&) = delete;
    ICollisionProxy(ICollisionProxy&&) = default;   // Used for constructing in arrays
    ICollisionProxy& operator=(ICollisionProxy&&) = delete;

    // This method is used to retrieve data from the global state used for later collision calculations
    virtual void getInputs(const State::Global& iReadOnlyData, ISystemData& oSystemData) = 0;

    // Perform a collision detection calculation, on another collision proxy
    // This may perform an reverse dispatch to transfer the calc to the other proxy
    virtual bool collide(ICollisionProxy* iProxy) = 0;

    // This method is used to return data to the global state determined in prior collision calculations
    virtual void setOutputs(const State::Global& iReadOnlyData, ISystemData& oSystemData) = 0;

    // Return a bounding box for the proxy, to the most sensible degree possible
    virtual const BoxType& boundingBox() const = 0;
};

#endif   // RL_COLLISION_ICOLLISIONPROXY
