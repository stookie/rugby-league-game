/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_COLLISION_GOALPROXY
#define RL_COLLISION_GOALPROXY


#include "collision/ICollisionProxy.hpp"

#include "GlobalConstants.hpp"


struct ISystemData;

namespace State
{

struct Global;

}

namespace Collision
{

class BallProxy;
class PlayerProxy;

}


namespace Collision
{

class GoalProxy : public ICollisionProxy
{
public:
    using ICollisionProxy::BoxType;


    GoalProxy(Constants::Field::EndType iFieldEnd);

    virtual ~GoalProxy() = default;

    GoalProxy(const GoalProxy&) = delete;
    GoalProxy& operator=(const GoalProxy&) = delete;
    GoalProxy(GoalProxy&&) = delete;
    GoalProxy& operator=(GoalProxy&&) = delete;

    virtual void getInputs(const State::Global& iReadOnlyData, ISystemData& oSystemData) override;
    virtual void setOutputs(const State::Global& iReadOnlyData, ISystemData& oSystemData) override;
    virtual bool collide(ICollisionProxy* iProxy) override;
    virtual const BoxType& boundingBox() const override;

    // The Goal proxy is responsible for collision logic with players and the ball
    virtual bool collide(BallProxy* iProxy);
    virtual bool collide(PlayerProxy* iProxy);

private:
    // Special case of potential goal detection for the ball
    bool checkPotentialGoal(BallProxy* iProxy);

    bool _firstPass;

    Constants::Field::EndType _fieldEnd;

    BoxType _boundingBox;
    BoxType _westPostBoundingBox;
    BoxType _eastPostBoundingBox;
    BoxType _barBoundingBox;

    bool _ballPostCollision;
    bool _ballBarCollision;
    bool _ballPotentialGoal;

    double _ballPosX_m;
    double _ballPosY_m;
    double _ballPosZ_m;
    double _prevBallPosX_m;
    double _prevBallPosY_m;
    double _prevBallPosZ_m;
};

}

#endif   // RL_COLLISION_GOALPROXY
