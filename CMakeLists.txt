# Authors:
# stookie <4303586-stookie@users.noreply.gitlab.com>
# Copyright (C) 2019-2020 authors
# Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.

project(rugby_league)

cmake_minimum_required(VERSION 2.8.9)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${rugby_league_SOURCE_DIR}/cmake")

list(APPEND rugby_league_SOURCES main.cpp SdlCtx.cpp Dispatcher.cpp)

include(ball/CMakeLists.txt)
include(camera/CMakeLists.txt)
include(collision/CMakeLists.txt)
include(input/CMakeLists.txt)
include(players/CMakeLists.txt)
include(rng/CMakeLists.txt)
include(rules/CMakeLists.txt)
include(video/CMakeLists.txt)

include_directories(${rugby_league_SOURCE_DIR})

add_executable(rugby_league ${rugby_league_SOURCES})

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wno-unused-parameter -pedantic -std=c++11")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_DEBUG} -g")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_RELEASE} -O2")

find_package(SDL2 REQUIRED)
find_package(SDL2_image REQUIRED)
find_package(SDL2_ttf REQUIRED)

target_link_libraries(rugby_league ${SDL2_LIBRARY} ${SDL2_IMAGE_LIBRARY} ${SDL2_TTF_LIBRARY})

include_directories(${SDL2_INCLUDE_DIR} ${SDL2_IMAGE_INCLUDE_DIR})
