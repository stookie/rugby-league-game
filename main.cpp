/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#include "GlobalData.hpp"
#include "Dispatcher.hpp"
#include "SdlCtx.hpp"

#include <iostream>
#include <string>


int main(int argc, char** argv)
{
    try
    {
        // Initialise the Dispatcher
        Dispatcher aDispatcher{Constants::Main::kIterationRate_Hz};
        aDispatcher.run();
    }
    catch (const SdlCtxException& iExc)
    {
        std::cerr << "SdlCtxException: " << iExc.what() << std::endl;
    }
    catch (const DispatcherException& iExc)
    {
        std::cerr << "DispatcherException: " << iExc.what() << std::endl;
    }
    catch (const std::exception& iExc)
    {
        std::cerr << "Standard Exception: " << iExc.what() << std::endl;
    }
    catch (...)
    {
        std::cerr << "Unknown Exception" << std::endl;
    }

    return 0;
}
