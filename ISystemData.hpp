/**
 * Authors:
 *   stookie <4303586-stookie@users.noreply.gitlab.com>
 *
 * Copyright (C) 2019-2020 authors
 *
 * Released under GNU AGPL v3 only, read the file 'COPYING.md' for more information.
 */

#ifndef RL_MAIN_ISYSTEM_DATA
#define RL_MAIN_ISYSTEM_DATA


struct ISystemData
{
public:
    ISystemData() = default;
    virtual ~ISystemData() = default;

    ISystemData(const ISystemData&) = delete;
    ISystemData& operator=(const ISystemData&) = default;   // default
    ISystemData(ISystemData&&) = delete;
    ISystemData& operator=(ISystemData&&) = delete;
};

#endif   // RL_MAIN_ISYSTEM_DATA
